<?php

class mnumicoreConfiguration extends sfApplicationConfiguration
{
    public function configure()
    {
    }

    /**
     * Get default configured country (ex. 'pl')
     * @return string Lower case country name
     * @throws Exception
     */
    public static function getCountry()
    {
        if(sfConfig::has('app_default_country'))
        {
            return strtolower(sfConfig::get('app_default_country'));
        }

        $culture = sfConfig::get('sf_default_culture', false);
        if($culture)
        {
            if($pos = strpos($culture, '_'))
            {
                return strtolower(substr($culture, 0, $pos));
            }

            return strtolower($culture);
        }

        return 'pl';
    }

    /**
     * Get precision number for setting prices
     *
     * @example sfValidatorPrices class
     * @return int Precision number (default "2")
     */
    public static function getSettingPricePrecision()
    {
        return sfConfig::get('app_price_precision', 2);
    }

}
