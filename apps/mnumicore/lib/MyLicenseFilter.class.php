<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * MyLicenseFilter checks license check license conditions.
 *
 * @package    mnumi
 * @subpackage license
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 */
class MyLicenseFilter extends sfExecutionFilter
{
    /**
     * Executes this filter.
     *
     * @param sfFilterChain $filterChain A sfFilterChain instance
     */
    public function execute($filterChain)
    {
        if(in_array($this->context->getActionName(), array('license', 'signout'))
            || (! $this->isUserAuthenticated())
            )
        {
            // the user has access, continue
            parent::execute($filterChain);
            return;
        }
            
        $information = priceTool::checkKey();
        
        if($information == false)
        {
            $this->forwardToLicenseAction();
            throw new sfStopException();
        }
        
        if($information['type'] == 'yearly-subs' && getenv('MNUMI_TIME') > $information['expire_at'])
        {
            $this->forwardToLicenseAction();
            throw new sfStopException();
        }
        
        // the user has access, continue
        parent::execute($filterChain);
    }
    
    protected function isUserAuthenticated()
    {
        if(!$this->getContext()->getUser()->isAuthenticated())
        {
            return false;
        }
        
        if(!$this->getContext()->getUser()->getGuardUser())
        {
            return false;
        }
        
        return true;
    }
    

    /**
     * Forwards the current request to the license action.
     *
     * @throws sfStopException
     */
    protected function forwardToLicenseAction()
    {   
        $this->context->getController()->redirect('@license');
    }
    
}