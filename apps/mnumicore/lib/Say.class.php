<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Say
{
    private function sayPL($amount, $part)
    {
        if($amount == 0)
            return '';
        if($amount == 1)
            return 'jeden '.$part;
        if($amount > 999)
            return 'Błędna liczba (>999)';
        $segment = '';
        
        $hundred = intval(($amount / 100));
        $ten = intval(($amount - 100 * $hundred) / 10);
        $unit = intval(($amount - 100 * $hundred - 10 * $ten));
        switch($hundred)
        {
            case 9: $segment.='dziewięćset ';
                break;
            case 8 : $segment.='osiemset ';
                break;
            case 7 : $segment.='siedemset ';
                break;
            case 6 : $segment.='sześćset ';
                break;
            case 5 : $segment.='pięćset ';
                break;
            case 4 : $segment.='czterysta ';
                break;
            case 3 : $segment.='trzysta ';
                break;
            case 2 : $segment.='dwieście ';
                break;
            case 1 : $segment.='sto ';
                break;
        }

        switch($ten)
        {
            case 9 : $segment.='dziewięćdziesiąt ';
                break;
            case 8 : $segment.='osiemdziesiąt ';
                break;
            case 7 : $segment.='siedemdziesiąt ';
                break;
            case 6 : $segment.='sześćdziesiąt ';
                break;
            case 5 : $segment.='pięćdziesiąt ';
                break;
            case 4 : $segment.='czterdzieści ';
                break;
            case 3 : $segment.='trzydzieści ';
                break;
            case 2 : $segment.='dwadzieścia ';
                break;
            case 1 :
                switch($unit)
                {
                    case 9 : $segment.='dziewiętnaście ';
                        break;
                    case 8 : $segment.='osiemnaście ';
                        break;
                    case 7 : $segment.='siedemnaście ';
                        break;
                    case 6 : $segment.='szesnaście ';
                        break;
                    case 5 : $segment.='piętnaście ';
                        break;
                    case 4 : $segment.='czternaście ';
                        break;
                    case 3 : $segment.='trzynaście ';
                        break;
                    case 2 : $segment.='dwanaście ';
                        break;
                    case 1 : $segment.='jedenaście ';
                        break;
                    case 0 : $segment.='dziesięć ';
                        break;
                }
        }

        if($ten != 1)
        {
            switch($unit)
            {
                case 9 : $segment.='dziewięć ';
                    break;
                case 8 : $segment.='osiem ';
                    break;
                case 7 : $segment.='siedem ';
                    break;
                case 6 : $segment.='sześć ';
                    break;
                case 5 : $segment.='pięć ';
                    break;
                case 4 : $segment.='cztery ';
                    break;
                case 3 : $segment.='trzy ';
                    break;
                case 2 : $segment.='dwa ';
                    break;
                case 1 : $segment.='jeden ';
                    break;
            }
        }
        return ($segment.$part);
    }

    /**
     * Returns amount in words.
     * 
     * @param float $amount
     * @return string 
     */
    public function getPL($amount)
    {
        $mln = intval(($amount / 1000000));
        $thou = intval((($amount - 1000000 * $mln) / 1000));
        $unit = intval(($amount - 1000000 * $mln - 1000 * $thou));
        $gr = round(100 * ($amount - 1000000 * $mln - 1000 * $thou - $unit));

        $wordAmount = $this->sayPL($mln, 'mil. ')
            . $this->sayPL($thou, 'tys. ')
            . $this->sayPL($unit, 'zł ')
            . $this->sayPL($gr, 'gr')
        ;

        return $wordAmount;
    }

}
