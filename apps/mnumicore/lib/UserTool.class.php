<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of UserTool
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class UserTool
{
    /** 
     * Get pricelist ID by user session handle
     * 
     * @param string $session_handle
     * @return integer
     */
    static function getPricelistBySessionHandle($session_handle)
    {
        throw new Exception('Withdrawn in v3.x version');
    }
    
    /** 
     * Get IDDelivery by user session handle
     * 
     * @param string $session_handle
     * @return integer
     */
    static function getDeliveryIdBySessionHandle($session_handle)
    {
        throw new Exception('Withdrawn in v3.x version');
    }
}

