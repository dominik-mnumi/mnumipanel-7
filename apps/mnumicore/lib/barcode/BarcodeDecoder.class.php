<?php

/*
 * This file is part of the MnumiCore package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * BarcodeDecoder class for decoding barcode from given string
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class BarcodeDecoder
{
    // barcode string
    protected $barcode;
    
    // available model tables
    private $modelTables = array('ClientTable', 'sfGuardUserTable', 
        'OrderTable', 'OrderPackageTable');
    
    /**
     * Constructor
     * 
     * @param $barcode barcode string
     * 
     */
    function __construct($barcode)
    {
        $this->barcode = $barcode;
    }
    
    /**
     * Get prefix from barcode
     * 
     * @return string 
     */
    protected function getPrefix()
    {
        return substr($this->barcode,0,1);
    }
    
    /**
     * Get type from barcode
     * 
     * @return integer 
     */
    protected function getType()
    {
        return (int)substr($this->barcode,1,2);
    }
    
    /**
     * Get id from barcode
     * 
     * @return integer 
     */
    public function getId()
    {
        return (int)ltrim(substr($this->barcode,3,8), '0');
    }
    
    /**
     * Get additional number from barcode
     * 
     * @return integer 
     */
    public function getAdditionalNumber()
    {
        return substr($this->barcode,11, -1);
    }
    
    /**
     * Get table name from type
     * 
     * @return string Table model name 
     */
    public function getTableNameFromType()
    {
        foreach($this->modelTables as $model)
        {
            if($this->getType() == $model::$barcodePrefix)
            {
                return $model;
            }
        }
        
        throw new Exception('Unknown model for type: ' . $this->getType());
    }
    
    /**
     * Get object for barcode reuest
     * 
     * @return Doctrine_Record
     */
    public function getObject()
    {
        $tableName = $this->getTableNameFromType();
        
        $object = $tableName::getInstance()->find($this->getId());

        if(!$object)
        {
            throw new Exception('Object for this barcode not found.');
        }
        
        return $object;
    }
}
