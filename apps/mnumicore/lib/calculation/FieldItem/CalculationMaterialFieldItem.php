<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationMaterialFieldItem class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationMaterialFieldItem extends CalculationFieldItem
{
    /** @var float */
    private $printSizeWidth;

    /** @var float */
    private $printSizeHeight;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $label
     * @param string $cost
     * @param float $printSizeWidth
     * @param float $printSizeHeight
     */
    public function __construct($name, $label, $cost, $printSizeWidth, $printSizeHeight)
    {
        $this->name = $name;
        $this->label = $label;
        $this->cost = $cost;
        $this->printSizeWidth = $printSizeWidth;
        $this->printSizeHeight = $printSizeHeight;
    }
}