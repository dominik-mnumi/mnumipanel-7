<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PriceListFixPrice class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PriceListFixPrice implements Countable, IteratorAggregate, Serializable
{
    /**
     * @var int
     */
    private $id;

    /** @var FixPrice[] */
    private $data;

    /**
     * Constructor
     *
     * @param int $id Price list ID
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @param FixPrice $fixPrice
     */
    public function add(FixPrice $fixPrice)
    {
        $primaryKey = $fixPrice->getQuantity();

        $this->data[$primaryKey] = $fixPrice;
    }

    /**
     * Get element by Quantity
     *
     * @param float $quantity
     * @return bool|FixPrice
     */
    public function get($quantity)
    {
        if (count($this->data) == 0) {
            return false;
        }

        $data = $this->data;

        $current = array_shift($data);

        foreach($this->data as $data) {
            if ($data->getQuantity() > $quantity) {
                continue;
            }
            $current = $data;
        }

        return $current;
    }

    public function count()
    {
        return count($this->data);
    }
    /**
     * @return int
     */
    public function getPriceListId()
    {
        return $this->id;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        $data = $this->data;
        return new ArrayIterator($data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        $vars = get_object_vars($this);

        return serialize($vars);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $array = unserialize($serialized);

        foreach ($array as $name => $values) {
            $this->$name = $values;
        }
    }
}