<?php

class dompdfView extends sfPHPView
{
    /**
     * Renders the presentation.
     *
     * @return string A string representing the rendered presentation
     */
    public function render()
    {
        $content = parent::render();

        mb_internal_encoding('UTF-8');
        $pdf = new DOMPDF();
        $pdf->set_protocol('http://');
        $pdf->set_host($_SERVER["SERVER_NAME"]);
        $pdf->set_base_path('/');
        $pdf->load_html($content, 'UTF-8');
        $pdf->set_paper("a4",  "portrait");
        $pdf->render();
        return $pdf->output();
    }
}