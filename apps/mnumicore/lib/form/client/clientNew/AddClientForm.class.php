<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ClientForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class AddClientForm extends EditClientGeneralForm
{
    protected $taxIdFormName = 'tax_id';
    protected $countryFormName = 'country';

    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        // widgets       
        $this->setWidget('company_checkbox', new sfWidgetFormInputHidden(
                        array('default' => true)));
        $this->setWidget('user_checkbox', new sfWidgetFormInputCheckbox(
                        array('label' => 'Create user', 'default' => true)));
        $this->setWidget('user_name_surname', new sfWidgetFormInput(
                        array('label' => 'Name and surname'),
                        array('class' => 'full-width')));
        $this->setWidget('user_email', new sfWidgetFormInput(
                        array('label' => 'Email'),
                        array('class' => 'full-width')));
        $this->setWidget('user_password', new sfWidgetFormInputPassword(
                        array('label' => 'Password'),
                        array('class' => 'full-width')));
        $this->setWidget('user_password_again', new sfWidgetFormInputPassword(
                        array('label' => 'Confirm password'),
                        array('class' => 'full-width')));       
        
        // validators
        // set fullname validator as pass; in post validator checks 
        // depending on client type
        $this->setValidator('fullname', new sfValidatorPass());       
        $this->setValidator('tax_id', new sfValidatorString(array('required' => false)));
        $this->setValidator('company_checkbox', new sfValidatorPass());
        $this->setValidator('user_checkbox', new sfValidatorBoolean(array('required' => false)));
        $this->setValidator('user_name_surname', new sfValidatorPass());
        $this->setValidator('user_email', new sfValidatorPass());
        $this->setValidator('user_password', new sfValidatorPass());
        $this->setValidator('user_password_again', new sfValidatorPass());
   
        // post validators
        $this->mergePostValidator(new sfValidatorAnd(
                        array(new sfValidatorCallback(array('callback' => array($this, 'validateUser'))),
                            new sfValidatorCallback(array('callback' => array($this, 'validateClientType'))))));


        $this->useFields(array('fullname', 'street', 'postcodeAndCity', 'tax_id',
            'user_checkbox', 'user_name_surname', 'user_email',
            'user_password', 'user_password_again',
            'account', 'country'));
    }

    /**
     * Conditional validator. If user creation is checked then validate all values.
     * 
     * @param sfValidator $validator
     * @param array $values
     * @return array
     * @throws sfValidatorErrorSchema 
     */
    public function validateUser($validator, $values)
    {
        $errorSchema = new sfValidatorErrorSchema($validator);
        $validatorArray = array();
        
        // if user creation is checked
        if($this->isUserChecked($values))
        {
            // sets validators 
            $this->setValidator('user_name_surname', new sfValidatorNameAndSurname(
                            array('required' => true),
                            array('invalid' => 'Field "Name and surname" (User) is invalid. Format: Name Surname')));
            $this->setValidator('user_email', new sfValidatorEmail(
                            array('required' => true),
                            array('invalid' => 'Field "Email address" is invalid')));
            $this->setValidator('user_password', new sfValidatorString(
                            array('required' => true,
                                  'min_length' => 6),
                            array('min_length' => 'Field "Password" must be at least 6 character long')));
            $this->setValidator('user_password_again', new sfValidatorString(
                            array('required' => true)));                             
              
            // if password are diff then add error
            if($values['user_password'] != $values['user_password_again'])
            {
                $error = new sfValidatorError($validator, 'The two passwords must be the same');
                $errorSchema->addError($error, 'user_checkbox');
            }

            // if email exist
            if(!empty($values['user_email']))
            {
                if(sfGuardUserTable::getInstance()->findOneByEmailAddress($values['user_email']))
                {
                    $error = new sfValidatorError($validator, 'This "Email address" already exists in database');
                    $errorSchema->addError($error, 'user_email');
                }
            }
            
            // prepare array with names to validate
            $validatorArray = array('user_name_surname', 'user_email', 'user_password', 'user_password_again');

            // validators messages
            $this->setMessages('required', 'Field "%label%" required', $validatorArray);
        }

        // foreach selected validator re-process clean method
        $values = $this->throwErrors($validatorArray, $errorSchema, $values);

        return $values;
    }

    /**
     * Conditional validator. If company is checked then validate NIP.
     * 
     * @param sfValidatorBase $validator
     * @param array $values
     * @return array
     * @throws sfValidatorErrorSchema 
     */
    public function validateClientType($validator, $values)
    {
        $errorSchema = new sfValidatorErrorSchema($validator);
        $validatorArray = array();

        // if company is checked (company)
        if($values['company_checkbox'])
        {
            $this->setValidator('fullname', new sfValidatorString(
                            array('required' => true,
                                  'max_length' => 255)));

            $validatorArray = array('fullname', 'tax_id');

            // validators messages
            $this->setMessages('required', 'Field "%label%" required', $validatorArray);
        }
        // if company is not checked (person)
        else
        {
            // sets validators
            $this->setValidator('fullname', new sfValidatorNameAndSurname(
                            array('required' => true),
                            array('invalid' => 'Field "Name and surname" is invalid. Format: Name Surname',
                                  'required'=> 'Field "Name and surname" required')));
            
            // prepare array with names to validate
            $validatorArray = array('fullname');       
        }

        // foreach selected validator re-process clean method
        $values = $this->throwErrors($validatorArray, $errorSchema, $values);

        return $values;
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);

        //gets and sets pricelist
        $pricelistObj = PricelistTable::getInstance()->getDefaultPricelist();
        $object->setPricelist($pricelistObj);
        $object->setPostcodeAndCity($this->getValue('postcodeAndCity'));
        
        return $object;
    }

    /**
     * Returns client object.
     * 
     * @param type $con
     * @return Client $clientObj 
     */
    public function save($con = null)
    {
        $clientObj = parent::save($con);

        $city = $this->getObject()->getCity();
        $postcode = $this->getObject()->getPostcode();
        $street = $this->getObject()->getStreet();

        $clientAddressObj = new ClientAddress();
        $clientAddressObj->setCity($city);
        $clientAddressObj->setPostcode($postcode);
        $clientAddressObj->setStreet($street);
        $clientAddressObj->setFullname('Delivery');
        $clientAddressObj->setClient($clientObj);
        $clientAddressObj->setCountry($clientObj->getCountry());
        $clientAddressObj->save();

        // create user
        $valueArray = $this->getValues();
        
        if($valueArray['user_checkbox'])
        {
            // gets first, last name string
            $firstnameLastname = explode(' ', $valueArray['user_name_surname']);
            
            $userObj = new sfGuardUser();
            $userObj->setFirstName($firstnameLastname[0]);
            $userObj->setLastName($firstnameLastname[1]);
            $userObj->setEmailAddress($valueArray['user_email']);
            $userObj->setPassword($valueArray['user_password']);
            $userObj->save();
            
            $clientUser = new ClientUser();
            $clientUser->setClient($clientObj);
            $clientUser->setUser($userObj);
            $clientUser->setClientUserPermission(ClientUserPermissionTable::getInstance()
                    ->findOneByName(ClientUserPermissionTable::$admin));
            $clientUser->save();
        }
        
        return $clientObj;
    }

    /**
     * Throws errors of callback validators.
     * 
     * @param array $validatorArray
     * @param type $errorSchema
     * @throws type 
     */
    private function throwErrors($validatorArray, sfValidatorErrorSchema $errorSchema, $values)
    {
        $valueArr = array();
        foreach($validatorArray as $field)
        {
            try
            {
                $valueArr[$field] =  $this->validatorSchema[$field]->clean($values[$field]);
            }
            catch(sfValidatorError $e)
            {
                $errorSchema->addError($e, $field);
            }
        }
       
        // if some errors
        if($errorSchema->count())
        {
            throw $errorSchema;   
        }
        
        $values = array_merge($values, $valueArr);
        
        return $values;
    }
    
    /**
     * Checks if user is checked.
     * 
     * @return boolean  
     */
    private function isUserChecked($values)
    {
        return ($values['user_checkbox']) ? true : false;       
    }
}
