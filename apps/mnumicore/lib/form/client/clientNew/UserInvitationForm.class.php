<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extends sfGuardUserForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class UserInvitationForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
       
        // validators messages
        $this->setMessages('required', 'Field "%label%" required');
        
        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorDoctrineUnique(
                        array('model' => 'sfGuardUser', 
                              'column' => 'email_address'),
                        array('invalid' => 'This "Email address" already exists in database')));
    
        $this->useFields(array('email_address'));
    }
    
    public function save($con = null)
    {    
        $obj = parent::save($con);
        $obj->createClientAndConnect();
        
        return $this->getObject();
    }

}