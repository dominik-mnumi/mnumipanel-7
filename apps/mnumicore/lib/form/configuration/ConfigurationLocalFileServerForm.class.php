<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationLocalFileServerForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationLocalFileServerForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'local_file_server_ip',
            'local_file_server_enable'));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['local_file_server']['ip'] = $values['local_file_server_ip'];
       
        // gets response and sets cookie
        $responseObj = sfContext::getInstance()->getResponse();
        $responseObj->setCookie(
                'local_file_server_enable', 
                $values['local_file_server_enable'], 
                time() + 20 * 365 * 24 * 60 * 60);

        // save app.yml file & clear cache
        parent::save();
    }
}