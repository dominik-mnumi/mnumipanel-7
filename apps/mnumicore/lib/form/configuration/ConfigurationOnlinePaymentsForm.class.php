<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationOnlinePaymentsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationOnlinePaymentsForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'platnoscipl_pos_id', 
            'platnoscipl_pos_auth',
            'platnoscipl_key_1', 
            'platnoscipl_key_2'));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['platnoscipl']['pos_id'] = $values['platnoscipl_pos_id'];
        $this->AppArray['all']['platnoscipl']['pos_auth'] = $values['platnoscipl_pos_auth'] == '0000000000' ? $this->defaults['platnoscipl_pos_auth'] : $values['platnoscipl_pos_auth'];
        $this->AppArray['all']['platnoscipl']['key_1'] = $values['platnoscipl_key_1'] == '00000000000000000000000000000000' ? $this->defaults['platnoscipl_key_1'] : $values['platnoscipl_key_1'];
        $this->AppArray['all']['platnoscipl']['key_2'] = $values['platnoscipl_key_2'] == '00000000000000000000000000000000' ? $this->defaults['platnoscipl_key_2'] : $values['platnoscipl_key_2'];

        // save app.yml file & clear cache
        parent::save();
    }
}