<?php
/*
 * This file is part of the MnumiCore package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * myUserTerminalAuthentication class for authenticate users using passwordPart and id
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class myUserTerminalAuthentication extends sfGuardSecurityUser
{
    /**
     * "Simple" authorization for users (for specific modules like barcode authorization)
     * 
     * @param $id sfGuardUser id
     * @param $passwordPart partial of password
     * 
     * @return boolean
     */
    public function authorizeUsingPasswordSaltAndId($id, $passwordPart)
    {
        $user = sfGuardUserTable::getInstance()->findOneByIdAndPasswordPart($id, $passwordPart);
        if(!$user)
        {
            throw new Exception('Unable to find user with given id and password part.');
        }
        
        $this->setAttribute('userId', $user->getId(), 'simpleAuthorization');
        return true;
    }
    
    /**
     * Check if user is "simple" authorized
     *
     * @return boolean
     */
    public function isAuthorizedUsingPasswordSaltAndId()
    {
        if($this->hasAttribute('userId', 'simpleAuthorization') && $this->getAttribute('userId', null, 'simpleAuthorization') > 0)
        {
            return true;
        }
      
        return false;
    }
    
    /**
     * Return "simple" authorized user id
     *
     * @return boolean
     */
    public function getAuthorizedUsingPasswordSaltAndIdUserId()
    {
        if(!$this->isAuthorizedUsingPasswordSaltAndId())
        {
            throw new Exception('Unable to get simple authorized user id - no user is authorized.');
        }
      
        return $this->getAttribute('userId', null, 'simpleAuthorization');
    }
    
    /**
     * Remove "simple" authorization
     *
     * @return boolean
     */
    public function removeAuthorizationUsingPasswordSaltAndId()
    {
        $this->setAttribute('userId', null, 'simpleAuthorization');
        return true;
    }
}
