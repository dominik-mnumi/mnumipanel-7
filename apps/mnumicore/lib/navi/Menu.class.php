<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Menu class. Simple object to manages menu data.
 *
 * @package    mnumicore
 * @author     Marek Balicki
 */
class Menu implements MenuInterface
{
    protected $menu;
    protected $navigatorObj;
    protected $level;
    
    /**
     * Creates instance of menu object.
     * 
     * @param array $menu
     * @param Navigator $navigatorObj
     * @param integer $level
     */
    public function __construct(array $menu, Navigator $navigatorObj, $level = 1)
    {
        $this->menu = $menu;
        $this->navigatorObj = $navigatorObj;
        $this->level = $level;
    }
    
    /**
     * Returns menu route.
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->menu['route'];
    }
    
    /**
     * Returns menu class.
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->menu['class'];
    }
    
    /**
     * Returns menu title.
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->menu['title'];
    }

    /**
     * Returns menu level.
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }
    
    /**
     * Returns true if has submenu, otherwise false.
     * 
     * @return boolean 
     */
    public function hasSubmenu()
    {
        return array_key_exists('submenu', $this->menu);
    }
    
    /**
     * Returns submenu objects in array.
     * 
     * @return array 
     */
    public function getSubmenu()
    {
        if(!isset($this->menu['submenu']))
        {
            return array();
        }
        
        $menuColl = array();
        foreach($this->menu['submenu'] as $rec)
        {
            $menuColl[] = new self($rec, $this->navigatorObj, $this->level + 1);
        }
        
        return $menuColl;
    }

    /**
     * Returns defined action array for current menu.
     *
     * @throws Exception
     * @return array 
     */
    public function getActions()
    {
        // if actions empty then return empty array
        if(!$this->menu['actions'])
        {
            return array();
        }
        
        // first divides into "module:action" sections
        $tmpArr = explode(',', $this->menu['actions']);

        // if "zero" sections 
        if(empty($tmpArr))
        {
            throw new Exception('Invalid actions attribute in "'.$this->getTitle().'".');
        }
        
        // foreach section "module:action"
        $moduleAndActionArr = array();
        foreach($tmpArr as $rec)
        {
            // divides module:action to [module] and [action]
            $tmp2Arr = explode(':', $rec);
            
            // if wrong semantic
            if(empty($tmp2Arr))
            {
                throw new Exception('Invalid actions attribute in "'.$this->getTitle().'".');
            }
            
            $moduleAndActionArr[] = array(
                'module' => trim($tmp2Arr[0]),
                'action' => trim($tmp2Arr[1]));
        }
        
        return $moduleAndActionArr;
    } 
    
    /**
     * Checks if menu is current.
     *  
     * @return boolean 
     */
    public function isCurrent()
    {
        return $this->navigatorObj->isCurrent($this);
    }
    
    /**
     * Returns true if user has credential to menu, otherwise false.
     * 
     * @return boolean 
     */
    public function hasCredential()
    {
        return $this->navigatorObj->hasCredential($this);
    }

    public function isVisible()
    {
        // menu when advanced accountancy is not disabled
        if(in_array($this->getRoute(), array('@invoiceList', '@invoiceCost'))
            && sfConfig::get('app_default_disable_advanced_accountancy', 0))
        {
            return FALSE;
        }

        // if route is @homepage
        if($this->getRoute() == '@homepage')
        {
            return TRUE;
        }

        // if at least 2-nd level submenu and has credential (2-nd and higher level must be visible if has credential)
        if($this->getLevel() > 1 && $this->hasCredential())
        {
            return TRUE;
        }

        return $this->hasSubmenuWithCredential();
    }

    /**
     * Returns true if submenu has at least one menu with credential.
     *
     * @return bool
     */
    public function hasSubmenuWithCredential()
    {
        foreach($this->getAllSubmenus() as $rec)
        {
            // if menu has credential then return TRUE
            if($rec->hasCredential())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Returns all submenus for current menu.
     *
     * @param array $menuArr
     * @return array
     */
    protected function getAllSubmenus($menuArr = array())
    {
        foreach($this->getSubmenu() as $rec)
        {
            $menuArr[] = $rec;
            $menuArr = $rec->getAllSubmenus($menuArr);
        }

        return $menuArr;
    }
}