<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Payment import adapter interface.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Marek Balicki
 */
interface PaymentImportAdapterInterface
{

    /**
     * Returns collection of imported rows. 
     */
    public function getRowColl();
}