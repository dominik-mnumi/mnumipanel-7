<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ImportPaymentInvoiceCollForm import payment.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class ImportPaymentOptionForm extends BaseForm
{
    public function configure()
    {   
        $this->myUserObj = $this->getOption('myUserObj');

        // matched invoice collection
        $invoiceColl = $this->getOption('invoiceColl');
        
        // creates form for collection       
        $invoiceCollForm = new sfForm();
        
        // foreach invoice generate embeded invoice form
        foreach($invoiceColl as $key => $rec)
        {
            $importPaymentInvoiceFormObj = new ImportPaymentInvoiceForm(
                    array('id' => $rec->getId()),
                    array('myUserObj' => $this->myUserObj));
            $invoiceCollForm->embedForm($key, $importPaymentInvoiceFormObj);
            $this->embedForm('invoiceColl', $invoiceCollForm);           
        }
    
        // sets default messages
        $this->setMessages();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
}