<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ImportPaymentRowCollForm import payment.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class ImportPaymentRowForm extends BaseForm
{    
    public function configure()
    {
        $this->myUserObj = $this->getOption('myUserObj');

        // matched invoice collection
        $invoiceColl = $this->getOption('invoiceColl');
        
        // gets row id
        $rowId = $this->getOption('rowId');
        
        // gets active
        $active = $this->getOption('active');

        $importPaymentInvoiceCollFormObj = new ImportPaymentInvoiceCollForm(
                        array('active' => $active,
                            'rowId' => $rowId), 
                        array('invoiceColl' => $invoiceColl,
                            'myUserObj' => $this->myUserObj));

        $this->embedForm($rowId, $importPaymentInvoiceCollFormObj);


        // sets default messages
        $this->setMessages();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
}