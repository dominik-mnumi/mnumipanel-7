<div id="reports-container">
<?php foreach($payments as $payment): ?>
    <?php
    // gets report month labels
    $paymentId = $payment->getId();
    $monthLabelColl = CashReportTable::getInstance()->getReportMonths($paymentId);
    ?>
    <?php if(isset($monthLabelColl) && $monthLabelColl->count() > 0): ?>
    <div class="block-border margin-top10">
        <div class="block-content form" id="table_form">
            <h1><?php echo __('Reports') . ': ' . $payment->getLabel(); ?></h1>

            <div class="no-margin last-child">
                <div class="block-controls no-margin-bottom"></div>
                <div class="loader margin-top10 margin-bottom-10px">
                    <?php echo image_tag('/images/loader.gif'); ?>
                </div>
                <table id="reports-table_payment-<?php echo $paymentId; ?>" class="table sortable no-margin" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%" class="sorting ">
                                <?php echo __('Year and month'); ?>
                            </th>

                            <th scope="col" class="sorting ">
                                <?php echo __('Reports'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($monthLabelColl as $rec): ?>
                        <?php $yearMonth = $rec->getYearMonth(); ?>
                        <tr>
                            <td>
                                <?php echo $yearMonth; ?>
                                <ul>
                                    <li>[<?php echo link_to(__('monthly report'), 'cashReportMonthlyPrint',
                                        array('paymentId' => $paymentId, 'date' => $yearMonth)); ?>]</li>
                                    <li>[<?php echo link_to(__('all documents'), 'cashReportMonthlyDocsPrint',
                                        array('paymentId' => $paymentId, 'date' => $yearMonth)); ?>]</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                <?php foreach(CashReportTable::getInstance()->getDailyReportsOfYearAndMonth($paymentId, $yearMonth) as $rec2): ?>

                                    <?php
                                        $currentReportClass = $rec2->isCurrentReport($paymentId, $reportDate) ? 'selected-a' : '';
                                        $created_at = $rec2->getDateTimeObject('created_at');
                                    ?>
                                    <li>
                                    <a  class="filter-daily-docs-click <?php echo $currentReportClass; ?>" href="#" 
                                        data-payment-id="<?php echo $paymentId; ?>"
                                        data-date="<?php echo $created_at->format('Y-m-d'); ?>">
                                            <?php echo __('day').' '.$created_at->format('d'); ?>:
                                            <em>
                                                <?php echo $sf_user->formatCurrency($rec2->getDailyBalance()); ?>
                                            </em>
                                    </a>
                                    <?php echo link_to('[R]', url_for('cashReportDailyPrint', $rec2)); ?>;
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php endforeach; ?>
</div>