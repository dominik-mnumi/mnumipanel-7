<div title="<?php echo __('Attachment'); ?>">
    <p class="validateTips"></p>
    <?php echo $form['_csrf_token']->render(); ?>
    <?php echo $form['id']->render(); ?>
    <div class="block-border">
        <div class="block-content form no_border">                      
            <p>
                <?php echo $form['filename']->renderLabel(); ?>
                <?php echo $form['filename']->render(); ?>
            </p>    
            <p>
                <?php echo $form['file']->renderLabel(); ?>
                <?php echo $form['file']->render(); ?>
            </p> 
            <p>
                <label><?php echo __('Download current file'); ?></label>
                <a href="<?php echo url_for('@clientAttachmentDownload?id='.$form->getObject()->getId()); ?>">
                    <?php echo $form->getObject()->getFilename(); ?>                          
                </a> 
            </p>
        </div>
    </div>
</div>


