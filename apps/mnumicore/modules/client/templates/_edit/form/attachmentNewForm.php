<form id="dialog-add-new-attachment-form" action="<?php echo url_for('reqEditClientNewAttachmentForm', 
        array('model' => 'ClientAttachment', 
            'type' => 'new', 
            'forcePost' => 1)); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Attachment'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p>
                    <?php echo $form['filename']->renderLabel(); ?>
                    <?php echo $form['filename']->render(); ?>
                </p>  
                <p class="required">
                    <?php echo $form['file']->renderLabel(); ?>
                    <?php echo $form['file']->render(); ?>
                </p>    
            </div>
        </div>
    </div>
</form>

