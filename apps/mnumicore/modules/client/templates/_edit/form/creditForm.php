<form id="dialog-edit-client-credit-form" action="<?php echo url_for('reqEditClientCreditForm', 
        array('model' => 'Client', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Credits'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p class="required">
                    <?php echo $form['pricelist_id']->renderLabel(); ?>
                    <?php echo $form['pricelist_id']->render(); ?>
                </p>                               
                <p class="required">
                    <?php echo $form['credit_limit_amount']->renderLabel(); ?>
                    <?php echo $form['credit_limit_amount']->render(); ?>
                <p>
                <p>
                    <?php echo $form['credit_limit_overdue']->renderLabel(); ?>
                    <?php echo $form['credit_limit_overdue']->render(); ?>
                <p>                          
            </div>
        </div>
    </div>
</form>

