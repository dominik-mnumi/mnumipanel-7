<?php

foreach($formArray as $key => $form)
{
    if('addressData' == $key)
    {
        foreach($form as $key2 => $addressForm)
        {
            if($addressForm->hasGlobalErrors())
            {
                echo $addressForm->renderGlobalErrors();
            }
            if($addressForm instanceof sfFormDoctrine)
            {
                foreach($addressForm->getFormFieldSchema() as $formField)
                {
                    echo $formField->renderError();
                }
            }
        }
    }
    else
    {
        if($form->hasGlobalErrors())
        {
            echo $form->renderGlobalErrors();
        }
        if($form instanceof sfFormDoctrine)
        {
            foreach($form->getFormFieldSchema() as $formField)
            {
                echo $formField->renderError();
            }
        }
    }
}
?>





