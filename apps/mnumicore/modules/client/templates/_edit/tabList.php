<?php $clientObj = $sf_data->getRaw('clientObj'); ?>
<div class="block-controls">
    <ul class="controls-tabs same-height with-children-tip">
        <li title="<?php echo __('Customer edit'); ?>" class="<?php echo (isset($currentTab) && $currentTab == 'tab-edit') ? 'current' : ''; ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#tab-edit" title="<?php echo __('Customer edit'); ?>">
                <img src="/images/icons/order/user.png" width="24" height="24">                               
            </a>
        </li>
        <li title="<?php echo __('Customer packages'); ?>" class="<?php echo (isset($currentTab) && $currentTab == 'tab-packages') ? 'current' : ''; ?>">
            <a href="<?php echo url_for('clientPackage', $clientObj); ?>#tab-packages" title="<?php echo __('Customer packages'); ?>">
                <img src="/images/icons/order/package.png" width="24" height="24">                               
            </a>
        </li>
        <li title="<?php echo __('Customer orders'); ?>" class="<?php echo (isset($currentTab) && $currentTab == 'tab-orders') ? 'current' : ''; ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#tab-orders" title="<?php echo __('Customer orders'); ?>">
                <img src="/images/icons/order/order-user.png" width="24" height="24">
            </a>
        </li>
        <?php if(!$defaultDisableAdvancedAccountancy): ?>
        <li title="<?php echo __('Customer invoices'); ?>" class="<?php echo (isset($currentTab) && $currentTab == 'tab-invoices') ? 'current' : ''; ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#tab-invoices" title="<?php echo __('Customer invoices'); ?>">
                <img src="/images/icons/order/invoice-user.png" width="24" height="24">
            </a>
        </li>
        <?php endif; ?>
    </ul>
    
    <!-- extra switch -->
    <?php if(isset($currentTab) && $currentTab == 'tab-packages'): ?>
    <ul class="controls-buttons margin-right20px">
        <li>
            <label for="checkboxFilter"><?php echo __('Show inactive packages'); ?></label>
            <?php $checkboxWidget = new sfWidgetFormInputCheckbox(array(), array('class' => 'mini-switch', 'value' => 1)); ?>
            <?php echo $checkboxWidget->render('checkboxFilter', !$filtered); ?>         
        </li>
    </ul>
    <?php endif; ?>
</div>

<!-- sets slot -->
<?php extendSlot('actionJavascript') ?>
<script type="text/javascript">
    jQuery(document).ready(function($) 
    {      
        $("#checkboxFilter").change(function()
        {
            var filtered = 0;
            if(!$(this).is(":checked"))
            {
                filtered = 1;
            }
            
            var href = "?filtered=" + filtered + "#tab-packages";
            
            window.location = href;
        });
    });
</script>
<?php end_slot() ?>
