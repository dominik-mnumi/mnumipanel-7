<?php use_helper('openModal')?>
<?php if(count($clientObj->getToBookOrderPackages())): ?>
<h2 class="client-order-header"><?php echo __('Customer orders (to book)') ?></h2>
<form action="<?php echo url_for('clientInvoiceAdd', $clientObj); ?>" method="post" accept-charset="utf-8" id="to-book-form">
    <table id="extended_order_list" class="table sortable" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th class="black-cell sorting_disabled" style="width: 16px; ">
                  <span class="success"></span>
              </th>
              <th>             
                  <?php echo __('Number') ?>
              </th>
              <th class="order-align-center">                 
                  <?php echo __('Photo') ?>
              </th>
              <th>
                  <?php echo __('Name') ?>
              </th>
              <th>
                  <?php echo __('Status') ?>
              </th>
              <th>
                  <?php echo __('Created at') ?>
              </th>
              <th>
                  <?php echo __('Client') ?>
              </th>
              <th>
                  <?php echo __('Price') ?>
              </th>
              <th scope="col" class="table-actions">
                  <?php echo __('Actions') ?>
                </th>
            </tr>
        </thead>
        <?php foreach($clientObj->getToBookOrderPackages() as $package): ?>
        <tbody>
            <tr>
                <td class="table-check-cell to-book-package-td">
                    <input type="checkbox" name="packageId[]" value="<?php echo $package->getId(); ?>" checked />
                </td>
                <td colspan="8" class="to-book-package-td">
                    <?php echo link_to($sf_user->getSystemDate($package->getCreatedAt(), OrderPackageTable::$packageTimeFormat), 'client_package_details', $package); ?>:
                    <?php echo $package->getWantInvoice() ? __('Invoice') : __('Receipt'); ?>,
                    <?php echo ($package->hasPayment()) ? $package->getPayment()->getName() : ''; ?>,
                    <?php echo ($package->isPaid()) ? __('paid') : __('not paid'); ?>
                </td>
            </tr>
            <?php foreach($package->getOrders() as $rec): ?>
            <tr class="odd">
                <td></td>
                <td><?php echo $rec->getId(); ?></td>
                <td><img src="<?php echo $rec->getPhotoWithPath(); ?>" /></td>
                <td><?php echo link_to($rec->getName(), 'orderListEdit', array('id' => $rec->getId())); ?></td>
                <td><?php include_partial('order/status', array('row' => $rec)) ?></td>
                <td><?php include_partial('order/accepted_at', array('row' => $rec)) ?></td>
                <td><?php include_partial('order/delivery', array('row' => $rec)) ?></td>
                <td><?php include_partial('order/price', array('row' => $rec)) ?></td>
                <td class="table-actions">
                    <a href="<?php echo url_for('orderListEdit', array('id' => $rec->getId())); ?>" title="<?php echo __('Edit'); ?>" class="with-tip">
                        <?php echo image_tag('/images/icons/fugue/pencil.png', array('width' => 16, 'height' => 16)) ?>
                    </a>          
                </td>
            </tr>
            <?php endforeach ?>
        </tbody>
        <?php endforeach ?>
    </table>
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$invoiceCreate)): ?>
    <div>
        <button type="submit">
            <?php echo __('Autogenerate invoice'); ?>
        </button>
    </div>
    <?php endif; ?>
</form>
<script type="text/javascript">
/* Visual addon for better package selection */
function focusPackageTd(input)
{
	tableTd = input.parent().parent().find('td');
    if(tableTd.hasClass('to-book-package-td-checked'))
    {
        tableTd.removeClass('to-book-package-td-checked');
    }
    else
    {
    	tableTd.addClass('to-book-package-td-checked');
    }
}

$(".td-click").click(function(event) {
	var input = $(this).parent().find('input');
	focusPackageTd(input);
	if(input.is(':checked'))
    {
		input.attr('checked', false);
    }
	else
	{
		input.attr('checked', true);
	}
	return false;
});

$(".to-book-package-td input").click(function(event) 
{
    focusPackageTd($(this));
});

/* if no package is select throw alert */
$('#to-book-form').submit(function() 
{
    if (!$("input[@name='packageId']:checked").val()) 
    {
        alertModal('<?php echo __('Autogenerate invoice')?>', '<h2><?php echo __('Select at least one package to generate invoice.')?></h2>');
        return false;
    }
});
</script>
<br/><br/><br/><br/>
<?php endif ?>
<h2 class="client-order-header"><?php echo __('Customer orders') ?></h2>
<?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>