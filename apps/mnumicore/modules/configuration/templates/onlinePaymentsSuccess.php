<form method = "post" class="form" action="<?php echo url_for('configurationOnlinePayments'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationOnlinePayments')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationOnlinePayments')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_online_payments">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Online payments'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('PayU'); ?></legend>
                            <p>
                                <?php echo $form['platnoscipl_pos_id']->renderLabel(); ?>
                                <?php echo $form['platnoscipl_pos_id']->render(); ?>
                                <?php echo $form['platnoscipl_pos_id']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['platnoscipl_pos_auth']->renderLabel(); ?>
                                <?php echo $form['platnoscipl_pos_auth']->render(); ?>
                                <?php echo $form['platnoscipl_pos_auth']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['platnoscipl_key_1']->renderLabel(); ?>
                                <?php echo $form['platnoscipl_key_1']->render(); ?>
                                <?php echo $form['platnoscipl_key_1']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['platnoscipl_key_2']->renderLabel(); ?>
                                <?php echo $form['platnoscipl_key_2']->render(); ?>
                                <?php echo $form['platnoscipl_key_2']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

<script type="text/javascript">
jQuery(document).ready(function($) 
{  
    $("#ConfigurationOnlinePaymentsForm_platnoscipl_pos_auth, #ConfigurationOnlinePaymentsForm_platnoscipl_key_1, #ConfigurationOnlinePaymentsForm_platnoscipl_key_2").click(function()
    {
        $(this).val('');
    });

});
</script>
