<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions) ?>
    <section class="grid_12"> 
        <div class="block-border"><div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __('Coupons'); ?>
                    <a href="<?php echo url_for('@couponCreate') ?>" id="add-coupon-click">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Create coupon') ?>"> <?php echo __('Create coupon'); ?>
                    </a>
                    <a href="<?php echo url_for('@couponCreateMany') ?>" id="add-many-coupons-click">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Create many coupons') ?>"> <?php echo __('Create many coupons'); ?>
                    </a>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
<script type="text/javascript">
    $("#add-many-coupons-click").css("left", $("#add-coupon-click").position().left + $("#add-coupon-click").width() + 24);
</script>