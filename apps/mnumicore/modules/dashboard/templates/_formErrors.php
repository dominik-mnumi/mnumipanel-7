<?php if($form->hasErrors()): ?>
<ul class="message error no-margin">
    <?php foreach($form as $field): ?>
    <?php if($field->hasError()): ?>  
    <li>
        <?php echo __($field->getError()->__toString(), 
            array('%label%' => __($field->renderLabelName()))); ?>
    </li>
    <?php endif; ?>  
    <?php endforeach; ?>
</ul>  
<?php endif; ?>