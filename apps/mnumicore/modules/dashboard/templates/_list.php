<?php use_helper('allTimeBack')?>
<ul class="extended-list no-margin icon-user">
    <?php foreach($orders as $row) : ?>
        <li >
            <span>
                <?php if($row->getInformations()): ?>
                    <img class="with-tip" src="/images/icons/fugue/flag.png" title="<?php echo $row->getInformations()?>"/>
                <?php endif ?>
            </span>
            <span class="dashboard-icon icon">
            <a href="<?php echo url_for('orderListEdit', array('id' => $row->getId())); ?>" >
                <img src="<?php echo $row->getPhotoWithPath('/images/no-preview-small.jpg')  ?>" />
            </a>
            </span>
            <a href="<?php echo url_for('orderListEdit', array('id' => $row->getId())); ?>" >
                <span class="dashboardClientFullname"><?php echo $row->getClient()->getFullName(); ?></span>
            <br />
            <small>
                <b><?php echo $row->getEditLabel() ?></b>
                <br/>
                <span class="dashboardClientText" title="<?php echo $row->getCreatedAt() ?>"><?php echo __('Since') ?>: <?php echo allTimeBack($row->getAcceptedAt()) ?></span>
            </small>
            </a>
            <?php if(isset($showProgressIcons)): ?>
            <div style ="float:right;">
                <img width="32" height="32" src="/images/icons/orderStatus/<?php echo $row->getOrderStatus()->getIcon(); ?>" title="<?php echo __($row->getOrderStatus()->getTitle()) ?>">
            </div>
            <?php endif ?>
        </li>            
    <?php endforeach ?>
</ul>
<div class="message no-margin">
    <?php echo __plural('1 order found', '@count orders found', array('@count' => $orders->count())); ?> 
</div>
