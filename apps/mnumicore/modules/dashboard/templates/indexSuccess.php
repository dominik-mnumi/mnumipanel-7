<?php echo include_partial('dashboard/message'); ?>

<table style="width: 100%; margin-top: 20px; ">
<tr>
<td width="50%" >
    <section class="grid_6" style="width: 99%;">
        <div class="block-border" >
            <div class="block-content">
                <h1>
                    <?php echo __('New orders'); ?>
                </h1>
                <?php echo include_partial('dashboard/list', array(
                            'orders' => $sf_data->get('newOrders'))); ?>
            </div>
        </div>
    </section>
    
    <section class="grid_6" style="width: 99%;">
        <div class="block-border" >
            <div class="block-content">
                <h1>
                    <?php echo __('Waiting orders'); ?>
                </h1>
                <?php echo include_partial('dashboard/list', array(
                            'orders' => $sf_data->get('waitingOrders'))); ?>
            </div>
        </div>
    </section>
</td>
<td width="50%" style="vertical-align: top">
    <section class="grid_6" style="width: 99%;">
        <div class="block-border" >
            <div class="block-content" >
                <h1>
                    <?php echo __('In progress'); ?>
                </h1>
                <?php echo include_partial('dashboard/list', array(
                            'orders' => $sf_data->get('printedOrders'), 'showProgressIcons' => true)); ?>   
            </div>             
        </div>        
    </section>
</td>
</tr>
</table>
