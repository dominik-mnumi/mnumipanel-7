<?php

/**
 * invoice actions.
 *
 * @package    mnumicore
 * @subpackage invoice
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoiceActions extends complexTablesActions
{
    /**
     * Executes index action
     *
     * @param sfWebRequest $request A request object
     * @return string
     */
    public function executeIndex(sfWebRequest $request)
    {
        parent::invoiceList($request);

        $this->customQuery = InvoiceTable::getInstance()->getLatestQuery();
        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    public function executeEdit(sfWebRequest $request)
    {
        $invoiceObj = $this->getRoute()->getObject();
        
        $this->redirect('clientInvoiceEdit', 
                array('client_id' => $invoiceObj->getClient()->getId(),
                    'id' => $invoiceObj->getId()));
    }

    public function executeFiscalPrint(sfWebRequest $request)
    {
        /** @var Invoice $object */
        $object = $this->getRoute()->getObject();

        if($object->getToFiscalPrint() == false) {
            $object->setToFiscalPrint(true);
            $object->save();

            $dataInfoArr = array(
                'message' => 'Document successful submitted to fiscal printer',
                'messageType' => 'msg_success',
            );
        } else {
            $dataInfoArr = array(
                'message' => 'Document was already printed in fiscal printer',
                'messageType' => 'msg_error',
            );
        }

        // sets flash
        $this->getUser()->setFlash('info_data', $dataInfoArr);

        return sfView::NONE;
    }

    /**
     * Ask for fiscal printer status
     *
     * @param sfWebRequest $request
     * @return mixed
     */
    public function executeFiscalPrinterSlip(sfWebRequest $request)
    {
        try {
            $this->checkConnectionKey($request->getParameter('secret'));
        } catch (InvalidArgumentException $e) {
            $this->getResponse()->setStatusCode(401);
            return $this->renderText($e->getMessage());
        }

        $invoice = InvoiceTable::getInstance()->getWaitingToFiscalPrinter();

        if ($invoice == null) {
            return sfView::NONE;
        }

        $this->invoice = $invoice;

        $slip = $invoice->getFiscalPrintSlip();

        $this->getResponse()->setHttpHeader('Content-type','application/json');
        return $this->renderText(
            json_encode($slip, JSON_PRETTY_PRINT)
        );
    }

    public function executeFiscalPrinterSlipStatus(sfWebRequest $request)
    {
        try {
            $this->checkConnectionKey($request->getParameter('secret'));
        } catch (InvalidArgumentException $e) {
            $this->getResponse()->setStatusCode(401);
            return $this->renderText($e->getMessage());
        }

        $type = $request->getParameter('type');
        $slipToPrinter = json_decode($request->getParameter('slipToPrinter'), true);
        $error = $request->getParameter('error');

        $documentName = isset($slipToPrinter['reference']) ? $slipToPrinter['reference'] : false;

        if(!$documentName) {
            $this->forward404('Document ' . $documentName . ' does not exist.');
        }

        /** @var Invoice $invoice */
        $invoice = InvoiceTable::getInstance()->findOneByName($documentName);

        if(!$invoice) {
            $this->forward404('Could not find ' . $documentName . ' in database.');
        }

        if($type == 'success')
        {
            $invoice->setPrintAt(new Doctrine_Expression('NOW()'));
        }
        else
        {
            $invoice->setFiscalPrintNote(date('Y-m-d H:i:s') . ': ' . $error);
        }

        $invoice->save();

        $result = array(
            'reference' => $documentName,
            'status' => $type,
        );

        return $this->renderText(
            json_encode($result, JSON_PRETTY_PRINT)
        );
    }

    /**
     * Validates connection key (used for FiscalPrinterSlip)
     *
     * @param string $connection_key
     * @throws Exception
     * @return true
     */
    protected function checkConnectionKey($connection_key)
    {
        if($connection_key != substr(sfConfig::get('sf_csrf_secret'), -6))
        {
            throw new InvalidArgumentException('Incorrect connection key. Check your secret key. ');
        }

        return true;
    }
    
    /**
     * Executes delete action. Deletes invoice and referenced invoice items.
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {     
        throw new Exception("Invoices can't be deleted.");
    }
    
    /**
     * Processes form binding and validation.
     * 
     * @param sfWebRequest $request
     * @param sfForm $form
     * @return boolean
     */
    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        if($request->hasParameter($form->getName()))
        {
            $paramArray = $request->getParameter($form->getName());

            if($form->isMultipart())
            {
                $fileArray = $request->getFiles($form->getName());
                $form->bind($paramArray, $fileArray);
            }
            else
            {
                $form->bind($paramArray);
            }          
        }
        
        return $form;
    }
    
    /**
     * Changing invoice status
     * 
     * @param sfRequest $request A request object
     */
    public function executeChangeStatus(sfWebRequest $request)
    {
        // gets object
        $invoiceObj = $this->getRoute()->getObject();
        
        if($invoiceObj->hasChildren())
        {
            throw new Exception('This invoice has children invoice. You cannot change its status.');
        }
        
        $invoiceStatus = $request->getParameter('invoice_new_status');

        // changes old invoice to unpaid
        $this->changeStatus($invoiceObj, $invoiceStatus);
        
        $this->redirect('clientInvoiceEdit', array('client_id' => $invoiceObj->getClient()->getId(),
                                                   'id' => $invoiceObj->getId()));
    }

    /**
     * Executes Changelog action. List of invoice changes.
     *
     * @param \sfWebRequest $request A request object
     */
    public function executeChangelog(sfWebRequest $request)
    {
        /** @var Invoice $invoice */
        $invoice = $this->getRoute()->getObject();
        
        $this->differences = $invoice->getChanges();
    }
    
    /**
     * Executes Clone action. For invoice correction
     *
     * @param sfWebRequest $request A request object
     * @throws Exception
     */
    public function executeClone(sfWebRequest $request)
    {
        $invoiceObj = $this->getRoute()->getObject();
        
        if(!($invoiceObj instanceof Invoice))
        {
            throw new Exception('Instance of invoice required');
        }

        $newInvoiceObj = $invoiceObj->copyWithItems($request->getParameter('status'));

        // if parent invoice is paid then add loyalty points
        if($invoiceObj->isPaid())
        {
            if($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$PRELIMINARY_INVOICE)
            {
                $newInvoiceObj->setInvoiceStatusName(InvoiceStatusTable::$paidFromPreliminaryInvoice);
                $newInvoiceObj->save();
            }
            
            $newInvoiceObj->addLoyaltyPoints();
        }
        
        $this->redirect('clientInvoiceEdit', 
                array('client_id' => $newInvoiceObj->getClient()->getId(), 
                    'id' => $newInvoiceObj->getId()));
    }
    
    /**
     * Executes InvoiceFromReceipt action. Generates invoice from receipt.
     *
     * @param sfRequest $request A request object
     */
    public function executeInvoiceFromReceipt(sfWebRequest $request)
    {
        // gets receipt object from route
        $receiptObj = $this->getRoute()->getObject();
        
        if($receiptObj->getInvoiceTypeName() != InvoiceTypeTable::$RECEIPT)
        {
            throw new Exception('Receipt is not found.');
        }
        
        // copy receipt and create invoice with "invoice" invoice type
        $newInvoice = $receiptObj->copyWithItems(InvoiceTypeTable::$INVOICE);
       
        // if invoice is already paid
        if($receiptObj->isPaid())
        {
            // changes old invoice to unpaid
            $newInvoice->setInvoiceStatusName(InvoiceStatusTable::$paidFromReceipt);
            $newInvoice->save();
            $newInvoice->addLoyaltyPoints();
        }
        
        $this->redirect('clientInvoiceEdit', 
                array('client_id' => $newInvoice->getClient()->getId(), 
                    'id' => $newInvoice->getId()));      
    }
    
    /**
     * Export invoice list as file.
     * 
     * @param sfWebRequest $request
     * @return string
     */
    public function executeExportInvoiceList(sfWebRequest $request)
    {   
        // sets escaping strategy of for this action
        sfConfig::set('sf_escaping_strategy', false);

        $exportEncoding = sfConfig::get('app_default_export_encoding', 'windows-1250');
        $sfFormat = $request->getParameter('sf_format');     
        $filterParamArr = $request->getParameter('invoice_filters');   
        
        // filter query
        $filterForm = new InvoiceFormFilter();    
        $filterForm->bind($filterParamArr);
        if($filterForm->isValid())
        {   
            $query = $filterForm->getQuery();
        }

        $timeStart = microtime(true);
        $invoiceColl = $query->execute();
        $timeEnd = microtime(true);
        
        $content = $this->getPartial('invoice/exportInvoiceList', 
                array('invoiceColl' => $invoiceColl,
                    'buildTime' => $timeEnd - $timeStart));

        $content = iconv('UTF-8', $exportEncoding, $content);

        $response = $this->getResponse();
        $response->setContentType('text/'.$sfFormat);
        $response->setHttpHeader('Content-Disposition', 
                'attachment; filename=invoiceListTo'
                .ucfirst($sfFormat)
                .'_'.date('Y-m-d_G:i:s').'.'.$sfFormat);
        $response->setContent($content);
   
        return sfView::NONE;
    }

    /**
     * Changes invoice status and prepares flash messages.
     * 
     * @param Invoice $invoiceObj
     * @param string $invoiceStatus 
     */
    protected function changeStatus($invoiceObj, $invoiceStatus)
    {
        $this->forward404Unless($invoiceObj);
        
        if(!$invoiceObj->getPaymentId() 
                && in_array($invoiceStatus, 
                        array(InvoiceStatusTable::$paid,
                            InvoiceStatusTable::$unpaid)))
        {
            $dataInfoArr = array(
                'message' => 'Choose payment method first',
                'messageType' => 'msg_error'
            );
            $this->getUser()->setFlash('info_data', $dataInfoArr);
            $this->redirect('clientInvoiceEdit', 
                    array('client_id' => $invoiceObj->getClient()->getId(), 
                        'id' => $invoiceObj->getId()));
        }
 
        $paymentStatusMappingArr = array(
            InvoiceStatusTable::$paid => PaymentStatusTable::$paid,
            InvoiceStatusTable::$unpaid => PaymentStatusTable::$waiting,
        );

        $invoiceObj->setInvoiceStatusName($invoiceStatus);

        // if not InvoiceStatusTable::$canceled
        if(array_key_exists($invoiceStatus, $paymentStatusMappingArr))
        {
            // validate if we could pay the preliminary invoice
            if($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$PRELIMINARY_INVOICE)
            {
                $orderPackageColl = $invoiceObj->getOrderPackages();

                $invoiceSummaryNetPrice = (float) $invoiceObj->getPriceNet();

                $packageSummaryNetPrice = 0;

                // check if Packages have correct Payment status
                foreach($orderPackageColl as $package)
                {
                    if($package->getPaymentStatusName() == $paymentStatusMappingArr[$invoiceStatus])
                    {
                        $dataInfoArr = array(
                            'message' => 'You cannot pay/unpay this preliminary invoice.',
                            'messageType' => 'msg_error',
                        );

                        $this->getUser()->setFlash('info_data', $dataInfoArr);
                        $this->redirect('clientInvoiceEdit',
                            array('client_id' => $invoiceObj->getClient()->getId(),
                                'id' => $invoiceObj->getId()));

                        return;

                    }
                }

                // check if the Invoice and Packages price is same
                foreach($orderPackageColl as $package)
                {
                    $package->calculatePrice($package->getOrdersBasketQuery());
                    $packageSummaryNetPrice += (float) $package->getSummaryTotalAmount();
                }

                $allowForce = (bool) $this->getRequest()->getParameter('force', false);

                if ((count($orderPackageColl) > 0) &&
                    ($invoiceStatus == InvoiceStatusTable::$paid) &&
                    ($allowForce === false) &&
                    ($invoiceSummaryNetPrice <> $packageSummaryNetPrice))
                {
                    sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));

                    $dataInfoArr = array(
                        'message' => 'You cannot pay/unpay preliminary invoice, when invoice price (%invoicePrice%) is different than summary package price (%packagePrice%).',
                        'messageType' => 'msg_error',
                        'messageParam' => array(
                            '%invoicePrice%' => $this->getUser()->formatCurrency($invoiceSummaryNetPrice),
                            '%packagePrice%' => $this->getUser()->formatCurrency($packageSummaryNetPrice),
                        ),
                    );
                    $this->getUser()->setFlash('allowForcePayment', true);
                    $this->getUser()->setFlash('info_data', $dataInfoArr);
                    $this->redirect('clientInvoiceEdit',
                        array('client_id' => $invoiceObj->getClient()->getId(),
                            'id' => $invoiceObj->getId()));

                    $this->getUser()->setFlash('allowForcePayment', true);

                    return;
                }
            }

            // changes payment status for connected packages
            foreach($orderPackageColl as $rec)
            {
                $newStatus = $paymentStatusMappingArr[$invoiceStatus];
                $rec->setPaymentStatusName($newStatus);
                $rec->save();
            }
        }

        $invoiceObj->save();

        $messages = array(InvoiceStatusTable::$paid => 'Payed successfull.', 
                          InvoiceStatusTable::$unpaid => 'Unpaid successfull.',
                          InvoiceStatusTable::$canceled => 'Canceled successfull.', 
                          InvoiceStatusTable::$block => 'Blocked successfull.');
        
        
        $message = isset($messages[$invoiceStatus]) 
                ? $messages[$invoiceStatus] 
                : 'Saved successfully.';

        $messageParameters = array();

        // cash desk is generated only for paid and unpaid status
        if (in_array($invoiceStatus, array(InvoiceStatusTable::$paid, InvoiceStatusTable::$unpaid)))
        {
            $generatedCashDesk = $invoiceObj->getCashDesks()->getLast();

            //if cash desk was generated
            if($generatedCashDesk)
            {
                $documentName = $generatedCashDesk->getFormattedNumber();

                $message .= ' Generated document: %docName%.';
                $messageParameters['%docName%'] = $documentName;
            }
        }
        
        // points message section
        if($invoiceStatus == InvoiceStatusTable::$paid)
        {
            $totalInvoicePoints = $invoiceObj->getTotalLoyaltyPoints();

            $message .= ' Loyalty points for invoice paid added: %loyaltyPoints%.';
            $messageParameters['%loyaltyPoints%'] = $totalInvoicePoints;
        }
        elseif($invoiceStatus == InvoiceStatusTable::$unpaid)
        {
            $message .= ' Loyalty points for invoice deleted.';
        }
        
        $dataInfoArr = array(
                'message' => $message,
                'messageType' => 'msg_success',
                'messageParam' => $messageParameters);

        // sets flash
        $this->getUser()->setFlash('info_data', $dataInfoArr);

    }

}
