<ul class="simple-list with-icon">
    <li class="icon-date"><span><?php echo $sf_user->getSystemDate($row->getCreatedAt(), 'f') ?></span></li>
    <?php if($row->getCreatedAt() != $row->getUpdatedAt()): ?>
        <li class="icon-date"><span><?php echo __('edited')?>: <?php echo $sf_user->getSystemDate($row->getUpdatedAt(), 'f') ?></span></li>
    <?php endif ?>
</ul>