<?php 
echo __('Invoice number').';';
echo __('Seller name').';';
echo __('Seller address').';';
echo __('Seller postcode').';';
echo __('Seller city').';';
echo __('Seller tax id').';';
echo __('Seller bank name').';';
echo __('Seller bank account').';';
echo __('Client name').';';
echo __('Client address').';';
echo __('Client postcode').';';
echo __('Client city').';';
echo __('Client tax id').';';
echo __('Invoice net amount').';';
echo __('Invoice tax amount').';';
echo __('Invoice gross amount').';';
echo __('Paid at').';';
echo __('Orders').';';
echo __('Order names')."\n";

foreach($invoiceColl as $invoice) {
    echo $invoice->getName().';';
    echo $invoice->getSellerName().';';
    echo $invoice->getSellerAddress().';';
    echo $invoice->getSellerPostcode().';';
    echo $invoice->getSellerCity().';';
    echo $invoice->getSellerTaxId().';';
    echo $invoice->getSellerBankName().';';
    echo $invoice->getSellerBankAccount().';';
    echo $invoice->getClientName().';';
    echo $invoice->getClientAddress().';';
    echo $invoice->getClientPostcode().';';
    echo $invoice->getClientCity().';';
    echo $invoice->getClientTaxId().';';
    echo $invoice->getPriceNet().';';
    echo $invoice->getPriceTax().';';
    echo $invoice->getPriceGross().';';
    echo $invoice->getPayedAt().';';
    echo $invoice->getOrdersIdString().';';
    echo $invoice->getOrderNamesString()."\n";
}
?>

