<ul class="simple-list with-icon">
    <li class="icon-tags"><span><?php echo __($row->getPayment()); ?></span></li>
    <li class="icon-date"><span><?php echo $sf_user->getSystemDate(!$row->getPayedAt() ? $row->getPaymentDateAt() : $row->getPayedAt()) ?></span></li>
    <li class="icon-file">
        <span>
            <?php if($row->getInvoiceStatusName() == InvoiceStatusTable::$paid): ?>
            <?php echo __('paid'); ?>
            <?php elseif($row->getInvoiceStatusName() == InvoiceStatusTable::$paidFromPreliminaryInvoice): ?>
            <?php echo __('paid from preliminary invoice'); ?>    
            <?php elseif($row->getInvoiceStatusName() == InvoiceStatusTable::$paidFromReceipt): ?>
            <?php echo __('paid from receipt'); ?>
            <?php else: ?>
            <?php echo __('not paid'); ?>
            <?php endif; ?>         
        </span>
    </li>
</ul>