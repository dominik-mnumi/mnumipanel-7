<?php

/**
 * invoiceCost actions.
 *
 * @package    mnumicore
 * @subpackage invoiceCost
 * @author     Michał Kopańko <michal.kopanko@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoiceCostActions extends tablesActions
{
    /**
     * Executes index action. Shows invoice costs list
     *
     * @param sfWebRequest $request A request object
     * @return string
     */    
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Fullname' => array('label' => 'Client name', 'clickable' => true),
            'InvoiceCostBalance' => array('label' => 'Balance'),
            'Invoices' => array('label' => 'Invoices', 'partial' => 'invoices'),
        );
        $this->displayGridFields = array(
            'Client' => array('destination' => 'client_name', 'label' => 'Name'),
            'CreatedAt' => array('destination' => 'keywords', 'label' => 'Created at')
        );
        $this->sortableFields = array('invoice_number', 'created_at', 'updated_at');
        $this->modelObject = 'Client';
        $this->removeAction('delete');
        $this->customQuery = ClientTable::getInstance()
                        ->createQuery('c')
                        ->leftJoin('c.InvoiceCosts ic')
                        ->where('c.id IN (ic.client_id)');
        $this->noActions = true;
        $this->actions['exportToCsv'] = array('manyRoute' => 'invoiceCostListExportCsv',
                                             'label' => 'Export to CSV (all)');
        
        $this->tableOptions = $this->executeTable();

        $this->addClientForm = new InvoiceCostForm();

        if ($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    /**
     * Executes edit action. Shows invoice costs list
     *
     * @param sfRequest $request A request object
     */    
    public function executeEdit(sfWebRequest $request)
    {
        $id = (int)$request->getParameter('id');
        $this->displayTableFields = array(
            'InvoiceNumber' => array('label' => 'InvoiceNumber'),
            'Amount' => array('label' => 'Amount', 'partial' => 'amount'),
            'PaymentDate' => array('label' => 'Payment date'),
            'InvoiceDate' => array('label' => 'Invoice date'),
            'Paid' => array('label' => 'Paid', 'partial' => 'payed'),
        );
        $this->displayGridFields = array(
            'Client' => array('destination' => 'client_name', 'label' => 'Name'),
            'CreatedAt' => array('destination' => 'keywords', 'label' => 'Created at')
        );
        $this->sortableFields = array('invoice_number', 'created_at', 'updated_at');
        $this->modelObject = 'InvoiceCost';
        $this->actions['edit'] = array('label' => 'Edit',
                'route' => 'invoiceCostRecordEdit',
                'icon' => '/images/icons/fugue/pencil.png',
                'attributes' => array('title' => 'Edit'));
        $this->customRoutingParameters = array('id' => $request->getParameter('id'));
        $this->customQuery = InvoiceCostTable::getInstance()->createQuery('ic')
                                        ->where('client_id = ?', $id);
        $this->tableOptions = $this->executeTable();

        $this->addInvoiceForm = new InvoiceCostClientForm();
        $id = $request->getParameter('id');
        $this->addInvoiceForm->setDefaults(array('client_id' => $id));

        if ($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }        
    }    
    
    /**
     * Executes edit record action. Edits specified invoice cost.
     *
     * @param sfRequest $request A request object
     */    
    public function executeEditRecord(sfWebRequest $request)
    {
        // gets form
        $invoiceForm = new InvoiceCostClientForm($this->getRoute()->getObject());
        
        if($this->getRequest()->isMethod('POST'))
        {
            $invoiceForm->bind($this->getRequest()->getParameter($invoiceForm->getName()));
            if($invoiceForm->isValid())
            {
                $invoiceForm->save();
                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Saved successfully.',
                    'messageType' => 'msg_success'));
                $this->redirect('invoiceCostRecordEdit', $invoiceForm->getObject());
            }
        }
      
        // view objects
        $this->invoiceForm = $invoiceForm;
    }    
  
    /**
     * Executes delete action.
     *
     * @param sfRequest $request A request object
     */
    public function executeRecordDelete(sfWebRequest $request)
    {
        $obj = $this->getRoute()->getObject();
        
        if($obj->canDelete())
        {
            if($obj->delete())
            {           
                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Deleted successfully.',
                    'messageType' => 'msg_success',
                ));
            }
            else
            {
                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Deleted unsuccessfully.',
                    'messageType' => 'msg_error',
                ));
            }
        }
        $this->redirect('invoiceCostEdit', $obj->getClient());
    }

    public function executeRecordDeleteMany(sfWebRequest $request)
    {
        $paramArr = $request->getParameter('selected');
        $clientId = InvoiceCostTable::getInstance()->find($paramArr[0])
                    ->getClient()->getId();
        
        $this->modelObject = 'InvoiceCost';
        $this->customRoutingParameters = array('id' => $clientId);
        parent::executeDeleteMany($request);
    }
    
   /**
    * Pay selected method
    * 
    * @param sfRequest $request A request object
    */ 
    public function executePaySelected(sfWebRequest $request)
    {
        if(!$request->hasParameter('invoices'))
        {
            $this->redirect('invoiceCost');
        }
        
        // gets invoices param
        $invoiceColl = InvoiceCostTable::getInstance()->getInvoicesById($request->getParameter('invoices'));

        $totalCost = 0;
        $titleArr = array();
        foreach($invoiceColl as $invoice)
        {
            $totalCost += $invoice->getBalance();
            $titleArr[] = $invoice->getInvoiceNumber();
        }
        
        // gets client object
        $clientObj = $invoiceColl->getFirst()->getClient();
        
        // gets client form
        $clientEditForm = new EditClientGeneralForm($clientObj);
        
        // view objects
        $this->invoiceColl = $invoiceColl;
        $this->totalCost = $totalCost;   
        $this->title = implode(', ', $titleArr);
        $this->clientObj = $clientObj;
        $this->clientEditForm = $clientEditForm;
    }
    
   /**
    * Process form of pay seleted
    * 
    * @param sfRequest $request A request object
    */ 
    public function executePaySelectedProcess(sfWebRequest $request)
    {
        $balance = (float)$request->getParameter('payValue');
        $invoicesId = $request->getParameter('invoicesId');
        if (!$invoicesId)
        {
            $this->redirect('@invoiceCost');
        }
        
        foreach($invoicesId as $invoiceId)
        {
            $invoice = Doctrine::getTable('InvoiceCost')->find($invoiceId); 

            if ($balance == 0)
            {
                break;
            }
            $odds = $invoice->getBalance();
            if($odds <= $balance)
            {
                $balance -= $odds;
            }
            else
            {
                $odds = $balance;
                $balance = 0;
            }
            $invoice->payInvoice($odds);
            $invoice->getClient()->updateInvoiceCostPayment();
        }
        $this->redirect('@invoiceCost');
    }

    /**
     * Executes form process InvoiceCost form
     *
     * @param sfRequest $request A request object
     */
    public function executeInvoiceCostAdd(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    /**
     * Executes form process InvoiceCost form
     *
     * @param sfRequest $request A request object
     */
    public function executeReqClientTransferDetails(sfWebRequest $request)
    {
        $clientId = $request->getParameter('clientId');
        
        return $this->renderPartial('invoiceCost/transferDetails',
                array('clientObj' => ClientTable::getInstance()->find($clientId),
                    'invoiceCount' => 0,
                    'title' => ''));
    }
    
    /**
     * Export invoice cost list as file.
     * 
     * @param sfWebRequest $request
     * @return string
     */
    public function executeExportInvoiceCostList(sfWebRequest $request)
    {
        $exportEncoding = sfConfig::get('app_default_export_encoding', 'windows-1250');
        $sfFormat = $request->getParameter('sf_format');    
        $invoiceCostColl = InvoiceCostTable::getInstance()->createQuery()
                ->execute();

        $content = $this->getPartial('invoiceCost/exportInvoiceCostList', 
                array('invoiceCostColl' => $invoiceCostColl));

        $content = iconv('UTF-8', $exportEncoding, $content);

        $response = $this->getResponse();
        $response->setContentType('text/'.$sfFormat);
        $response->setHttpHeader('Content-Disposition', 
                'attachment; filename=invoiceCostListTo'
                .ucfirst($sfFormat)
                .'_'.date('Y-m-d_G:i:s').'.'.$sfFormat);
        $response->setContent($content);
   
        return sfView::NONE;
    }
}
