<?php

/**
 * notification actions.
 *
 * @package    mnumicore
 * @subpackage notification
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notificationActions extends tablesActions
{

    /**
     * Executes index action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        // @todo Use this module  somewhere in core 3
        $this->displayTableFields = array(
            'Title' => array(),
            'NotificationTypeName' => array('label' => 'Type'),
            'NotificationTemplateName' => array('label' => 'Template',
                'partial' => 'notification/template'),
        );
        $this->displayGridFields = array(
            'Title'  => array('destination' => 'name'),
            'NotificationTypeName' => array('destination' => 'keywords')
        );
        $this->sortableFields = array('title');
        $this->modelObject = 'Notification';
        
        $this->tableOptions = $this->executeTable();
          
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    public function executeCreate(sfWebRequest $request)
    {
        $form = new NotificationForm();
        $this->processForm($form);
        
        //view objects
        $this->form = $form;
        
        $this->setTemplate('edit');
    }
    
    public function executeDelete(sfWebRequest $request)
    {
        $this->getRoute()->getObject()->delete();
        $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('@settingsNotification');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $notificationObj = $this->getRoute()->getObject();

        $form = new NotificationForm($notificationObj);
        $this->processForm($form);

        //view objects
        $this->form = $form;
    }
    
    /**
     * Executes save action. Saves environment state.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeSave(sfWebRequest $request)
    {
        //saves state
        $this->saveEnvironment($request);

        //redirect to list
        $this->redirect('@settingsNotification');
    }

    /**
     * Previews notification templates.
     *
     * @param sfWebRequest $request
     */
    public function executePreview(sfWebRequest $request)
    {
        $notificationArr = $request->getParameter('notification');

        // gets notification template object (NotificationTemplate)
        $templateObj = NotificationTemplateTable::getInstance()
            ->find($notificationArr['notification_template_id']);
        $notificationTemplateClassName = $templateObj->getNotificationClassName();

        // creates notification object (extended from Notifications)
        $notificationTemplateObj = new $notificationTemplateClassName(
            $this->getContext());

        // rendered Twig content
        $content = TwigNotificationManager::getInstance($this->getUser(), 'Twig_Loader_String')
            ->getTwigObj()
            ->render(
                $notificationArr['content'],
                $notificationTemplateObj->getShortcodesAsArgArr());

        $this->setLayout(false);

        // view objects
        $this->content = $content;
    }

    protected function processForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return false;
        }

        $form->bind($this->getRequest()->getParameter($form->getName()));

        if($form->isValid())
        {
            $form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settingsNotificationEdit?id='.$form->getObject()->getId());
        }
    }
    
    /**
     * Builds query
     *
     * @return Doctrine_Query
     */
    protected function buildQuery()
    {
        //gets base query
        $query = NotificationTable::getInstance()->createQuery('p');

        //filter query
        $this->addFilterQuery($query);

        //sort query
        $this->addSortQuery($query);

        return $query;
    }

    /**
     *
     * @param Doctrine_Query $query
     * @return boolean
     */
    protected function addFilterQuery($query)
    {
        //if null then do nothing
        $filterArray = $this->getUser()->getFilters();

        if(null == $filterArray)
        {
            return true;
        }
    }

    /**
     *
     * @param Doctrine_Query $query
     * @return boolean
     */
    protected function addSortQuery($query)
    {
        //permitted columns to sort
        $permittedColumnArray = array('title', 'notification_type_id', 'notification_template_id');

        //if array null then do nothing
        $sortArray = $this->getUser()->getSort();

        if(array(null, null) !== $sortArray && in_array($sortArray[0], $permittedColumnArray))
        {
            $query->orderBy($sortArray[0].' '.$sortArray[1]);
        }

        return true;
    }
    
    /**
     * Method save state of environment
     *
     * @param sfWebRequest $request
     */
    private function saveEnvironment(sfWebRequest $request)
    {
        //sets sort
        if($request->hasParameter('sort') && $request->hasParameter('order'))
        {
            //gets paramater from GET
            $sortArray[] = $request->getParameter('sort');
            $sortArray[] = $request->getParameter('order');
            $this->getUser()->setSort($sortArray);
        }     
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Notification';
        parent::executeDeleteMany($request);
    }

}
