<?php

/**
 * notification components.
 *
 * @package    mnumicore
 * @subpackage notification
 */
class notificationComponents extends sfComponents
{
    /**
     * Executes generateOrderFormSteps action
     *
     * @param sfWebRequest $request A request object
     */
    public function executeShortcodeInfo(sfWebRequest $request)
    {
        // global tags
        $notification = new Notifications($this->getContext());
        $this->globalTags = $notification->getGlobalShortcodes();

        // local tags
        $this->templates = NotificationTemplateTable::getInstance()->findAll();
    }

}
