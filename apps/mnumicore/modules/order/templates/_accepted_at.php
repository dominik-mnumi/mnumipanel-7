<?php use_helper('allTimeBack'); ?>
<?php if($row->getAcceptedAt()) : ?>
    <div class="humanTime"><?php echo allTimeBack($row->getAcceptedAt()); ?> <?php echo __('ago');?>.</div>
    <div class="date"><?php echo $row->getAcceptedAt(); ?></div>
<?php endif ?>
<small>
<?php if($row->getShopName() != ''): ?>
    <?php echo __('Added from'); ?>: <?php echo $row->getShop(); ?>
<?php else: ?>
    <?php echo __('Added by'); ?>: <?php echo __('Office'); ?>
<?php endif; ?>
</small>