<form id="dialog-order-notice-form" action="<?php echo url_for('clientNoticeAdd', array('id' => $clientId));?>" enctype="multipart/form-data" method="post" style="display: none;">
    <?php echo $form['_csrf_token']->render(); ?>
    <?php echo $form['id']->render(); ?>
    <div class="block-border">
        <div class="block-content form no_border">                      
            <p class="required">
                <?php echo $form['notice']->renderLabel(); ?>
                <?php echo $form['notice']->render(); ?>
            </p>  
        </div>
    </div>
</form>

