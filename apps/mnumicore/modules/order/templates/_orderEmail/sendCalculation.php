<br/>
<div id="send_calculation_form" style="display: none; text-align: left;">
    <div class="send-errors"></div>
    <br/>
    <form action="<?php echo url_for("orderSendEmailCalculation"); ?>" method="post" id="form_send">
        <?php echo $form->renderHiddenFields(); ?>
        <b><?php echo $form["to"]->renderLabel(); ?></b><br/>
        <?php echo $form["to"]->render(); ?>
        <?php echo $form["to"]->renderError(); ?>
        <br /><br />
        <b><?php echo $form["subject"]->renderLabel(); ?></b><br/>
        <?php echo $form["subject"]->getWidget()->render('send_calculation[subject]',
                __('Calculation from').' '.$sf_user->getSystemDate(null, 'I')); ?>
        <?php echo $form["subject"]->renderError(); ?>
        <br /><br />
        <h3><?php echo __("Email preview"); ?></h3><br/>
        <?php echo $form["calculation"]->render(); ?>
        <?php echo $form["calculation"]->renderError(); ?>
        <br />        
    </form>

</div>


<script type="text/template" id="email-message-form"><?php echo Cast::html2Text(sfOutputEscaper::unescape($calculationSendTemplate)); ?></script>


<!-- sets slot -->
<?php extendSlot("actionJavascript") ?>
<script type="text/javascript">
    /**
     * Refreshes email content.
     */
    function refreshEmailBody()
    {
        var content = _.template($('#email-message-form').html(), {collection: calcItems, calculation: CalculationApp });
		$(".modal-content #email_calculation_textarea").html(content);
    }
    
    function sendEmail()
    {
        $(".modal-content #email_calculation").val($(".modal-content #calculation_details").html());
        $(".modal-content .send-errors").html("");   

        var form = $(".modal-content #form_send");
        var data = form.serialize();
        var uri =  form.attr("action");
        var method =  form.attr("method");
        $.ajax(
        {
            type: method,
            url: uri,
            data: data,
            dataType: "json",
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert("<?php echo __("There was error. Please try again later.") ?>");
            },
            success: function(data)
            {
                modalFooterLoader(false);
                if(!data.result.success)
                {
                    var errors = data.result;

                    $(".modal-content .send-errors").html(renderError(errors));
                }
                else
                {
                    $(".modal-content #send_calculation_form").html('<ul class="message success grid_12"><li><?php echo __("Email has been sent.") ?></li></ul>');                                   
                    $("#modal .block-footer button:first").remove();
                }
            }
        });  
    }
    
    /**
     * Switches modal footer loader.
     */
    function modalFooterLoader(switchValue)
    {
        if(switchValue)
        {
            $(".block-content .block-footer").prepend(
                        '<?php echo image_tag('/images/loader.gif', 
                                array('width' => 30,
                                    'class' => 'modal-footer-loader')); ?>');
            $(".block-content .block-footer button:first").attr('disabled', 'disabled');
        }
        else
        {
            $(".block-content .block-footer img").remove();
            $(".block-content .block-footer button:first").removeAttr('disabled');
        }
    }
    
    jQuery(document).ready(function($) 
    {   
        /**
         * Send button action (form shows).
         */
        $("#sendBtn").click(function() 
        {         
            $.modal(
            {
                content: $("#send_calculation_form"),
                title: "<?php echo __("Send calculation"); ?>",
                maxWidth: 500,
                maxHeight: 450,
                resizable: false,
                buttons: 
                {
                    "<?php echo __("Send calculation"); ?>": function(win) 
                    {
                        modalFooterLoader(true);
                        saveOrder(null, sendEmail);
                    },
                    "<?php echo __("Close"); ?>": function(win) 
                    {
                        win.closeModal();
                    }
                }
            });
            refreshEmailBody();
        });
    });
</script>
<?php end_slot() ?>