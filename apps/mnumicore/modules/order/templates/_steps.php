<?php $statusArr = $sf_data->get('statusArr'); ?>

<div class="float-left with-menu big-button order-status-menu-button next-step save-form"
     data-id="step_<?php echo $orderObj->getId(); ?>"
     rel="<?php echo $statusArr['button']['name']; ?>">
    <h1>
        <?php echo image_tag($statusArr['button']['imagePath']); ?>
        <?php echo __($statusArr['button']['title']); ?>
    </h1>
        
    <div class="menu">
        <?php echo image_tag('menu-open-arrow.png', 
                array(
                    'width' => 16, 
                    'height' => 16)); ?>
        <ul>     
            <!-- next statuses -->
            <?php foreach($statusArr['button']['menu']['nextStatuses'] as $key => $rec): ?>           
            <li class="<?php echo $rec['iconClassLi']; ?>">
                <?php if(empty($rec['disabled'])): ?>
                <a href="<?php echo isset($rec['href']) ? $rec['href'] : '#'; ?>"
                   class="<?php echo isset($rec['cssClassA']) ? $rec['cssClassA'] : 'save-form'; ?>"
                   data-id="step_<?php echo $orderObj->getId(); ?>" 
                   rel="<?php echo $rec['name']; ?>">
                       <?php echo __($rec['title']); ?>
                </a>
                <?php else: ?>
                <?php echo __($rec['title']); ?>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>    
  
            <!-- previous statuses --> 
            <?php foreach($statusArr['button']['menu']['previousStatuses'] as $rec): ?>
            <li class="<?php echo $rec['iconClassLi']; ?>">
                <a href="#"
                   class="save-form"
                   data-id="step_<?php echo $orderObj->getId(); ?>" 
                   rel="<?php echo $rec['name']; ?>">
                       <?php echo __($rec['title']); ?>
                </a>
            </li>          
            <?php endforeach; ?>    
        </ul>
    </div>
</div>

<script type="text/javascript">
    /**
     * Services button click event.
     */
    $(".order-status-menu-button").click(function(e)
    {
        saveOrder(this);        
    });
    
    /**
     * Services submenu click event.
     */
    $(".order-status-menu-button a").click(function(e)
    {
        e.preventDefault();

        if($(this).hasClass('save-form'))
        {
            saveOrder(this);
            return;
        }
        
        if($(this).hasClass('to-invoicing'))
        {
            saveOrder(this, toInvoicing);
            return;
        }    
        
        window.location.href = $(this).attr('href');
        showLoaderAndDisableButtons();
    });
    
    /**
     * Disables li propagation (from custom jQuery behaviour)
     */ 
    $(".order-status-menu-button li").click(function(e)
    {
        e.stopPropagation();
    });
</script>