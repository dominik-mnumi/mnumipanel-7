<?php echo link_to($sf_user->getSystemDate($row->getCreatedAt(), OrderPackageTable::$packageTimeFormat), url_for('client_package_details', $row)); ?><br />

<?php if($row->getOrders()->count()): ?>
<?php foreach($row->getOrders() as $rec): ?>
<?php $orderHrefArr[] = link_to($rec->getId(), 'orderListEdit', $rec); ?>
<?php endforeach; ?>
(<?php echo __('orders'); ?>: <?php echo implode(', ', $orderHrefArr); ?>)
<?php endif; ?>
