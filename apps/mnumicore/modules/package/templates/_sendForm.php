<form id="send-package-form" action="<?php echo url_for('sendPackageForm', 
        array('model' => 'OrderPackage', 
            'type' => 'edit')); ?>" method="post" style="display: none;">
    <div title="<?php echo __('Send package'); ?>">
        <p class="validateTips"></p>
        <?php echo $form->renderHiddenFields() ?>
        <div class="block-border">
            <div class="block-content form no_border">                                                  
                <p class="required">
                    <?php echo $form['transport_number']->renderLabel(); ?>
                    <?php echo $form['transport_number']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>