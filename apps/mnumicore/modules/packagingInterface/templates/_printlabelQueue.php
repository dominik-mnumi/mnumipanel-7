<div id="printlabelTitle" style="display: none;">
    <h3><?php echo __('Printlabel queue') ?></h3>
    <br/>
    <button id="printAgain" class="print-again-button" type="submit"><?php echo __('Print again') ?></button>
    <br/><br/>
</div>
<table id="allPrintlabels" width="100%" cellspacing="0" class="table sortable" style="display:none;">
    <thead>
        <tr>
            <th scope="col" class="sorting_disabled"><?php echo __('Id')?></th>
            <th scope="col" class="sorting_disabled"><?php echo __('Printer name')?></th>
            <th scope="col" class="sorting_disabled"><?php echo __('Status')?></th>
        </tr>
    </thead>
    <tbody></tbody>
</table>