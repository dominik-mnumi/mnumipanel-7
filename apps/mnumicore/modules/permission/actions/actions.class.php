<?php

/**
 * permission actions.
 *
 * @package    mnumicore
 * @subpackage permission
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class permissionActions extends tablesActions
{

    public function executeIndex(sfWebRequest $request)
    {
        if($request->isMethod('post'))
        {
            // saves permissions
            sfGuardGroupPermissionTable::getInstance()
                    ->save($request->getParameter('permission'));
            
            $this->getUser()->setFlash('info_data',
                array('message' => 'Saved successfully.',
                      'messageType' => 'msg_success'));
        }
        
        $groupColl = sfGuardGroupTable::getInstance()->findAll();     
       
        // view objects
        $this->departmentArr = sfGuardPermissionTable::getInstance()->getDepartments();
        $this->groupColl = sfGuardGroupTable::getInstance()->findAll();
        $this->matrix = sfGuardGroupPermissionTable::getInstance()->getMatrix();
        $this->newGroupForm = new sfGuardGroupForm();      
    }

    public function executeDelete(sfWebRequest $request)
    {
        $obj = $this->getRoute()->getObject();
        
        if(!$obj->canDelete())
        {
            throw new Exception('You cannot delete this role');
        }
        
        $obj->delete();
        $this->getUser()->setFlash('info_data',
                array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('permission');
    }

    public function executeReqAddPermissionForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }

    // ajax form actions
    public function executeReqGroupForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }

    public function executeReqGetEditForm(sfWebRequest $request)
    {
        // gets edit group form based on parameter
        $editGroupForm = new sfGuardGroupForm(sfGuardGroupTable::getInstance()
                ->find($request->getParameter('groupId'))); 
        
        return $this->renderPartial('permission/form/editForm', 
                array('form' => $editGroupForm));
    }
    
    private function proceedForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
          $this->redirect('@permissionEdit?id='.$this->form->getObject()->id);
      }
  }

}
