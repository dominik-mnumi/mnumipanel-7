<form id="dialog-edit-group-form" action="<?php echo url_for('reqGroupForm', 
        array('model' => 'sfGuardGroup', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" 
            method="post" style="display: none;">
    <div title="<?php echo __('Edit role'); ?>">
        <p class="validateTips"></p>
        <?php echo $form->renderHiddenFields(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p class="required">
                    <?php echo $form['name']->renderLabel(); ?>
                    <?php echo $form['name']->render(array("class" => "full-width")); ?>
                </p>                               
                <p  class="required">
                    <?php echo $form['description']->renderLabel(); ?>
                    <?php echo $form['description']->render(array("class" => "full-width")); ?>
                <p>
            </div>
        </div>
    </div>
</form>
