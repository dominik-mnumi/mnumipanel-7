<div class="medium-calendar">
    <table cellspacing="0">
        <thead>
            <tr>
                <th class="permission-table-header width25proc"><?php echo __('Permission'); ?></th>
                <?php foreach($groupColl as $rec): ?>
                <th class="permission-table-header" style="width: <?php echo 75/$groupColl->count(); ?>%">
                    <?php echo __($rec->getDescription()); ?>                   
                    <nobr>    
                        <?php if($rec->canEdit()): ?>
                        <a class="dialog-edit-group-click" data-group-id="<?php echo $rec->getId(); ?>" href="#">
                            <?php echo image_tag('/images/icons/fugue/pencil.png', array(16, 16)); ?>
                        </a>    
                        <?php endif; ?>
                        <?php if($rec->canDelete()): ?>
                        <?php echo link_to(image_tag('/images/icons/fugue/cross-circle.png', 
                                array('width' => 16, 'height' => 16)), 
                                'permissionDelete', 
                                $rec, 
                                array('class' => 'delete', 
                                    'onclick' => 'alertModalConfirm(\''.__('Delete item').'\',\'<h3>'.__('Do you really want to delete this item?').'</h3>\', 500, this.href); return false;'));
                        ?>    
                        <?php endif; ?>
                    </nobr>                  
                </th>
                <?php endforeach; ?>          
            </tr>
        </thead>
        <tbody>
            <?php foreach($departmentArr as $rec): ?>
            <tr>                
                <td class="text-left padding-left10px width-auto"><?php echo __($rec); ?></td>  
                <td class="width-auto" colspan="<?php echo $groupColl->count(); ?>"></td>   
            </tr>  
            
            <?php foreach(sfGuardPermissionTable::getInstance()->getByDepartment($rec) as $rec2): ?>
            <tr>                
                <td class="width-auto text-left padding-left30px font-weight-normal">
                    <?php echo __($rec2->getPermissionName()); ?>
                    <span class="description"><?php echo __($rec2->getPermissionDescription()); ?></span>
                </td>   
                <?php foreach($groupColl as $rec3): ?>
                <?php $checkboxWidget = new sfWidgetFormInputCheckbox(); ?>
                <td class="width-auto">
                    <?php echo $checkboxWidget->render('permission['.$rec3->getId().']['.$rec2->getId().']', 
                            $matrix[$rec3->getId()][$rec2->getId()] ?: false,
                            array('value' => 1)); ?></td>
                <?php endforeach; ?>          
            </tr>     
            <?php endforeach; ?>   
            
            <?php endforeach; ?> 
        </tbody>
    </table>
</div>