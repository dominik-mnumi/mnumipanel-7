<?php use_helper('openModal') ?>

<script type="text/javascript">
    var barcodePrefix = '<?php echo sfConfig::get('app_barcode_prefix', '^') ?>';
    var barcodeSufix = '<?php echo sfConfig::get('app_barcode_sufix', '#') ?>';
    var barcodeSize = <?php echo sfConfig::get('app_barcode_size', '8') ?>;
    var barcodeRead = '<?php echo url_for('@barcodePrinter') ?>';
    var activeTime = <?php echo sfConfig::get('app_barcode_active_time', '60') ?>;
    var messageTitle = '<?php echo __('Error') ?>';
    var removeAuthorization = '<?php echo url_for('@barcodeRemoveAuthorization') ?>';
</script>

<div class="printer-interface">
    <div id="page-locked">
        <div class="block">
        	<div id="head">
        		<h1><?php echo __('Page locked') ?></h1>
        	</div>
        	<div id="body" class="big-blue">
        		<?php echo __('Please read your personal barcode to unlock page.') ?>
        	</div>
        </div>
    </div>
    
    <div id="page-authorized">
        <div class="wizard-bg">
            <section>
            <div class="block-border">
            <div class="block-content form">
                <ol class="wizard-steps no-margin">
    				<li style="float: right;"><span id="countdown"><?php echo sfConfig::get('app_barcode_active_time', '60') ?></span> <?php echo __('sec.')?></li>
    			</ol>
            	<h1><?php echo __('Point of Print Workflow')?></h1>
            	<ul id="global-message" class="message no-margin" style="display:none;"></ul>
            	<div id="worklog-wait" class="big-blue"><?php echo __('Please read order barcode to register order as printed.') ?></div>
                <form id="worklog-form" method="post" action="<?php echo url_for('@worklogSave') ?>">
                    <ul id="save-message" class="message no-margin"></ul>
                    <div class="order-status-changed-info"><?php echo __('Order status was changed to "bindery"')?></div>
                    <?php echo $form ?>
                    
                    <table id="previousWorklogs" width="100%" cellspacing="0" class="table sortable">
                        <thead>
                            <tr>
                                <th scope="col" class="sorting_disabled"><?php echo __('Username')?></th>
                                <th scope="col" class="sorting_disabled"><?php echo __('Page count')?></th>
                                <th scope="col" class="sorting_disabled"><?php echo __('Notice')?></th>
                                <th scope="col" class="sorting_disabled"><?php echo __('Created at')?></th>
                            </tr>
                        </thead>
              			<tbody></tbody>
          			</table>
    				
                    </form>
             </div>
             </div>
            </section>
        </div>
    </div>
</div>
<div id="progressAndWait" style="display: none;"></div>