<?php use_helper('allTimeBack') ?>
<?php if($form->isValid()): ?>
<?php
$previousWorklogs = $sf_data->getRaw('previousWorklogs');

foreach($previousWorklogs as $k => $worklog)
{
    $previousWorklogs[$k]['createdAt'] = allTimeBack($worklog['createdAt']) .  __('ago');
}

?>
<?php echo json_encode(array('success' => __('Saved successfully.'), 'previousWorklogs' => $previousWorklogs)) ?>
<?php else: ?>
<?php 
$errors = array();
foreach($form->getErrorSchema()->getErrors() as $error)
{
    $errors[] = __($error->getMessage());
}
echo json_encode(array('errors' => $errors));
?>
<?php endif ?>