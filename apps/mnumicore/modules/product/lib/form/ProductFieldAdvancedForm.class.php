<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldAdvancedForm extends ProductFieldForm
{
    public function configure()
    {
        parent::configure();

        $fieldItemColl = $this->getOption('fieldItemColl') 
                ?: $this->getObject()
                ->getFieldset()
                ->getFieldItemQuery()
                ->execute();

        $this->setWidget('product_field_item', new sfWidgetFormDoctrineChoiceGroupedI18N(
                array('multiple' => true,
                    'model' => 'FieldItem',
                    'group_by' => 'Fieldset',
                    'collection' => $fieldItemColl), // show all field items (with Hidden to: ex. "--Customization--" item) 
                array('style' => 'width: 400px; height: 350px;')));

        // prepares selected choices with relations
        $productFieldItemArr = ProductFieldItemTable::getFieldItemArray(
                        $this->getObject()
                                ->getProductFieldItemWithFieldItemRelationQuery()
                                ->execute());

        $this->getWidget('product_field_item')->setDefault(array_keys($productFieldItemArr));
        $this->getWidget('product_field_item')->setLabel('Available values');
        
        $this->setWidget('default_value', new sfWidgetFormChoice(array(
                'choices' => $productFieldItemArr,
            )));

        // validators
        $fieldItemArr = FieldItemTable::getFieldItemArray($fieldItemColl);

        $this->setValidator('product_field_item', new sfValidatorChoice(
                array('choices' => array_keys($fieldItemArr),
                      'multiple' =>true),
                array('required' => $this->getFieldset()->getLabel().' - '.sfContext::getInstance()->getI18N()->__('"Available values" are required'))));
                //bad solution with sfContext but i have not better idea at this moment

        //post validator 
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
        //checkLabel inherited
        new sfValidatorCallback(array('callback' => array($this, 'checkLabel'))),
        new sfValidatorCallback(array('callback' => array($this, 'canDefaultValue'))))));

        $this->useFields(array('visible', 'change_label_checkbox', 'label', 
            'required', 'product_field_item', 'default_value'));
    }

    protected function doSave($con = null)
    {
        parent::doSave($con);
       
        // prepares selected choices with relations
        $productFieldSelectedItemArr = ProductFieldItemTable::getFieldItemArray(
                        $this->getObject()
                                ->getProductFieldItemWithFieldItemRelationQuery()
                                ->execute());
        
        $productFieldItemArrayExist = array_keys($productFieldSelectedItemArr);
        $productFieldItemArrayForm = ($this->getValue('product_field_item')) ? $this->getValue('product_field_item')
                                                                             : array();

        if(is_array($productFieldItemArrayExist) && is_array($productFieldItemArrayForm))
        {           
            //deletes uneccessary
            $toDeleteArray = array_diff($productFieldItemArrayExist, $productFieldItemArrayForm);
           
            foreach($toDeleteArray as $rec)
            {
                $product_fielditem = ProductFieldItemTable::getInstance()->findOneByFieldItemIdAndProductFieldId($rec, $this->getObject()->getId());
                
                if(! $product_fielditem instanceof ProductFieldItem)
                {
                    throw new Exception('ProductFieldItem does not exist (field_item_id: '.$rec . ', product_field_id: '. $this->getObject()->getId() . ').');
                }
                
                $product_fielditem->delete();
            }

            //add entries to ProductFieldItem
            $toAddArray = array_diff($productFieldItemArrayForm, $productFieldItemArrayExist);
            foreach($toAddArray as $rec)
            {
                $productFieldItemObj = new ProductFieldItem();
                $productFieldItemObj->setProductField($this->getObject());
                $productFieldItemObj->setFieldItemId($rec);
                $productFieldItemObj->save();
            }
        }
        return $this->getObject();
    }

    public function canDefaultValue($validator, $values)
    {
        $availableArray = $values['product_field_item'];
        $emptyFieldId = FieldItemTable::getInstance()->getEmptyField()->getId();
        
        // if only one or more options then empty option added
        if(count($values['product_field_item']) >= 1 
                && empty($values['required'])
                && !empty($values['visible']))
        {   
            $availableArray = array('-1' => $emptyFieldId);
            $availableArray = $availableArray + $values['product_field_item'];
        }

        $i18N = sfContext::getInstance()->getI18N();
        // if required and default value = empty value
        if(!empty($values['required']) 
                && !empty($values['visible'])
                && in_array($emptyFieldId, $values['product_field_item']))
        {
            $error = new sfValidatorError($validator, $i18N->__('%fieldsetLabel% - Field "Available values" is required and it cannot have empty value', 
                    array('%fieldsetLabel%' => $this->getFieldsetLabel())));
            throw new sfValidatorErrorSchema($validator, array('type' => $error));
        }

        // if not visible and default value = empty value
        if(empty($values['visible']) && $values['default_value'] == $emptyFieldId)
        {
           $error = new sfValidatorError($validator, $i18N->__('%fieldsetLabel% - Field "Default value" is required and it cannot have empty value', 
                    array('%fieldsetLabel%' => $this->getFieldsetLabel())));
            throw new sfValidatorErrorSchema($validator, array('type' => $error));
        }
        
        if(!empty($values['default_value']) && !empty($availableArray))
        {
            if(!in_array($values['default_value'], $availableArray))
            {                
                $error = new sfValidatorError($validator, 'Error');
                throw new sfValidatorErrorSchema($validator, array('type' => $error));
            }
        }
        return $values;

    }//end canDefaultValue
}

//end class ProductFieldAdvancedForm
