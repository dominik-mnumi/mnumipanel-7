<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldOtherForm extends ProductFieldAdvancedForm
{
    public function configure()
    {  
        parent::configure();
        
        $this->setWidget('valid', new sfWidgetFormInputHidden(array(), array('class' => 'valid_hidden_field')));
        $this->setValidator('valid', new sfValidatorPass());

        $this->setWidget('default_value', new sfWidgetFormChoice(array(
                'choices' => $this->getObject()->getDefaultValuesChoiceArrayFixed(),
            )));

        // in other tabs is conditional validator
        $this->getValidator('product_field_item')->setOption('required', false);

        $this->mergePostValidator(new sfValidatorAnd(array(
        // checkLabel inherited
        new sfValidatorCallback(array('callback' => array($this, 'checkLabel'))),
        new sfValidatorCallback(array('callback' => array($this, 'checkAvailableValues'))))));
    }
    
    public function checkAvailableValues($validator, $values)
    {
        if($values['valid'] && !$values['product_field_item'])
        {
            $error['product_field_item'] = new sfValidatorError($validator, $this->getFieldset()->getLabel().' - "Available values" are required.');
            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

    protected function doSave($con = null)
    {
        if($this['valid']->getValue() != 1)
        {
            $this->getObject()->delete();
            return true;
        }

        parent::doSave($con = null);
   
        // checks if not required (allows empty field)
        if(!$this->getObject()->getRequired())
        {
            // if empty field has not been saved already
            if(!$this->getObject()->getEmptyField()->count())
            {
                $productFieldItem = new ProductFieldItem();
                $productFieldItem->setProductField($this->getObject());
                $productFieldItem->setFieldItem(FieldItemTable::getInstance()->getEmptyField());
                $productFieldItem->save();
            }

            $this->getObject()->setRequired(true);
        }
        else
        {
            // if empty field has been saved already then delete
            if($this->getObject()->getEmptyField()->count())
            {
                $this->getObject()->getEmptyField()->delete();
            }   
        }
    }
}

