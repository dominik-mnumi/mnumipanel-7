<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldSimpleForm extends ProductFieldForm
{

    public function configure()
    {
        parent::configure();

        $this->getWidget('default_value')->setDefault(1);
        $this->setValidator('default_value', new sfValidatorNumber(
                array('required' => true,
                    'min' => 0,
                    'max' => 9999999),
                array('required' => $this->getFieldset()->getLabel() . ' - '.sfContext::getInstance()->getI18N()->__('required "Default value"'),
                    'invalid' => $this->getFieldset()->getLabel() . ' - '.sfContext::getInstance()->getI18N()->__('invalid "Default value"'),
                    'min' => $this->getFieldset()->getLabel() . ' - '.sfContext::getInstance()->getI18N()->__('"Default value" must be greater than %min%'),
                    'max' => $this->getFieldset()->getLabel() . ' - '.sfContext::getInstance()->getI18N()->__('"Default value" must be less than %max%'))));
                    //bad solution with sfContext but i have not better idea at this moment    

        //post, conditional validator for change label - inherited
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array(
                                'callback'=>array($this, 'checkLabel'))));
        
        $this->useFields(array('visible', 'required', 'change_label_checkbox', 'label', 'default_value'));
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);

        $object->setDefaultValue((int)$object->getDefaultValue());
    }
}

