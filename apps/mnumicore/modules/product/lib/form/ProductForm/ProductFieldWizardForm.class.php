<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of ProductFieldWizardForm
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class ProductFieldWizardForm extends ProductFieldForm
{
    public function configure()
    {
        parent::configure();

        // no embedded forms, no list
        if($this->isNew())
        {
            $this->useFields(array());
        }
        // otherwise
        else
        {
            $form = new sfForm();
            
            // gets wizard collection
            $wizardColl = $this->getProduct()->getProductSortedWizards();
            
            foreach($wizardColl as $key => $rec)
            {
                $wizardForm = new ProductWizardForm($rec);
                $wizardForm->setDefault('valid', 1);
                
                $form->embedForm($key, $wizardForm);
            }

            // if key exist means that there are some entries in database
            $key = (isset($key)) ? $key += 1 : 0;
            $i = $key;

            for($i; $i < $key + 10; $i++)
            {
                $wizardForm = new ProductWizardForm();
                $wizardForm->setDefault('valid', 0);
                
                $form->embedForm($i, $wizardForm);
            }
            
            // embeds wizard subforms
            $this->embedForm('wizard', $form);  
            
            // new wizard dialog
            $this->setWidget('wizard_size_width', new sfWidgetFormInputText(
                    array('label' => 'Width'),
                    array('class' => 'full-width')));
            
            $this->setWidget('wizard_size_height', new sfWidgetFormInputText(
                    array('label' => 'Height'),
                    array('class' => 'full-width')));
            
            $this->setWidget('wizard_crop_top', new sfWidgetFormInputText(
                    array('label' => 'Top',
                          'default' => 0),
                    array('class' => 'full-width')));
            
            $this->setWidget('wizard_crop_bottom', new sfWidgetFormInputText(
                    array('label' => 'Bottom',
                          'default' => 0),
                    array('class' => 'full-width')));
            
            $this->setWidget('wizard_crop_left', new sfWidgetFormInputText(
                    array('label' => 'Left',
                          'default' => 0),
                    array('class' => 'full-width')));
            
            $this->setWidget('wizard_crop_right', new sfWidgetFormInputText(
                    array('label' => 'Right',
                          'default' => 0),
                    array('class' => 'full-width')));
            
            // sets validators
            $this->setValidator('wizard_size_width', new sfValidatorPass());
            $this->setValidator('wizard_size_height', new sfValidatorPass());
            $this->setValidator('wizard_crop_top', new sfValidatorPass());
            $this->setValidator('wizard_crop_bottom', new sfValidatorPass());
            $this->setValidator('wizard_crop_left', new sfValidatorPass());
            $this->setValidator('wizard_crop_right', new sfValidatorPass());
            
            $this->useFields(array('wizard', 'wizard_size_width', 'wizard_size_height',
                'wizard_crop_top', 'wizard_crop_bottom', 'wizard_crop_left',
                'wizard_crop_right'));
        }        
    }

    public function bind(array $taintedValues = null, array $taintedFiles = null)
    {
        // if not new
        if(!$this->isNew())
        {
            if(empty($taintedValues['wizard_crop_top']))
            {
                $taintedValues['wizard_crop_top'] = 0;
            }

            if(empty($taintedValues['wizard_crop_bottom']))
            {
                $taintedValues['wizard_crop_bottom'] = 0;
            }

            if(empty($taintedValues['wizard_crop_left']))
            {
                $taintedValues['wizard_crop_left'] = 0;
            }

            if(empty($taintedValues['wizard_crop_right']))
            {
                $taintedValues['wizard_crop_right'] = 0;
            }
        }
        
        parent::bind($taintedValues, $taintedFiles);
    }
    
    public function saveEmbeddedForms($con = null, $forms = null)
    {    
        // when new product - no wizards
        if(isset($this->embeddedForms['wizard']))
        {
            $forms = $this->embeddedForms['wizard']->embeddedForms;
            
            foreach($forms as $key => $formRow)
            { 
                if($this['wizard'][$key]['valid']->getValue())
                {
                    $formRow->getObject()->setProduct($this->getProduct());                
                }    
                else
                {
                    $formRow->getObject()->delete();
                    unset($forms[$key]);
                }
            }
        }
        parent::saveEmbeddedForms($con, $forms);
    }

}

