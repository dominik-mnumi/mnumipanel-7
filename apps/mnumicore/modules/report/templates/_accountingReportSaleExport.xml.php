<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<items version="<?php echo sfConfig::get('app_version'); ?>" buildtime="<?php echo $buildTime; ?>" title="<?php echo __($title); ?>">
    <?php foreach($coll as $rec): ?>
    <item>
        <number><?php echo $rec; ?></number>
        <seller_name><?php echo $rec->getSellerName(); ?></seller_name>
        <seller_address><?php echo $rec->getSellerAddress(); ?></seller_address>
        <seller_postcode><?php echo $rec->getSellerPostcode(); ?></seller_postcode>
        <seller_city><?php echo $rec->getSellerCity(); ?></seller_city>
        <seller_tax_id><?php echo $rec->getSellerTaxId(); ?></seller_tax_id>
        <seller_bank_name><?php echo $rec->getSellerBankName(); ?></seller_bank_name>
        <seller_bank_account><?php echo $rec->getSellerBankAccount(); ?></seller_bank_account>
        <client_name><?php echo $rec->getClientName(); ?></client_name>
        <client_address><?php echo $rec->getClientAddress(); ?></client_address>
        <client_postcode><?php echo $rec->getClientPostcode(); ?></client_postcode>
        <client_city><?php echo $rec->getClientCity(); ?></client_city>
        <client_tax_id><?php echo $rec->getClientTaxId(); ?></client_tax_id>
        <price_net><?php echo $rec->getPriceNet(); ?></price_net>
        <price_tax><?php echo $rec->getPriceTax(); ?></price_tax>
        <price_gross><?php echo $rec->getPriceGross(); ?></price_gross>
        <paid_at><?php echo $rec->getPayedAt(); ?></paid_at>
        <orders><?php echo $rec->getOrdersIdString(); ?></orders>     
    </item>        
    <?php endforeach; ?>
</items>

