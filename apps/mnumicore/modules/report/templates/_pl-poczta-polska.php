<div class="poczta-polska-report">
    <div class="attachment">Załącznik nr .......................................................................</div>
    <div class="name">
        Imię i nazwisko (nazwa) oraz adres nadawcy: 
        <strong>
        <?php echo sfConfig::get('app_company_data_seller_name', 'My Company Name') ?>, 
        <?php echo sfConfig::get('app_company_data_seller_postcode', '00-000') ?> 
        <?php echo sfConfig::get('app_company_data_seller_city', 'City') ?>, 
        <?php echo sfConfig::get('app_company_data_seller_address', 'Street Number/Flat') ?>
        </strong>
    </div>
    <table class="address-table">
    <tr>
    <td class="address">
        E-PRZESYŁKA: adres e-mail albo numer tel. komórkowego w sieci telefonii komórkowej operatora krajowego (nadawcy)*
    </td>
    <td class="address-dots">.................................................................................................................</td>
    </tr>
    </table>
    
    <table class="address-table">
    <tr>
    <td class="address">
        POCZTEX: telefon / faks / SMS /e-mail**
    </td>
    <td class="address-dots">....................................................................................................................................................................................................................................................................</td>
    </tr>
    </table>
    
    <table class="report-data-table">
    <tr>
    	<td rowspan="2" class="lp">Lp.</td>
    	<td rowspan="2" class="address-name">ADRESAT<br/><br/>(Imię i nazwisko<br/>lub nazwa)</td>
    	<td rowspan="2" class="address">Adres adresata</td>
    	<td rowspan="2" class="post">Placówka<br/>pocztowa<br/>wydania<br/>E-PRZESYŁKI</td>
    	<td rowspan="2" class="email">Adres e-mail albo<br/>nr tel. komórkowego<br/>w sieci telefonii<br/>komórkowej<br/>operatora krajowego***</td>
    	<td rowspan="2" class="service">Serwis<br/>POCZTEX</td>
    	<td colspan="2">Kwota zadekl.<br/>wartości</td>
    	<td colspan="2">Masa</td>
    	<td rowspan="2" class="number">Numer<br/>nadawczy</td>
    	<td rowspan="2" class="notice">Uwagi</td>
    	<td colspan="2" class="left-border-big right-border-big top-border-big">Opłata</td>
    	<td colspan="2">Kwota<br/>pobrania</td>
    </tr>
    <tr>
    	<td class="cost-zl">zł</td>
    	<td class="cost-gr">gr</td>
    	<td class="weight-kg">kg</td>
    	<td class="weight-kg">g</td>
    	<td class="payment-zl left-border-big">zł</td>
    	<td class="payment-gr right-border-big">gr</td>
    	<td class="collection-zl">zł</td>
    	<td class="collection-gr">gr</td>
    </tr>
    <tr>
    	<td>1</td>
    	<td>2</td>
    	<td>3</td>
    	<td>4</td>
    	<td>5</td>
    	<td>6</td>
    	<td colspan="2">7</td>
    	<td colspan="2">8</td>
    	<td>9</td>
    	<td>10</td>
    	<td colspan="2" class="left-border-big right-border-big">11</td>
    	<td colspan="2">12</td>
    </tr>
    <tr>
    	<td colspan="12" class="transfer">Z przeniesienia</td>
    	<td class="left-border-big"></td>
    	<td class="right-border-big"></td>
    	<td></td>
    	<td></td>
    </tr>
    <?php $i = 1 ?>
    <?php foreach($reportObj->getOrderPackages() as $package): ?>
    <tr>
    	<td class="item"><?php echo $i++ ?></td>
    	<td><?php echo $package->getDeliveryName() ?></td>
    	<td><?php echo $package->getDeliveryStreet() .' '. $package->getDeliveryPostcodeAndCity() ?></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td></td>
    	<td class="left-border-big"></td>
    	<td class="right-border-big"></td>
    	<td></td>
    	<td></td>
    </tr>
    <?php endforeach ?>
    <tr>
    	<td colspan="10" class="infos no-border">
    	&nbsp;&nbsp;* w przypadku wykupienia usługi komplementarnej potwierdzenie wydania przesyłki<br/>
    	&nbsp;** w przypadku wykupienia usługi komplementarnej potwierdzenie doręczenia przesyłki – niewłaściwe skreślić i podać nr tel. lub adres e-mail<br/>
    	*** wypełnia się w usłudze E-PRZESYŁKA
    	</td>
    	<td colspan="2" class="transfer no-border">Do przeniesienia</td>
    	<td class="left-border-big bottom-border-big"></td>
    	<td class="right-border-big bottom-border-big"></td>
    	<td class="no-border"></td>
    	<td class="no-border"></td>
    </tr>
    </table>
    <br/>
</div>
