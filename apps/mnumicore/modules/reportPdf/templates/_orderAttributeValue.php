<?php if($attributeObj->getProductField()->getFieldset()->getName() != FieldsetTable::$WIZARD): ?>
    <td>
        <!-- if COUNT or QUANTITY only show number -->
        <?php if($attributeObj->getProductField()->getFieldset()->getName() == FieldsetTable::$COUNT
            || $attributeObj->getProductField()->getFieldset()->getName() == FieldsetTable::$QUANTITY): ?>
            <?php echo $attributeObj->getCalculatedValue(); ?>
            <!-- otherwise get field label from reffered FieldItem -->
        <?php else: ?>
            <?php $fieldItemObj = FieldItemTable::getInstance()->find($attributeObj->getValue()); ?>
            <!-- For customizable sizes -->
            <?php if($fieldItemObj->getFieldLabel() == '- Customizable -' &&
                $attributeObj->getProductField()->getFieldset()->getName() ==  FieldsetTable::$SIZE): ?>
                <?php echo $orderObj->getCustomizable()->getWidth() ?>  x <?php echo $orderObj->getCustomizable()->getHeight() ?>
                <?php echo sfConfig::get('app_size_metric', 'mm'); ?>
            <?php else: ?>
                <?php if($fieldItemObj): ?>
                    <?php echo $fieldItemObj->getFieldLabel(); ?>
                <?php endif ?>
            <?php endif ?>
        <?php endif ?>
    </td>
<?php endif; ?>