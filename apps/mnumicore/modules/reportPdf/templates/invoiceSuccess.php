<div id="invoice">
    <!-- top invoice seller data -->
    <table class="invoice-seller-data-table">
        <tbody>
        <tr>
            <td id="logo" class="td-top td-left">
                <?php if(sfConfig::has('app_company_data_seller_logo_path')): ?>
                    <?php echo image_tag(sfConfig::get('app_company_data_seller_logo_path')); ?>
                <?php endif; ?>
            </td>
            <td id="sellerName">
                <strong><?php echo __('Seller'); ?></strong>:
                <br />
                <?php echo $invoiceObj->getSellerName(); ?>
                <br />
                <?php echo $invoiceObj->getSellerAddress(); ?>,
                <?php echo $invoiceObj->getSellerPostcodeAndCity(); ?>
                <br />
                <?php echo __('Tax ID'); ?>: <?php echo $invoiceObj->getSellerTaxId(); ?>
                <?php if($invoiceObj->getSellerBankName()
                    && $invoiceObj->getSellerBankAccount()): ?>
                    <br />
                    <?php echo $invoiceObj->getSellerBankName(); ?> - <?php echo $invoiceObj->getSellerBankAccount(); ?>
                <?php endif; ?>
                <br/><br/>
            </td>
            <td id="barcode" class="td-top">
                <img src="<?php echo url_for('reportBarcode',
                    array('code' => $invoiceObj->getBarcodeIdentification()), true); ?>" />
            </td>
        </tr>
        </tbody>
    </table>

    <!-- invoice number -->
    <div class="invoice-number">
        <p class="invoice-number-main">
            <?php if($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$PRELIMINARY_INVOICE): ?>
                <?php echo __('Preliminary invoice'); ?>
            <?php elseif($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$INVOICE): ?>
                <?php echo __('VAT invoice'); ?>
            <?php elseif($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$CORRECTION_INVOICE): ?>
                <?php echo __('Correction invoice'); ?>
            <?php elseif($invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$RECEIPT): ?>
                <?php echo __('Receipt'); ?>
            <?php endif; ?>
            <?php echo $invoiceObj->getName(); ?>

            <?php if($invoiceObj->isCancelled()): ?>
                <br/><?php echo __('canceled'); ?>
            <?php endif; ?>
        </p>
        <?php if(sfConfig::has('app_invoice_header_text')): ?>
        <p style="font-size: 16px;">
            <?php echo sfConfig::get('app_invoice_header_text'); ?>
        </p>
        <?php endif; ?>

        <?php if($invoiceObj->getParentId()
            && $invoiceObj->getInvoiceTypeName() == InvoiceTypeTable::$CORRECTION_INVOICE): ?>
            <p><?php echo __('of day')?> <?php echo $sf_user->getSystemDate($invoiceObj->getCreatedAt(), 'p') ?></p>
            <p><?php echo __('Corrected invoice') ?> <?php echo $invoiceObj->getInvoice()->getName() ?></p>
        <?php endif ?>
        <?php if($duplicate): ?>
            <p class="invoice-number-main invoice-duplicate">
            <?php echo __('Duplicate'); ?>
            <?php echo __('from day'); ?>
            <?php echo $sf_user->getSystemDate(); ?>
            </p>
        <?php endif; ?>
    </div>

    <!-- client data -->
    <div id="clientData">
        <table class="client-data">
            <tbody>
            <tr>
                <td class="td-top td-right">
                    <strong><?php echo __('Buyer'); ?></strong>:
                </td>
                <td class="td-top td-left" colspan="3">
                    <div class="client-address">
                        <p><?php echo $invoiceObj->getClientName(); ?></p>
                        <p><?php echo $invoiceObj->getClientAddress(); ?></p>
                        <p><?php echo $invoiceObj->getClientPostcodeAndCity(); ?></p>
                        <?php if ($invoiceObj->getClientTaxId() != ""): ?>
                            <p><?php echo __('Tax ID'); ?> <?php echo $invoiceObj->getClientTaxId(); ?></p>
                        <?php endif; ?>
                        <?php if($invoiceObj->getClientCountry() != 'pl'): ?>
                            <p><?php echo __($invoiceObj->getClientCountryLabel()); ?></p>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <!-- dates and payment -->
    <div id="invoiceDetails">
        <p>
            <strong><?php echo __('Date of issue'); ?></strong>:
            <?php echo $sf_user->getSystemDate($invoiceObj->getSellAt(), 'p'); ?>
        </p>
        <p>
            <strong><?php echo __('Payment'); ?></strong>:
            <?php echo $invoiceObj->getPayment()->getLabel(); ?>
        </p>
        <p>
            <strong><?php echo __('Payment date'); ?></strong>:
            <?php echo $sf_user->getSystemDate($invoiceObj->getPaymentDateAt(), 'p'); ?>
        </p>
        <?php if($invoiceObj->isPaid()): ?>
        <p>
            <strong><?php echo __('Payment Date'); ?></strong>:
            <?php echo $sf_user->getSystemDate($invoiceObj->getPayedAt(), 'p'); ?>
        </p>
        <?php endif; ?>

        <?php if ($invoiceObj->getServiceRealizedAt() != "") : ?>
        <p>
            <strong><?php echo __('Date of sale'); ?></strong>:
            <?php echo $sf_user->getSystemDate($invoiceObj->getServiceRealizedAt(), 'p') ?>
        </p>
        <?php endif; ?>
    </div>

    <!-- invoice items -->
    <div id="invoiceItems">
        <?php if($isCorrection): ?>
            <h4><?php echo __('Before correction'); ?></h4>
            <?php include_partial('invoice/print/invoiceItems',
                array('invoiceObj' => $invoiceObj->getInvoice(),
                    'isCorrection' => false,
                    'groupPriceArr' => $groupPriceArr)); ?>

            <h4><?php echo __('After correction'); ?></h4>
        <?php endif; ?>

        <?php include_partial('invoice/print/invoiceItems',
            array('invoiceObj' => $invoiceObj,
                'isCorrection' => $isCorrection,
                'showTotal' => true,
                'groupPriceArr' => $groupPriceArr)); ?>

    </div>

    <div id="invoiceSummary">
        <table>
            <tr>
                <td class="td-top">
                    <strong><?php echo __('To pay'); ?></strong>:
                </td>
                <td>
                    <p><?php echo $sf_user->formatCurrency($invoiceObj->getPriceGross()); ?></p>
                    <p><?php echo __('In words'); ?>:
                        <?php echo $sf_user->getAmountInWords($invoiceObj->getPriceGross()); ?></p>
                </td>
            </tr>
        </table>


        <?php if($invoiceObj->getNotice()): ?>
            <br/><br/>
            <span class="summary-header">
                <?php echo __('Notes'); ?>:<br/>
                <?php echo nl2br($invoiceObj->getNotice()); ?>
            </span>
        <?php endif; ?>
    </div>
    <p id="footer-bar"><?php echo __('Generated in Mnumi System'); ?></p>
</div>

