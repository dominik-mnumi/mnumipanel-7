<div id="ajax_body">
    <div style = "text-align: center;">
    <br/>
    </div>
    <form id = "new_field_category_form" method = "post" action = "<?php echo url_for('@settingsCreate')?>">
       <?php echo $fieldCategoryForm->renderHiddenFields() ?>
       <table class = "new_field_category_table">
         <tr>
             <td><?php echo $fieldCategoryForm['name']->renderLabel() ?>: </td>
             <td><?php echo $fieldCategoryForm['name']->render() ?><br/>
             <?php echo $fieldCategoryForm['name']->renderError() ?>
             </td>
         </tr>
         <tr>
             <td><?php echo $fieldCategoryForm['label']->renderLabel() ?>: </td>
             <td><?php echo $fieldCategoryForm['label']->render() ?><br/>
             <?php echo $fieldCategoryForm['label']->renderError() ?>
             </td>
         </tr>
         <tr>
            <td colspan="2" style="width: 300px;">
            <?php echo $fieldCategoryForm['root_id']->render() ?>
            <?php echo $fieldCategoryForm['root_id']->renderError() ?>
                <?php echo include_partial('settings/chooseParentCategoryAddForm', array('treeColl' => $treeColl)); ?>
            </td>
        </tr>
       </table> 
    </form>
</div>

<div style = "text-align: center; display:none;" id = "ajax_loader">
    <img src = "/images/mask-loader.gif" alt="<?php echo __('loader') ?>" />
</div>