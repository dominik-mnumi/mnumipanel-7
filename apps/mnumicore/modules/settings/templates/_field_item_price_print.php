<div class="price">
    <?php echo __('Minimal price'); ?>:
    <input type="text" name="pricelist[<?php echo $id; ?>][minimal_price]" value="<?php echo $item['min']; ?>" size="7" />

    <?php echo __('Maximal price'); ?>:
    <input type="text" name="pricelist[<?php echo $id; ?>][maximal_price]" value="<?php echo $item['max']; ?>" size="7" />
</div>
<div class="price">
    <?php echo __('Plus price single'); ?>:
    <input type="text" name="pricelist[<?php echo $id; ?>][plus_price]" value="<?php echo $item['plus']; ?>" size="7" />

    <?php echo __('Plus price double'); ?>:
    <input type="text" name="pricelist[<?php echo $id; ?>][plus_price_double]" value="<?php echo $item['plus_double']; ?>" size="7" />
</div>

<div class="price-tables"></div>

<script type="text/template" id="price-content">
    <div class="price-<%= options.name %> price-table price">
	<br/>
        <table class="table">
            <thead>
            <tr>
                <th><?php echo __('Sheet quantity'); ?>/m2</th>
                <th><?php echo __('Simplex price'); ?></th>
                <th><?php echo __('Duplex price'); ?></th>
                <th><?php echo __('Action'); ?></th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <!-- /* backbone adding new row */ -->
            </tfoot>
        </table>
    </div>
</script>

<script type="text/template" id="price-item">
   <td>
       <input type="hidden" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][print]" class="tab-delete" value="1" />
       <input type="text" size="6" class="field-quantity" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][quantity]" value="<%= parseFloat(quantity) %>" <%= (this.options.item_no == 0) ? 'readonly="readonly"' : '' %>/>
   </td>
   <td><input type="text" size="6" class="field-price" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][price_simplex]" value="<%= price %>" /></td>
   <td><input type="text" size="6" class="field-price-duplex" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][price_duplex]" value="<%= price_duplex %>" /></td>
   <td>
   <% if(this.options.item_no != 0 && this.options.item_no != this.options.totalRowsNo - 1) { %>
       <a class="delete" title="<?php echo __('Delete'); ?>">
           <img src="/images/icons/fugue/cross-circle.png" width="16" height="16">
       </a>
   <% } %>
   </td>
</script>

<!-- gets field item price -->
<script type="text/javascript">
    jQuery(document).ready(function($)
    {   
        priceRangeViewColl['<?php echo $id; ?>'] = new Array();        
        priceRangeViewValueColl['<?php echo $id; ?>'] = new Array();

        <?php $coll1 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice)); ?>

        priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_simplex; ?>'] = <?php echo $coll1; ?>

        addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_simplex; ?>', '<?php echo __('Per sheet'); ?>', '<?php echo $id; ?>');
        
        /**
         * Services first quantity input.
         */
        $("#field_item_measure_unit_id").change(function()
        {
            // gets all pricelists
            var coll = $(".tabs-content > div");
            
            if($("#field_item_measure_unit_id").val() == '<?php echo MeasureUnitTable::getInstance()->findOneByName(MeasureUnitTable::$price_page)->getId(); ?>')
            {
                coll.each(function()
                {
                    $(this).find('.field-quantity').eq(0).val(1);
                });
            }
            else
            {
                coll.each(function()
                {
                    $(this).find('.field-quantity').eq(0).val(0);
                });
            }
        });
        
        /**
         * Triggers change when start and checks prices if first quantity is
         * properly defined. 
         */
        $("#field_item_measure_unit_id").trigger('change');
    }); 
</script>
