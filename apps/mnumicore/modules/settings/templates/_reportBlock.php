<div class="task with-legend">
    <div class="legend"><?php echo image_tag('/images/icons/fugue/status/'.$rec->getStatus().'.png', array('width' => 16, 'height' => 16)); ?> <?php echo __($rec->getName()); ?></div>

    <div class="task-description">
        <h3><?php echo __($rec->getStatusInfo()); ?></h3>
        <br />
        <?php echo __('Currently'); ?>: <?php echo __($rec->getValue()); ?>
    </div>
</div>