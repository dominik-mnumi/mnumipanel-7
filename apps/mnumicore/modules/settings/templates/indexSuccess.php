<?php echo include_partial('dashboard/message'); ?>
<?php include_partial('global/alertModalConfirm') ?>
<article class="container_12">
  <section class="grid_12">
      <?php echo include_component('settings', 'navigation', array('id' => isset($id) ? $id : null)); ?>
  </section>
</article>

<!-- create form -->
<div id="new_field_category" class = "new_field_category" style = "display: none;">
    <?php include_component('settings', 'createFieldset') ?>
</div>
