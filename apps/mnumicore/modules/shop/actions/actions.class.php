<?php

/**
 * shop actions.
 *
 * @package    mnumicore
 * @subpackage shop
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class shopActions extends tablesActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Description' => array(),
            'ValidDate' => array('label' => 'Expiry date'),
            'Host' => array(),
            'Type' => array('label' => 'Type',
                'partial' => 'shop/type')     
        );
        $this->displayGridFields = array(
            'Description'  => array('destination' => 'name'), 
            'Host'  => array('destination' => 'keywords')
        );
        $this->sortableFields = array('description', 'valid_date', 'host', 'type');
        $this->modelObject = 'Shop';
        
        $this->tableOptions = $this->executeTable();
        
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    public function executeCreate(sfWebRequest $request)
    {      
        $this->form = new ShopForm();
        $this->proceedForm($this->form);
        $this->setTemplate('edit');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->form = new ShopForm($this->getRoute()->getObject());
        $this->proceedForm($this->form);
    }

    public function executeDelete(sfWebRequest $request)
    {
        $this->getRoute()->getObject()->delete();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        $this->redirect('@settingsShop');
    }

    private function proceedForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settingsShopEdit?name=' . $this->form->getObject()->name);
        }
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Shop';
        parent::executeDeleteMany($request);
    }
}
