<?php

/**
 * stats actions.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class statsActions extends sfActions
{
    public function executeOrders(sfWebRequest $request)
    {
        $fields = array(
            'range' => $request->getParameter('range', 'cy'),
            'date_range' => $request->getParameter('date_range', array(
                'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1, date("Y"))),
                'to' => date('Y-m-d'),
            )),
            'shop_name' => $request->getParameter('shop_name', ''),
            'group_by' => $request->getParameter('group_by', 'm'),
            'status' => $request->getParameter('status', 'none'),
        );

        $filter = new StatsOrderFormFilter(array(), array(
            'table_method' => 'statsQuery'
        ));
        $filter->bind($fields);

        // if Json request format
        if($this->getRequest()->getRequestFormat() == 'json')
        {
            $json = new StatOrderJson(array(
                'method_name' => 'statsJson',
                'i18n' => $this->getContext()->getI18N(),
            ));
            $json->setQuery($filter->getQuery());

            // view
            echo json_encode($json->asArray());
            return sfView::NONE;
        }

        $this->title = 'Orders';

        $this->prepareFilter($filter);

        $this->tableHeader = array(
            'Date',
            'Qty',
            'Value',
        );

        $this->tableRows = array();
        foreach($this->pager as $row)
        {
            $this->tableRows[] = array(
                $row->getDate(),
                $row->getQty(),
                $this->getUser()->formatCurrency($row->getPrice()),
            );
        }
    }

    public function executeProducts(sfWebRequest $request)
    {
        $fields = array(
            'range' => $request->getParameter('range', 'cy'),
            'date_range' => $request->getParameter('date_range', array(
                'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1, date("Y"))),
                'to' => date('Y-m-d'),
            )),
            'shop_name' => $request->getParameter('shop_name', ''),
            'order_status_name' => $request->getParameter('order_status_name', ''),
        );

        $filter = new StatsProductFormFilter(array(), array(
            'table_method' => 'statsQuery'
        ));
        $filter->bind($fields);

        // if Json request format
        if($this->getRequest()->getRequestFormat() == 'json')
        {
            $json = new StatProductsJson(array(
                'method_name' => 'statsJson',
                'i18n' => $this->getContext()->getI18N(),
            ));
            $json->setQuery($filter->getQuery());

            // view
            echo json_encode($json->asArray());
            return sfView::NONE;
        }

        $this->title = 'Frequently Purchased';

        $this->prepareFilter($filter);

        $this->tableHeader = array(
            'Product',
            'Cost',
            'Value',
        );

        $this->tableRows = array();
        foreach($this->pager as $row)
        {
            $this->tableRows[] = array(
                $row->getName(),
                $this->getUser()->formatCurrency($row->getCost()),
                $this->getUser()->formatCurrency($row->getAmount()),
            );
        }
    }

    public function executeOrdersByType(sfWebRequest $request)
    {
        $fields = array(
            'range' => $request->getParameter('range', 'cy'),
            'date_range' => $request->getParameter('date_range', array(
                'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1, date("Y"))),
                'to' => date('Y-m-d'),
            )),
            'shop_name' => $request->getParameter('shop_name', ''),
            'group_by' => $request->getParameter('group_by', 'm'),
            'status' => $request->getParameter('status', 'none'),
        );

        $filter = new StatsOrderFormFilter(array(), array(
            'table_method' => 'statsByTypeQuery',
        ));
        $filter->bind($fields);

        // if Json request format
        if($this->getRequest()->getRequestFormat() == 'json')
        {
            $json = new StatOrderJson(array(
                'method_name' => 'statsByTypeJson',
                'i18n' => $this->getContext()->getI18N(),
            ));
            $json->setQuery($filter->getQuery());

            // view
            echo json_encode($json->asArray());
            return sfView::NONE;
        }

        $this->title = 'Orders by type';

        $this->prepareFilter($filter);

        $this->tableHeader = array(
            'Date',
            'One-time purchase',
            'Another customer purchase',
            'One-time purchase',
            'Another customer purchase',
        );

        $this->tableRows = array();
        foreach($this->pager as $row)
        {
            $this->tableRows[] = array(
                $row->getDate(),
                $row->getQtyFirst(),
                $row->getQtyNext(),
                $this->getUser()->formatCurrency($row->getPriceFirst()),
                $this->getUser()->formatCurrency($row->getPriceNext()),
            );
        }
    }

    /**
     * Payment statistics
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executePayment(sfWebRequest $request)
    {
        $fields = array(
            'range' => $request->getParameter('range', 'cy'),
            'date_range' => $request->getParameter('date_range', array(
                'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1, date("Y"))),
                'to' => date('Y-m-d'),
            )),
            'shop_name' => $request->getParameter('shop_name', ''),
            'status' => $request->getParameter('status', 'none'),
        );

        $filter = new StatsPaymentFormFilter(array(), array(
            'table_method' => 'statsPaymentQuery',
        ));
        $filter->bind($fields);

        // if Json request format
        if($this->getRequest()->getRequestFormat() == 'json')
        {
            $json = new StatOrderJson(array(
                'method_name' => 'statsPaymentJson',
                'i18n' => $this->getContext()->getI18N(),
            ));
            $json->setQuery($filter->getQuery());

            // view
            echo json_encode($json->asArray());
            return sfView::NONE;
        }

        $this->title = 'Payments methods';

        $this->prepareFilter($filter);

        $this->tableHeader = array(
            'Payment name',
            'Number of orders',
            'Total sales',
        );

        $this->tableRows = array();
        foreach($this->pager as $row)
        {
            $this->tableRows[] = array(
                $row->getPaymentName(),
                $row->getQty(),
                $this->getUser()->formatCurrency($row->getPrice()),
            );
        }

    }

    /**
     * Delivery statistics
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeCarrier(sfWebRequest $request)
    {
        $fields = array(
            'range' => $request->getParameter('range', 'cy'),
            'date_range' => $request->getParameter('date_range', array(
                'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1, date("Y"))),
                'to' => date('Y-m-d'),
            )),
            'shop_name' => $request->getParameter('shop_name', ''),
            'status' => $request->getParameter('status', 'none'),
        );

        $filter = new StatsPaymentFormFilter(array(), array(
            'table_method' => 'statsCarrierQuery',
        ));
        $filter->bind($fields);

        // if Json request format
        if($this->getRequest()->getRequestFormat() == 'json')
        {
            $json = new StatOrderJson(array(
                'method_name' => 'statsCarrierJson',
                'i18n' => $this->getContext()->getI18N(),
            ));
            $json->setQuery($filter->getQuery());

            // view
            echo json_encode($json->asArray());
            return sfView::NONE;
        }

        $this->title = 'Shipping options';

        $this->prepareFilter($filter);

        $this->tableHeader = array(
            'Carrier name',
            'Number of orders',
            'Total sales',
        );

        $this->tableRows = array();
        foreach($this->pager as $row)
        {
            $this->tableRows[] = array(
                $row->getCarrierName(),
                $row->getQty(),
                $this->getUser()->formatCurrency($row->getPrice()),
            );
        }

    }

    /**
     * Prepare filters for stats results
     * @param sfFormFilter $filter
     * @return string
     */
    protected function prepareFilter(BaseFormFilterDoctrine $filter)
    {
        $this->setTemplate('baseStats');

        $this->filter = $filter;
        $this->url = http_build_query($filter->getValues());

        $this->pager = new sfDoctrinePager('Order', $this->getRequest()->getParameter('limit', 20));
        $this->pager->setQuery($filter->getQuery());
        $this->pager->setPage($this->getRequestParameter('page',1));
        $this->pager->init();
    }

    public function preExecute()
    {
        $this->demoMode = sfConfig::get('app_demo_mode', 0);

        // gets request
        $request = $this->getRequest();
        
        // gets user object
        $myUser = $this->getUser();

        // foreach element sets attributes to session and sets variables
        $dateArr = array('Y' => 'year', 'n' => 'month', 'j' => 'day', 'orderType' => 'orderType');
        foreach($dateArr as $key => $rec)
        {   
            if($request->hasParameter($rec))
            {
                // gets parameter from $request
                $this->$rec = $request->getParameter($rec);
                $myUser->setAttribute(
                        $rec, 
                        $this->$rec, 
                        myUser::$attributeStatsNamespace);
            }
            else
            {
                // gets parameter from session or current (if does not exist)
                $this->$rec = $myUser->getAttribute(
                        $rec, 
                        date($key), 
                        myUser::$attributeStatsNamespace);
            }
        }
      
        parent::preExecute();
    }

    /**
     * Executes employee action.
     *
     * @param sfRequest $request A request object
     */
    public function executeEmployee(sfWebRequest $request)
    { 
        // gets year and month
        $year = $this->getYear();
        $month = $this->getMonth();
	    $orderType = $this->getOrderType();

        // creates date form
        $dateForm = new StatsDateForm(
                array('year' => $year, 
                    'month' => $month,
                    'orderType' => $orderType));
        
        // prepares group permissions
        $groupPermissionArr = array(sfGuardGroupTable::$admin,
            sfGuardGroupTable::$office);
        
        // gets users with defined group permissions
        $coll = sfGuardUserTable::getInstance()
                ->getUsersByGroupPermission($groupPermissionArr);

        // if demo mode
        if($this->isDemo())
        {
            // prepares employee variables
            $this->prepareEmployeeDemo($month);
            
            // creates object of stats
            $employeeStatsObj = new StatsDemo($this->randomTo);   
        }
        else
        {
            // creates object of stats
            $employeeStatsObj = new EmployeeStats($groupPermissionArr, $year, $month, $orderType);
        }

        // view objects
        $this->coll = $coll;
        $this->employeeStatsObj = $employeeStatsObj;
        $this->dateForm = $dateForm; 
        
        // if demo mode
        if($this->isDemo())
        {
            return 'Demo';
        }
    }

    /**
     * Executes sale action.
     *
     * @param sfRequest $request A request object
     */
    public function executeSale(sfWebRequest $request)
    {
        // gets month
        $day = $this->getDay();

        // gets month
        $month = $this->getMonth();

        // gets shop
        $shop = $this->getShop();

        // date form
        $this->dateForm = new StatsDateForm(array('month' => $month, 'shop' => $shop));

        // gets year array
        $this->yearArr = StatsDateForm::$yearArr;

        // get results
        $results = array();

        // if demo mode
        if($this->isDemo())
        {
            for($year=2002; $year < date('Y'); $year++)
            {
                // prepares product variables
                $results[$year] = array(
                    0 => array(
                        'panelIncome'       => rand(1000,1500),
                        'panelOrderCount'   => rand(1000,1500),
                        'shopIncome'        => rand(1000,1500),
                        'shopOrderCount'    => rand(1000,1500),
                    ),
                    1 => array(
                        'panelIncome'       => rand(1000,1500),
                        'panelOrderCount'   => rand(1000,1500),
                        'shopIncome'        => rand(1000,1500),
                        'shopOrderCount'    => rand(1000,1500),
                    ),
                );
            }

            // creates object of stats
            $this->saleStatsObj = new StatsDemo(2000);

            $this->results = $results;

            return sfView::SUCCESS;
        }

        // creates object of stats
        $this->saleStatsObj = new SaleStats($this->yearArr, $month);

        foreach($this->yearArr as $year)
        {
            $results[$year] = array(
                0 => array(
                    'panelIncome'       => OrderTable::getInstance()->getOrdersIncome($year, $month, null),
                    'panelOrderCount'   => OrderTable::getInstance()->getStatsOrdersCount($year, $month, null),
                    'shopIncome'        => OrderTable::getInstance()->getOrdersIncome($year, $month, null, $shop),
                    'shopOrderCount'    => OrderTable::getInstance()->getStatsOrdersCount($year, $month, null, $shop),
                ),
                1 => array(
                    'panelIncome'       => OrderTable::getInstance()->getOrdersIncome($year, $month, $day),
                    'panelOrderCount'   => OrderTable::getInstance()->getStatsOrdersCount($year, $month, $day),
                    'shopIncome'        => OrderTable::getInstance()->getOrdersIncome($year, $month, $day, $shop),
                    'shopOrderCount'    => OrderTable::getInstance()->getStatsOrdersCount($year, $month, $day, $shop),
                ),
            );
        }

        $this->results = $results;
    }

    /**
     * Prepares employee demo variables.
     * 
     * @param type $month 
     */
    protected function prepareEmployeeDemo($month)
    {
        if(in_array($month, array(1, 12)))
        {
            $randomFrom = 900;
            $randomTo = 1000;   
        }
        elseif(in_array($month, array(2, 11)))
        {
            $randomFrom = 1000;
            $randomTo = 1100;   
        }
        elseif(in_array($month, array(3, 10)))
        {
            $randomFrom = 1200;
            $randomTo = 1300;   
        }
        elseif(in_array($month, array(4, 5, 9)))
        {
            $randomFrom = 1300;
            $randomTo = 1400;   
        }
        else
        {
            $randomFrom = 900;
            $randomTo = 1400;   
        }

        // view objects
        $this->randomFrom = $randomFrom;
        $this->randomTo = $randomTo;
    }

    /**
     * Checks if demo mode.
     * 
     * @return boolean 
     */
    protected function isDemo()
    {
        return (boolean)$this->demoMode;
    }
    
    /**
     * Returns year for stats.
     * 
     * @return integer 
     */
    protected function getYear()
    {
        return $this->year;
    }
    
    /**
     * Returns month for stats.
     * 
     * @return integer 
     */
    protected function getMonth()
    {
        return $this->month;
    }
    
    /**
     * Returns day for stats.
     * 
     * @return integer 
     */
    protected function getDay()
    {
        return $this->day;
    }

    /**
     * Returns day for stats.
     *
     * @return integer
     */
    protected function getShop()
    {
        $shop = $this->getUser()->getAttribute('shop', true, myUser::$attributeStatsNamespace);

        if ($this->getRequest()->hasParameter('shop'))
        {
            $shop = $this->getRequest()->getParameter('shop');
            $this->getUser()->setAttribute('shop', $shop, myUser::$attributeStatsNamespace);
        }

        return $shop;
    }

	/**
	 * Returns orderType for stats.
	 *
	 * @return string
	 */
	protected function getOrderType()
	{
		return $this->orderType;
	}
}
