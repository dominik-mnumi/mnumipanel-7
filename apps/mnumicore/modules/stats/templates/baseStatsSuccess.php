<article class="container_12">
    <div class="block-border"><div class="block-content">
            <h1>
                <?php echo __('Report'); ?>:
                <?php echo __($title); ?>
            </h1>

            <form class="form" action="" method="post">

                <fieldset class="grey-bg">
                    <legend><a href="javascript:void(0)"><?php echo __('Options'); ?></a></legend>
                    <div class="formFilter"><?php echo $filter; ?></div>
                </fieldset>
                <?php if(method_exists($filter, 'getDateRange')): ?>
                <h2>
                    <?php echo __('Report generated for the range') . ": " . __($filter->getDateRange()); ?>
                </h2>
                <?php endif; ?>
                <?php if(count($pager) > 0): ?>
                <div id="chart">
                    <object height="400" width="100%" type="application/x-shockwave-flash" data="/images/open-flash-chart.swf" id="my_chart" style="visibility: visible;">
                        <param name="flashvars" value="<?php echo http_build_query(array(
                             'data-file' => url_for("@stats".ucfirst($sf_params->get('action'))."Json?".$sf_data->getRaw('url'))
                        ), '', '&'); ?>">
                        <param name="wmode" value="transparent">
                        <param name="movie" value="/images/open-flash-chart.swf">
                    </object>
                </div>

                <div id="stat-table">
                    <table class="table full-width">
                        <thead>
                        <tr>
                            <?php foreach($tableHeader as $head): ?>
                                <th><?php echo __($head); ?></th>
                            <?php endforeach; ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($tableRows as $row): ?>
                            <tr>
                                <?php $i=0; foreach($row as $field): ?>
                                    <td<?php echo ($i==0) ? '': ' class="right"' ?>><?php echo $field; ?></td>
                                <?php $i++; endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <div class="table-navigation">
                        <p class="limit">
                            <?php echo __('Show'); ?>
                            <select name="limit" onchange="this.form.submit()">
                                <option<?php if($pager->getMaxPerPage() == 10): ?> selected=""<?php endif; ?>>10</option>
                                <option<?php if($pager->getMaxPerPage() == 20): ?> selected=""<?php endif; ?>>20</option>
                                <option<?php if($pager->getMaxPerPage() == 30): ?> selected=""<?php endif; ?>>30</option>
                                <option<?php if($pager->getMaxPerPage() == 40): ?> selected=""<?php endif; ?>>40</option>
                                <option<?php if($pager->getMaxPerPage() == 50): ?> selected=""<?php endif; ?>>50</option>
                            </select>
                            <?php echo __('entries'); ?>
                        </p>
                        <p class="info">
                            <?php echo __('Showing'); ?>
                            <?php echo ' ' . $pager->getFirstIndice() . ' ' . __('to') . ' ' . $pager->getLastIndice(); ?>
                            <?php echo __('of'); ?>
                            <strong><?php echo __plural('1 entry', '@count entries', array('@count' => $pager->getNbResults())); ?></strong>
                        </p>
                        <span class="navigation">
                            <?php if($pager->getPage() > 1): ?>
                                &#171; <?php echo link_to(__('Previous'), '@statsOrders?page='. $pager->getPreviousPage().'&'.$sf_data->getRaw('url')) ?>
                                -
                            <?php endif; ?>
                            <?php if($pager->getPage() < $pager->getLastPage()): ?>
                                <?php echo link_to(__('Next'), '@statsOrders?page=' . $pager->getNextPage().'&'.$sf_data->getRaw('url')) ?> &#187;
                            <?php endif; ?>
                        </span>
                    </div>
                </div>
                <?php else: ?>
                    <p><?php echo __('No results'); ?></p>
                <?php endif; ?>
            </form>

        </div></div>
</article>


<script type="text/javascript">
    jQuery(function($) {
        $('form.form :input').change(function(){
            $('#sf_content').load('<?php echo url_for("@stats".ucfirst($sf_params->get('action'))); ?>',
                $('form.form').serializeArray(), function(response, status, xhr) {
                    if (status == "error")
                    {
                        alert('<?php echo __('An error occurred'); ?>');
                    }
                }
            );
        });

        $(".navigation a").on("click", function(){
            $("#sf_content").load($(this).attr("href"));
            return false;
        });

        if($('form #range').val() == 'custom')
        {
            $('form #range').parent().hide();
            $('form #date_range_from').parent().show();
            loadDataPicker($);
        }
        else
        {
            $('form #date_range_from').parent().hide();
        }
    });
</script>