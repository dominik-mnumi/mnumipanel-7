<form id="dialog-edit-user-permission-form" style="display: none;" action="<?php echo url_for('reqEditUserPermissionCollForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post">
    <p class="validateTips"></p>
    <?php echo $form['_csrf_token']->render(); ?>    
    <?php echo $form['id']->render(); ?>
    <?php $groupArrayForm =  $form->getEmbeddedForm('UserGroups'); ?>
    <?php $obj =  $form->getObject(); ?>
    <div class="text_container">
        <?php if(0 < count($groupArrayForm)): ?>
        <table class="permission_group_table">
            <?php foreach($groupArrayForm->getEmbeddedForms() as $key => $groupForm): ?>
            <tr>   
                <td style="width: 200px">
                    <div class="text_container">
                        <?php echo __($groupForm->getObject()->getGroup()->getDescription()); ?>
                    </div>
                </td>
                <td style="text-align: center;">
                    <?php echo $form['UserGroups'][$key]['group_id']->render(); ?>
                    <?php echo $form['UserGroups'][$key]['user_id']->render(); ?>
                    <?php echo $form['UserGroups'][$key]['value']->render(); ?>
                </td>
            </tr>    
            <?php endforeach; ?>
        </table>
        <?php else: ?>
        <?php echo __('No permissions'); ?>
        <?php endif; ?>
    </div>    
</form>

