<div class="block-controls">					
    <ul class="controls-tabs js-tabs same-height with-children-tip">
        <li class="current" title="<?php echo __('User edit'); ?>">
            <a href="#tab-edit" title="<?php echo __('User edit'); ?>">
                <img src="/images/icons/order/user.png" width="24" height="24">                               
            </a>
        </li>
        <li title="<?php echo __('User orders'); ?>">
            <a href="#tab-orders" title="<?php echo __('User orders'); ?>">
                <img src="/images/icons/order/order-user.png" width="24" height="24">
            </a>
        </li>
        <?php /* @TODO Think about invoices and users connection (for now is missing)
        
        <li title="<?php echo __('User invoices'); ?>">
            <a href="#tab-invoices" title="<?php echo __('User invoices'); ?>">
                <img src="/images/icons/order/invoice-user.png" width="24" height="24">
            </a>
        </li>
        */ ?>
        <li title="<?php echo __('User loyalty points'); ?>">
            <a href="#tab-loyalty-points" title="<?php echo __('User loyalty points'); ?>">
                <img src="/images/icons/web-app/24/Bar-Chart.png" width="24" height="24">
            </a>
        </li>
        <li title="<?php echo __('User notifications'); ?>">
            <a href="#tab-notifications" title="<?php echo __('Customer notification'); ?>">
                <img src="/images/icons/web-app/24/Comment.png" width="24" height="24">
            </a>
        </li>
    </ul>
</div>
<br/><br/>