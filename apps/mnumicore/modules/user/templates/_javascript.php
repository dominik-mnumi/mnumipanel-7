<script type="text/javascript">    
    function renderError(errorArray)
    {
        var errorlist = '<ul class="message error no-margin">';
        for(i in errorArray)
        {
            errorlist += '<li>' + errorArray[i] + '</li>';
        }
        errorlist += '</ul>';
        
        return errorlist;
    }
    
    function hideAllShowCurrent()
    {
        //hides all tabs
        $("#tab-edit").css("display", "none");
        $("#tab-orders").css("display", "none");
        $("#tab-invoices").css("display", "none");

        //gets param tab
        var href = window.location.toString();
        var param = href.split("#");
        
        if(2 == param.length)
        {
            //if none or undefined set to first tab
            if('none' == param[1] || 'undefined' == param[1])
            {
                param[1] = 'tab-edit';
            }
        }
        //sets to first tab 
        else
        {
            param[1] = 'tab-edit';
        }

        $("#" + param[1]).css("display", "");
    }
    
    function changeSwitch(input)
    {
        if(true == $(input).attr('checked'))
        {
            $(input).attr('checked', false);
        }
        else
        {
            $(input).attr('checked', true);
        }
    }
    
    //js hack for client edit header
    $(".block-content .h1").css("left", $(".block-content h1").width() + 35);
 
    //user add form
    $("#dialog-add-user-click").click(function() 
    {                          
        $.modal({
            content: $("#dialog-add-user-form"),
            title: '<?php echo __('Add user'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            buttons: 
                {
                '<?php echo __('Add user'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-user-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.')  ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-add-user-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    var href = "<?php echo url_for('@userEditAjaxRedirect'); ?>" + dataOutput.result.id;
                                    window.location.href = href;         
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });      
    });
    
    //user edit general form
    $("#dialog-edit-user-general-click").click(function() 
    {                          
        $.modal({
            content: $("#dialog-edit-user-general-form"),
            title: '<?php echo __('Edit user'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-user-general-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-user-general-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    $.get(window.location + "?reload=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );              
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });      
    });
    
    //user edit password form
    $("#dialog-edit-user-password-click").click(function() 
    {                          
        $.modal({
            content: $("#dialog-edit-user-password-form"),
            title: '<?php echo __('Change password'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-user-password-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-user-password-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();

                                    $.get(window.location + "?reload=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );              
                                }

                            }

                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });     
    });

    //user edit contact data form
    $("#dialog-edit-user-contact-data-click").click(function() 
    {                          
        $.modal({
            content: $("#dialog-edit-user-contact-data-form"),
            title: '<?php echo __('Contact data'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-user-contact-data-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-user-contact-data-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    $.get(window.location + "?reload=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );              
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });      
    });

    //user edit notification form
    $("#dialog-edit-user-notification-form input").change(function() 
    {                  
        var form = $(this).parents("form");
        var dataInput = form.serialize();
        var dataOutput;
        var uri =  form.attr('action');
        var method = form.attr('method'); 

        $.ajax({
            type: method,
            url: uri,
            data: dataInput,
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(dataOutput)
            {
                if('success' == dataOutput.result.status)
                {       
                    /*
                $.get(window.location,
                    function(html)
                    {
                        $("#sf_content").html(html);
                    }
                );  */                           
                }
            }        
        });
    });
    
    // initializes autocomplete for client
    initializeAutocomplete($("#dialog-add-edit-client-form #EditClientEditUserForm_client"), "<?php echo url_for('@userClientAutocomplete'); ?>");
    
    
    // user edit client new form
    $("#dialog-add-new-client-click").click(function() 
    {      
        $.modal({
            content: $("#dialog-add-new-client-form"),
            title: '<?php echo __('User'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-new-client-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
                
                // initializes autocomplete for user
                initializeAutocomplete($(".modal-content #EditUserNewClientForm_client"), "<?php echo url_for('@userClientAutocomplete'); ?>");
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-new-client-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);
                                    
                                    $(".modal-content #dialog-add-new-client-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();    
                                    $.get("?forcePost=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });       
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
  
    //user edit permission form
    $("#dialog-edit-user-permission-click").click(function() 
    {
        $.modal({
            content: $("#dialog-edit-user-permission-form"),
            title: '<?php echo __('Permission groups'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            // adds class to switch on start open modal
            onOpen: $("#dialog-edit-user-permission-form input:checkbox").attr('class', 'mini-switch'),
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-user-permission-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {        
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-user-permission-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    $.get(window.location + "?reload=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );              
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
    });

    

</script>