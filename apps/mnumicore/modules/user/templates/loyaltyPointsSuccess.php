<article class="container_12">
    <section class="grid_4" class="with-margin">
        <div class="block-border">
            <div class="block-content form">
                <h1><?php echo __('Points') ?></h1>
                <ul class="simple-list with-icon">
                    <?php foreach($types as $type): ?>
                    <li class="icon-points-<?php echo $type?>">
                        <a href="<?php echo url_for('userLoyaltyPoints', $userObj)?>?type=<?php echo $type?>">
                            &nbsp;&nbsp;<?php echo __(ucfirst($type)) ?>: <?php echo $userObj->getPointsCount($type) ?>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="grid_8">
        <div class="block-border">
            <div class="block-content">
                <h1><?php echo __('Details') ?></h1>
                <ul class="simple-list with-icon">
                <?php foreach($points as $point) : ?>
                    <li class="icon-points-<?php echo $point->getStatus() ?>">
                        <span>
                            &nbsp;&nbsp;<?php echo __(ucfirst($point->getStatus())) ?>: <b><?php echo $point->getPoints() ?> <?php echo __('points')?></b>
                            (<?php echo __('Modification date') ?>: <?php echo __($point->getCreatedAt())?>)<br/>
                            &nbsp;&nbsp;<i><?php echo $point->getDescription() ?></i>
                        </span>
                    </li>
                <?php endforeach ?>
                </ul>
            </div>
        </div>
    </section>
</article>