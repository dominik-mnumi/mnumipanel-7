<?php $settings = $settings->getRawValue() ?>
<div class="no-margin last-child">
    <div class="block-controls">
        <div class="controls-buttons">
            <ul class="controls-buttons">
                <?php if(isset($pager)) : ?>
                <?php if ($pager->haveToPaginate()): ?>
                <?php if($pager->getPage() > 1): ?>
                    <li><a title="<?php echo __('Previous') ?>" href="#" class = "page" rel="<?php echo $pager->getPreviousPage() ?>"><img width="16" height="16" src="/images/icons/fugue/navigation-180.png"> <?php echo __('Prev') ?></a></li>
                <?php endif; ?>
                <?php $links = $pager->getLinks(); ?>
                <?php foreach ($links as $page): ?>
                    <li><a class = "page <?php if($page == $pager->getPage()) echo ' current' ?>" title="<?php echo __('Page') ?> <?php echo $page; ?>" href="#" rel="<?php echo $page; ?>"><b><?php echo $page; ?></b></a></li>
                <?php endforeach ?> 
                <?php if($pager->getPage() < $pager->getLastPage()): ?>
                    <li><a title="<?php echo __('Next') ?>" href="#" rel="<?php echo $pager->getNextPage() ?>" class = "page"> <?php echo __('Next') ?> <img width="16" height="16" src="/images/icons/fugue/navigation.png"></a></li>
                <?php endif; ?>
                <li class="sep"></li>
                <?php endif; ?>
                <li><a href="#" id="reload">&nbsp;<img width="16" height="16" src="/images/icons/fugue/arrow-circle.png">&nbsp;</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <form method="post" action="" id="manyItems" class="no-margin">
        <?php if($settings['tableView'] == 'table') : ?>
        <?php include_partial('global/tableView', array(
            'fields' => $fields, 
            'pager' => $pager, 
            'sortableFields' => $sortableFields, 
            'settings' => $settings,
            'layoutOptions' => $layoutOptions)) ?>
        <?php elseif($settings['tableView'] == 'grid') : ?>
        <?php include_partial('global/gridView', array(
            'fields' => $fields, 
            'pager' => $pager, 
            'sortableFields' => $sortableFields, 
            'settings' => $settings,
            'layoutOptions' => $layoutOptions)) ?>
        <?php else: ?>
        <?php echo __('View is not implemented yet') ?>
        <?php endif ?>
    </form>

    <div class="message no-margin"><?php echo __('Showing') ?> 
        <?php echo !$pager->count() ? '0' : ($pager->getPage() * $pager->getMaxPerPage())-$pager->getMaxPerPage()+1 ?> <?php echo __('to') ?>
        <?php $last = $pager->getPage() * $pager->getMaxPerPage()?>
        <?php echo $last > $pager->count() ? $pager->count() : $last ?> 
        <?php echo __('of') ?> <?php echo __plural('1 entry', '@count entries', array('@count' => $pager->count())) ?>
    </div>

    <div class="block-footer clearfix">
        <?php if($settings['manyActionAvailable']): ?>
        <div class="float-left">
            <?php if($layoutOptions['showCheckboxes']): ?>
            <img width="16" height="16" class="picto" src="/images/icons/fugue/arrow-curve-000-left.png"> 
            <a class="button" href="#" id="selectAll"><?php echo __('Select All') ?></a> 
            <a class="button" href="#" id="deselectAll"><?php echo __('Unselect All') ?></a>
            <span class="sep"></span>
            <?php endif; ?>
            <select id="table-action" name="table-action">
                <option value=""><?php echo __($layoutOptions['manyActionsDefaultLabel']); ?></option>
                <?php foreach($settings['actions'] as $actionName => $action): ?>
                <?php if(!empty($action['manyRoute']) && empty($action['disabledForMany'])): ?>
                <option value="<?php echo $actionName; ?>"><?php echo __($action['label']) ?></option>
                <?php endif;?>    
                <?php endforeach; ?>
            </select>
            <button class="small" type="submit" id="submitItems"><?php echo __('Ok') ?></button>
        </div>
        <span class="sep float-left"></span>
        <?php endif; ?>

        <div class="float-left">
            <form action="<?php echo url_for($settings['route'], $settings['routeParams']) ?>" method="post" id="perPageForm">
                <div class="float-left">
                    <?php echo __('Show')?>    
                    <?php echo $perPageForm->renderHiddenFields(); ?>
                    <?php echo $perPageForm['options']->render(array("id" => "perPage")) ?>  
                    <?php echo __('entries')?>
                </div>
            </form>
        </div>
        <div class="float-right" style="margin-left: 10px;">
            <?php if(count($availableViews->getRawValue()) > 1): ?>
            <?php echo __('Display mode')?>: 
            <select id="table-display" name="table-display">
                <?php foreach($availableViews->getRawValue() as $option) : ?>
                <option value="<?php echo $option ?>" <?php if($option == $settings['tableView']) echo 'selected="selected"' ?>><?php echo __($option) ?></option>
                <?php endforeach ?>
            </select>
            <?php endif ?>
        </div>
        <?php if($settings['search']): ?>
        <div class="float-right"><?php echo __('Search')?>: <input type="text" id="searchWord" value="<?php echo $settings['searchWord'] ?>">
            <div id="search-result" class="result-block" style="display: none; "></div>
        </div>
        <?php endif ?>
    </div>
</div>