	<header>
		<h1>Reports</h1>
	</header>
	
	<div id="menu">
		<?php echo link_to('Logout', '@sf_guard_signout'); ?>
	</div>
	
	<a href="<?php echo url_for('homepage');?>" id="back">Back</a>
	<div id="header-shadow"></div>
	
	<article>
		
		<section id="login-block">
			<div class="block-border"><div class="block-content no-padding">
				
				<h1>General overview</h1>
				
				<div class="block-controls">
					
					<ul class="controls-tabs js-tabs">
						<li><a href="#tab-today" title="Today"><img src="/images/icons/calendar/calendar-day.png" width="24" height="24"></a></li>
						<li><a href="#tab-week" title="This week"><img src="/images/icons/calendar/calendar-week.png" width="24" height="24"></a></li>
						<li><a href="#tab-month" title="This month"><img src="/images/icons/calendar/calendar-month.png" width="24" height="24"></a></li>
						<li><a href="#tab-year" title="This year"><img src="/images/icons/calendar/calendar-year.png" width="24" height="24"></a></li>
					</ul>
					
				</div>
				
				<div id="tab-today">
					
					<ul class="message success no-margin">
						<li><?php echo date('F j, Y'); ?></li>
					</ul>
					
					<ul class="blocks-list with-padding no-bottom-margin">
						<li>
							<a href="#" class="float-left">Income:</a>
							<ul class="tags float-right">
								<li class="tag">12.847 €</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Number of orders</a>
							<ul class="tags float-right">
								<li class="tag">840</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Average value: </a>
							<ul class="tags float-right">
								<li class="tag">14,23 €</li>
							
						</ul>
						</li>
						<li>
							<a href="#" class="float-left">New customers:</a>
							<ul class="tags float-right">
								<li class="tag">2 289</li>
							</ul>
						</li>
					</ul>
					
				</div>
				
				<div id="tab-week">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m")  , date("d")-7, date("Y"))); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<ul class="blocks-list with-padding no-bottom-margin">
						<li>
							<a href="#" class="float-left">Income:</a>
							<ul class="tags float-right">
								<li class="tag">66.210 €</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Number of orders</a>
							<ul class="tags float-right">
								<li class="tag">5 100</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Average value: </a>
							<ul class="tags float-right">
								<li class="tag">13,10 €</li>
							
						</ul>
						</li>
						<li>
							<a href="#" class="float-left">New customers:</a>
							<ul class="tags float-right">
								<li class="tag">11 912</li>
							</ul>
						</li>
					</ul>					
				</div>
				
				<div id="tab-month">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m")-1, date("d"), date("Y"))); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<ul class="blocks-list with-padding no-bottom-margin">
						<li>
							<a href="#" class="float-left">Income:</a>
							<ul class="tags float-right">
								<li class="tag">257.829 €</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Number of orders</a>
							<ul class="tags float-right">
								<li class="tag">19 991</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Average value: </a>
							<ul class="tags float-right">
								<li class="tag">13,05 €</li>
							
						</ul>
						</li>
						<li>
							<a href="#" class="float-left">New customers:</a>
							<ul class="tags float-right">
								<li class="tag">46 192</li>
							</ul>
						</li>
					</ul>						
				</div>
				
				<div id="tab-year">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m"), date("d"), date("Y")-1)); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<ul class="blocks-list with-padding no-bottom-margin">
						<li>
							<a href="#" class="float-left">Income:</a>
							<ul class="tags float-right">
								<li class="tag">3,097.170 €</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Number of orders</a>
							<ul class="tags float-right">
								<li class="tag">203 182</li>
							</ul>
						</li>
						<li>
							<a href="#" class="float-left">Average value: </a>
							<ul class="tags float-right">
								<li class="tag">12,99 €</li>
							
						</ul>
						</li>
						<li>
							<a href="#" class="float-left">New customers:</a>
							<ul class="tags float-right">
								<li class="tag">474 264</li>
							</ul>
						</li>
					</ul>	
					
				</div>

				
			</div></div>
		</section>
		
	</article>
