<?php

//for compatibility with dev-version
set_include_path(get_include_path().PATH_SEPARATOR.dirname(__FILE__).'/../lib/vendor/symfony/lib');
set_include_path(get_include_path().PATH_SEPARATOR.'/usr/share/php/symfony');
require_once 'autoload/sfCoreAutoload.class.php';

sfCoreAutoload::register();

// load all customized classes (required for SourceGuardian encoding)
require_once realpath(dirname(__FILE__).'/..').'/lib/autoload/myAutoloadConfigHandler.class.php';
require_once realpath(dirname(__FILE__).'/..').'/lib/autoload/mySimpleAutoload.class.php';
require_once realpath(dirname(__FILE__).'/..').'/lib/myPluginConfiguration.class.php';
require_once realpath(dirname(__FILE__).'/..').'/lib/Mnumi.class.php';
require_once realpath(dirname(__FILE__).'/..').'/lib/FileManager/FileManager.class.php';
require_once realpath(dirname(__FILE__).'/..').'/lib/Cast.class.php';

class ProjectConfiguration extends sfProjectConfiguration
{

    static protected $zendLoaded = false;
    static protected $twigLoaded = false;

    static public function registerZend()
    {
        if(self::$zendLoaded)
        {
            return;
        }
        set_include_path(dirname(__FILE__).'/../lib/vendor/'.PATH_SEPARATOR.get_include_path());
        require_once 'Zend/Loader/Autoloader.php';
        Zend_Loader_Autoloader::getInstance();
        self::$zendLoaded = true;
    }

    static public function registerTwig()
    {
        if(self::$twigLoaded)
        {
            return;
        }
        set_include_path(dirname(__FILE__).'/../lib/vendor/twig/lib/'.PATH_SEPARATOR.get_include_path());
        require_once 'Twig/Autoloader.php';

        Twig_Autoloader::register();

        set_include_path(dirname(__FILE__).'/../lib/vendor/twig-extensions/lib/'.PATH_SEPARATOR.get_include_path());
        require_once 'Twig/Extensions/Autoloader.php';
        Twig_Extensions_Autoloader::register();
        self::$twigLoaded = true;
    }

    /**
     * Get version of application, generated before 
     * build Debian package  
     */
    static public function getVersion()
    {
        $version = getenv('MNUMI_VERSION');

        return self::parseVersion($version);
    }

    static public function getRepositoryVersion()
    {
        $version = getenv('MNUMI_GITVERSION');

        return self::parseVersion($version);
    }

    static private function parseVersion($customVersion)
    {
        $version = '3.x-dev';

        if($customVersion !== false)
        {
            $version = $customVersion;

            // if beta version, display only second part: from "v1+v1.2-b1" to "v1.2-b1"
            if(preg_match('/v[\d\.]+\+(.*)/', $customVersion, $match))
            {
                $version = $match[1];
            }

            // reduce 4'th version character: from "v1.2.3.4" to "v1.2.3"
            if(preg_match('/(v\d+\.\d+\.\d+)\.\d+/', $version, $match))
            {
                $version = $match[1];
            }
        }

        return $version;
    }

    /**
     * Get site id for model APC cache - required for multisite
     *
     * @return string
     */
    static public function getSiteId()
    {
        if(sfContext::hasInstance())
        {
            return sfContext::getInstance()->getRequest()->getHost() . '_';
        }

        return null;
    }

    public function setup()
    {
        // creates singleton
        Mnumi::getInstance()->setEventDispatcher($this->dispatcher);

        if(getenv('PRODUCTION_ENVIRONMENT') == 1)
        {
            $this->setDebianStructure();
        }
        elseif((sfConfig::get('sf_environment') == 'test') || (php_sapi_name() == 'cli'))
        {
//            $this->enablePlugins('sfPhpunitPlugin');
        }

        // initalize Zend framework library
        self::registerZend();

        $this->enablePlugins('sfDoctrinePlugin');
        $this->enablePlugins('sfDoctrineGuardPlugin');
        $this->enablePlugins('sfTaskExtraPlugin');
        $this->enablePlugins('sfDoctrineNestedSetPlugin');
        $this->enablePlugins('sfFormExtraPlugin');
        $this->enablePlugins('sfImageTransformPlugin');
        $this->enablePlugins('sfI18NGettextPluralPlugin');
        $this->enablePlugins('sfPLValidatorsPlugin');
        $this->enablePlugins('sfActionCredentialsGetterPlugin');

        if(!defined("DOMPDF_ENABLE_REMOTE"))
        {
            define("DOMPDF_ENABLE_REMOTE", true);
        }

        if(!defined("DOMPDF_UNICODE_ENABLED"))
        {
            define("DOMPDF_UNICODE_ENABLED", true);
        }

        if(!defined("DOMPDF_FONT_DIR"))
        {
            define("DOMPDF_FONT_DIR", dirname(__FILE__).'/../lib/fonts/');
        }

        $this->enablePlugins('acDompdfPlugin');

        $this->registerEvents();


        // twig
        self::registerTwig();
    }

    public function configureDoctrine(Doctrine_Manager $manager)
    {
        // Enable callbacks so that softDelete behavior can be used
        $manager->setAttribute(Doctrine_Core::ATTR_USE_DQL_CALLBACKS, true);

        // This settings conflicting on many instances on one machine - temporary disabled (unit finish maintance MnumiCore 2.0)
        $manager->setAttribute(Doctrine_Core::ATTR_QUERY_CACHE, new Doctrine_Cache_Array());

        $manager->setAttribute(Doctrine_Core::ATTR_RESULT_CACHE, new Doctrine_Cache_Array());
        
        $manager->setCollate('utf8_general_ci');
        $manager->setCharset('utf8');
    }

    protected function setDebianStructure()
    {
        if(strpos(__FILE__, '/usr/share/mnumi/mnumicore3/') !== false)
        {
            // default debian package installation
            $this->setCacheDir('/var/cache/mnumi/mnumicore3');
            $this->setLogDir('/var/log/mnumi/mnumicore3');
        }
        else
        {
            // multisite debian package installation
            preg_match('/^\/usr\/share\/mnumi\/mnumicore3-mu\/(\d+)\//', __FILE__, $site);
            $siteId = $site[1];
            $this->setCacheDir('/var/cache/mnumi/mnumicore3-mu/'.$siteId);
            $this->setLogDir('/var/log/mnumi/mnumicore3-mu/'.$siteId);
        }
    }

    public function registerEvents()
    {
        // events - register listeners
        // user events
        $this->dispatcher->connect('user.create', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('user.password.lost', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('user.password.lost.shop', array('MyEvent', 'notificationToUserListener'));

        // package events
        $this->dispatcher->connect('package.ready', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('package.ready', array('MyEvent', 'createPrintLabelListener'));
        $this->dispatcher->connect('package.buy', array('MyEvent', 'notificationToUserListener'));

        // order events
        $this->dispatcher->connect('order.status.new', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('order.status.calculation', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('order.status.calculationNotLogged', array('MyEvent', 'notificationToUserListener'));

        // client events
        $this->dispatcher->connect('client.create', array('MyEvent', 'notificationToUserListener'));
        $this->dispatcher->connect('client.invitation', array('MyEvent', 'notificationToUserListener'));
    }

}
