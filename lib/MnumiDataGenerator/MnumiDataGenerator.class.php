<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Data generator class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
abstract class MnumiDataGenerator
{   
    protected $modelName;
    protected $methodArr;
    protected $dataArr;
    
    protected $targetFilename;
    protected $targetSampleFilename;
    protected $targetTmpFilename;
    protected $fileLineArr;
    
    
    
    /**
     * Constructor of MnumiDataGenerator.
     * 
     * @param MnumiDataSqlParser $mnumiDataMigrationObj
     */
    public function __construct(MnumiDataSqlParser $mnumiDataMigrationObj, $targetFilename = null)
    {
        $this->targetFilename = $targetFilename;
        if(!$targetFilename)
        {
            $this->targetFilename = __DIR__.'/MnumiFixturesLoader.class.php';
        }
        
        if(!file_exists($this->targetFilename))
        {
            fopen($this->targetFilename, "a");
            chmod($this->targetFilename, 0777);
        }
    
        if(!is_writable($this->targetFilename))
        {
            throw new Exception('File '.$this->targetFilename.' is not writable.');
        }

        // sets sample target filename
        $this->targetSampleFilename = $this->targetFilename.'.sample';
        
        if(!file_exists($this->targetSampleFilename))
        {
            throw new Exception($this->targetFilename.' does not exist.');
        }

        // sets tmp target filename (needed for recursion generating)
        $this->targetTmpFilename = $this->targetFilename.'.tmp';
        
        // if does not exist then copy from sample
        if(!file_exists($this->targetTmpFilename))
        {
            copy($this->targetSampleFilename, $this->targetTmpFilename);
            chmod($this->targetTmpFilename, 0777);
        }
        
        $this->modelName = Cast::camelize($mnumiDataMigrationObj->getTableName());
        $this->methodArr = $this->prepareMethods($mnumiDataMigrationObj->getColumns());
        $this->dataArr = $mnumiDataMigrationObj->getData();
        
        // gets file to array
        $this->fileLineArr = file($this->targetTmpFilename);
    }
  
    /**
     * Returns model name.
     * 
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Inserts generated text to file and saves.
     */
    public function insertIntoMarkedPlace()
    {
        // gets marked line
        $markLineNr = $this->findMarkedLine();

        // prepares parts with arrays
        $partArr[] = array_slice($this->fileLineArr, 0, $markLineNr - 1);
        $partArr[] = array($this->generate());
        $partArr[] = array_slice($this->fileLineArr, $markLineNr);

        $resultArr = array();
        foreach($partArr as $rec)
        {
            foreach($rec as $rec2)
            {
                $resultArr[] = $rec2;
            }
        }

        // output string
        $output = implode('', $resultArr);
        
        // saves text to file
        file_put_contents($this->targetTmpFilename, $output);
        file_put_contents($this->targetFilename, $output);
    }
    
    /**
     * Generates common data for every record code.
     * 
     * @return string
     */
    public function generate()
    {
        $text = '';
        $text .= "\n\t".'// '.$this->getModelName()."\n\n";
        
        return $text;
    }
    
    /**
     * Converts column names to methods.
     * 
     * @param type $columnArr
     * @return string
     */
    protected function prepareMethods($columnArr)
    {
        foreach($columnArr as $rec)
        {
            $methodArr[] = 'set'.Cast::camelize($rec);
        }
        
        return $methodArr;
    }
   
    /**
     * Returns line number with matched string.
     * 
     * @return mixed
     */
    protected function findMarkedLine()
    {
        $found = false;
        foreach($this->fileLineArr as $lineNumber => $line)
        {
            if(strpos($line, '@'.$this->getModelName()) !== false)
            {
                $found = true;
                $lineNumber++;
                break;
            }
        }
        
        if(!$found)
        {
            return null;
        }
        
        return $lineNumber;
    }
}
