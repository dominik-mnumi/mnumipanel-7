<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductFieldItem data generator class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataGeneratorProductFieldItem extends MnumiDataGenerator
{
    /**
     * Constructor of MnumiDataGenerator.
     * 
     * @param MnumiDataSqlParser $mnumiDataMigrationObj
     */
    public function __construct(MnumiDataSqlParser $mnumiDataMigrationObj)
    {
        parent::__construct($mnumiDataMigrationObj);
    }
  
    public function generate()
    {
        $text = parent::generate();

        // foreach record
        $nameArr = array();
        foreach($this->dataArr as $rec)
        {
            $text .= "\t".'// '.$rec[0]."\n";
            $text .= "\t".'$obj = new '.$this->modelName.'();'."\n";

            // foreach column
            foreach($this->methodArr as $key2 => $rec2)
            {
                // ommit setId
                if($rec2 == 'setId')
                {
                    continue;
                }
                // if null then insert directly without quotation marks
                elseif($rec[$key2] == 'NULL')
                {
                    $text .= "\t".'$obj->'.$rec2.'('.$rec[$key2].');'."\n";
                }
                elseif($rec2 == 'setProductFieldId')
                {
                    $text .= "\t".'$obj->setProductField($this->productField['.$rec[$key2].']);'."\n";
                }
                elseif($rec2 == 'setFieldItemId')
                {
                    $text .= "\t".'$obj->setFieldItem($this->fieldItem['.$rec[$key2].']);'."\n";
                }
                elseif($rec2 == 'setLabel')
                {
                    $text .= "\t".'$obj->'.$rec2.'($this->i18NObj->__(\''.$rec[$key2].'\'));'."\n";
                }                      
                else
                {
                    $text .= "\t".'$obj->'.$rec2.'(\''.$rec[$key2].'\');'."\n";
                }
            }

            $text .= "\t".'$obj->save();'."\n";
            $text .= "\t".'$this->'.lcfirst($this->modelName).'['.$rec[0].'] = $obj;'."\n\n";
        }

        return $text;
    }
}
