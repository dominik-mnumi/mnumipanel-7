<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sql parser class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
abstract class MnumiDataSqlParser
{
    protected $filename;
    protected $fileContent;
    protected $pattern;
    protected $methodArr;
    protected $insertArr;
    
    /**
     * Constructor of MnumiDataSqlParser.
     * 
     * @param string $filename
     */
    public function __construct($filename = null)
    {
        $this->filename = $filename;
        if(!$this->filename)
        {
            $this->filename = __DIR__.'/../../../data/v2MigrationFiles/v2Data.sql';
        }
        
        if(!file_exists($this->filename))
        {
            throw new Exception($this->filename.' does not exist.');
        }
        
        $this->fileContent = file_get_contents($this->filename);

        $this->parse();
    }
    
    /**
     * Parses file content and prepares column array and data array.
     */
    protected function parse()
    {
        preg_match($this->patternColumns, 
                $this->fileContent, 
                $matchArr);

        $insertString = $matchArr[2];
        
        preg_match_all('/`([a-z0-9_]*)`/i', 
                $matchArr[1], 
                $columnArr);

        $columnArr = $columnArr[1];
        
        // sets vars
        $this->methodArr = $columnArr;
        $this->insertArr = $this->prepareData($insertString);
    }
 
    /**
     * Returns prepared data in array.
     * 
     * @param array $data
     * @return array
     */
    protected function prepareData($data)
    {
        $explodedInsertArr = explode("\n", $data);
        $insertArr = array();

        // foreach VALUE row
        foreach($explodedInsertArr as $key => $rec)
        {
            // if empty then continue
            if(empty($rec))
            {
                continue;
            }

            // strips from some chars (from start and from end of row)
            // if last then cut off only one char
            if(end($explodedInsertArr) == $rec)
            {
                $rec = substr($rec, 0, -1);
            }
            else
            {
                $rec = substr($rec, 0, -2);
            }
            
            // cut off from beginning
            $rec = substr($rec, 1);
            
            // preg matches all fields
            preg_match($this->patternData, 
                    $rec, 
                    $dataArr);
 
            // unsets first because unnecessary
            unset($dataArr[0]);

            $valArr = array();
            foreach($dataArr as $rec2)
            {
                $valArr[] = $rec2;
            }
            
            $insertArr[] = $valArr;
        }
        
        return $insertArr;
    }
    
    /**
     * Returns array of columns.
     * 
     * @return array
     */
    public function getColumns()
    {
        return $this->methodArr;
    }
    
    /**
     * Returns array of data.
     * 
     * @return array
     */
    public function getData()
    {
        return $this->insertArr;
    }
}
