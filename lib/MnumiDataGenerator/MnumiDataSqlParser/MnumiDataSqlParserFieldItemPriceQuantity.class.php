<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sql parser class (for field_item_price_range).
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataSqlParserFieldItemPriceQuantity extends MnumiDataSqlParser
{
    protected $tableName = 'field_item_price_quantity';
    
    // order: `id`, `field_item_price_id`, `field_item_price_type_name`, `quantity`, `price`
    protected $patternColumns = "|INSERT INTO `field_item_price_quantity`(.*)VALUES([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*);|i"; 
    protected $patternData = "|([0-9A-Z\.]*), ([0-9A-Z\.]*), '([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*)', ([0-9A-Z\.-]*), ([0-9A-Z\.-]*)|i";
    
    /**
     * Creates instance of MnumiDataSqlParserProduct. 
     */
    public function __construct($filename = null)
    {
        parent::__construct($filename);
    }  
    
    /**
     * Returns table name.
     * 
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}
