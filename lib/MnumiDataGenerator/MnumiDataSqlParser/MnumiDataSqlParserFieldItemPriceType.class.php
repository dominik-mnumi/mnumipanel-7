<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sql parser class (for field_item_price_type).
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataSqlParserFieldItemPriceType extends MnumiDataSqlParser
{
    protected $tableName = 'field_item_price_type';
    
    // order: `name`
    protected $patternColumns = "|INSERT INTO `field_item_price_type`(.*)VALUES([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*);|i"; 
    protected $patternData = "|'([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*)'|i";
    
    /**
     * Creates instance of MnumiDataSqlParserProduct. 
     */
    public function __construct($filename = null)
    {
        parent::__construct($filename);
    }  
    
    /**
     * Returns table name.
     * 
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}
