<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sql parser class (for field_item_size).
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataSqlParserFieldItemSize extends MnumiDataSqlParser
{
    protected $tableName = 'field_item_size';
    
    // order: `id`, `field_item_id`, `pricelist_id`, `minimal_price`, `maximal_price`
    protected $patternColumns = "|INSERT INTO `field_item_size`(.*)VALUES([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*);|i"; 
    protected $patternData = "|([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*)|i";
    
    /**
     * Creates instance of MnumiDataSqlParserFieldItemSize. 
     */
    public function __construct($filename = null)
    {
        $this->filename = $filename;
        if(!$this->filename)
        {
            $this->filename = __DIR__.'/../../../data/v2MigrationFiles/fieldItemSize.sql';
        }
        
        if(!file_exists($this->filename))
        {
            throw new Exception($this->filename.' does not exist.');
        }
        
        $this->fileContent = file_get_contents($this->filename);

        $this->parse();
    }  
    
    /**
     * Returns table name.
     * 
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}
