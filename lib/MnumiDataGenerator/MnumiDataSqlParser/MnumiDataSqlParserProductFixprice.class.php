<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sql parser class (for product_fixprice).
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataSqlParserProductFixprice extends MnumiDataSqlParser
{
    protected $tableName = 'product_fixprice';
    
    // order: `id`, `product_id`, `pricelist_id`, `quantity`, `price`, `cost`
    protected $patternColumns = "|INSERT INTO `product_fixprice`(.*)VALUES([a-ząęółńśćźżĄĘÓŁŃŚĆŹŻ0-9\\\\<>@&;:=,\"\.#\!$\^&\*\?%+=/\n\r\s\(\)'_-]*);|i"; 
    protected $patternData = "|([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*), ([0-9A-Z\.]*)|i";
    
    /**
     * Creates instance of MnumiDataSqlParserProduct. 
     */
    public function __construct($filename = null)
    {
        parent::__construct($filename);
    }  
    
    /**
     * Returns table name.
     * 
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }
}
