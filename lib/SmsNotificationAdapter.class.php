<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * Integration with powiadomieniasms.pl
 *
 * @package   Extremouh
 * @author    Ernest Wojciuk <ernest@powiadomieniasms.pl>
 * @version   $Id$
 * @copyright Studiouh.com Sp. z o.
 */

class SmsNotificationAdapter 
{
    var $server = null; // declared in __construct();
    var $server_script = '/get/';
    var $server_script_send = '/send/';
    var $server_script_del = '/delete/';
    var $id = null; // declared in __construct();
    var $PubKay = null; // declared in __construct();
    var $PrivKay = null; // declared in __construct();
    var $smsid; #sms id in system
    var $fp; #server connection
    
    public function __construct()
    {
        $this->server = sfConfig::get('app_sms_server_host');
        $this->PubKay = sfConfig::get('app_sms_server_user');
        $this->PrivKay = sfConfig::get('app_sms_server_password');
        $this->id = sfConfig::get('app_sms_server_id');
        
        if(!$this->server || !$this->PubKay || !$this->PrivKay || !$this->id)
        {
            throw new Exception('Missing configuration data for SMS server.');
        } 
    }

    /**
     * Check  server connection
     * 
     * @return string
     */
    function checkConnection()
    {
        if ( (PHP_VERSION >= 4.3) && ($this->fp = @fsockopen($this->server, 80, $errno, $errstr, 30)) )
        {
            $type = 'socket';
        }
        return $type;
    }

    /**
     * Crypt message
     * 
     * @param string $string
     * @param string $key
     * 
     * @return array
     */
    private function smsCrypt($string, $key)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $string = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $string, MCRYPT_MODE_ECB, $iv);
        
        return array(base64_encode($string), base64_encode($iv));
    }

    /**
     * Return informations about server status
     * 
     * @return array
     */
    public function smsInfo()
    {
        $hash = md5($this->id.$this->PrivKay.'info');
        $connection = $this->checkConnection();
        $prm = "userid=" . $this->id . "&kay=" . $this->PubKay."&hash=" . $hash;

        if ($connection == 'socket')
        {
            $SMSresponse = '';

            $header = 'POST ' . $this->server_script . ' HTTP/1.0' . "\r\n" .
                    'Host: ' . $this->server . "\r\n" .
                    'Content-Type: application/x-www-form-urlencoded' . "\r\n" .
                    'Content-Length: ' . strlen($prm) . "\r\n" .
                    'Connection: close' . "\r\n\r\n";

            @fputs($this->fp , $header . $prm);
            while (!@feof($this->fp))
            {
                $res = @fread($this->fp , 1024);
                $SMSresponse .= $res;
            }
            @fclose($this->fp);

            if (eregi("<connect>.*<userid>([0-9]*)</userid>.*<smstotal>([0-9]*)</smstotal>.*<smsout>([0-9]*)</smsout>.*<smslastdate>(.*)</smslastdate>.*<key>(.*)</key>.*</connect>", $SMSresponse, $parts))
            {
                $info['userid']        =  $parts[1];
                $info['smstotal']      =  $parts[2];
                $info['smsout']        =  $parts[3];
                $info['smstoend']      =  $parts[2] - $parts[3];
                $info['smslastdate']   =  $parts[4];

                return $info;
            }
        }
    }

    /**
     * Delete sms
     * 
     * @param string $smsid
     */
    public function deleteSms($smsid)
    {
        if($smsid <= 0) return false;

        // hash is unique id based on few query parts
        $hash = md5($this->id.$this->PrivKay.$smsid);

        list($string, $iv) = $this->smsCrypt($smsid, $this->PrivKay);

        $prm = "userid=" . $this->id . "&smsid=" . $smsid."&iv=" . $iv . "&kay=" . $this->PubKay."&hash=" . $hash;

        $connection = $this->checkConnection();

        if ($connection == 'socket')
        {
            $SMSresponse = '';

            $header = 'POST ' . $this->server_script_del . ' HTTP/1.0' . "\r\n" .
                    'Host: ' . $this->server . "\r\n" .
                    'Content-Type: application/x-www-form-urlencoded' . "\r\n" .
                    'Content-Length: ' . strlen($prm) . "\r\n" .
                    'Connection: close' . "\r\n\r\n";

            @fputs($this->fp , $header . $prm);
            while (!@feof($this->fp ))
            {
                $res = @fgets($this->fp , 1024);
                $SMSresponse .= $res;
            }
            @fclose($this->fp);

            if(eregi('PSMS_DEL_OK', $SMSresponse))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        } 
        else
        {
            return 0;
        }
    }

    /**
     * Send sms for given number
     * 
     * @param string $number
     * @param string $text
     * @param date $datesms
     * @param string $smsid
     * @param integer $type
     * 
     * @return integer
     */
    public function sendSms($number, $text, $datesms='', $smsid='', $type=0)
    {
        // hash is unique id based on few query parts
        $hash = md5($this->id.$this->PrivKay.$number.$smsid);
        list($string, $iv) = $this->smsCrypt($text, $this->PrivKay);

        $prm = "userid=" . $this->id . "&smsid=" . $smsid."&number=" . $number . "&iv=" . $iv . "&text=" .rawurlencode($string). "&type=" . $type. "&datesms=" . $datesms. "&kay=" . $this->PubKay."&hash=" . $hash;

        $connection = $this->checkConnection();

        if ($connection == 'socket')
        {
            $SMSresponse = '';

            $header = 'POST ' . $this->server_script_send . ' HTTP/1.0' . "\r\n" .
                    'Host: ' . $this->server . "\r\n" .
                    'Content-Type: application/x-www-form-urlencoded' . "\r\n" .
                    'Content-Length: ' . strlen($prm) . "\r\n" .
                    'Connection: close' . "\r\n\r\n";

            @fputs($this->fp , $header . $prm);
            while (!@feof($this->fp ))
            {
                $res = @fgets($this->fp , 1024);
                $SMSresponse .= $res;
            }
            @fclose($this->fp);

            if(preg_match('/PSMS_OK/', $SMSresponse))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
}
?>
