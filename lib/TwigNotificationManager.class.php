<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Helps with servicing all object connected with twig configuration and database.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class TwigNotificationManager 
{
    private static $_twigNotificationManager;
    private $_twigObj;
    
    /**
     * Returns TwigNotificationManager instance.
     *
     * @param myUser $userObj
     * @param string $loaderClass
     * @return TwigNotificationManager 
     */
    public static function getInstance($userObj = null, $loaderClass = 'TwigNotificationDatabaseLoader')
    {
        if(!self::$_twigNotificationManager)
        {
            if(!$userObj)
            {
                $userObj = sfContext::getInstance()->getUser();
            }
            self::$_twigNotificationManager = new TwigNotificationManager($userObj, $loaderClass);
        }
        
        return self::$_twigNotificationManager;
    }

    /**
     * Creates instance of TwigNotificationManager.
     *
     * @param $userObj
     * @param string $loaderClass
     */
    public function __construct($userObj, $loaderClass = 'TwigNotificationDatabaseLoader')
    {
        // sets some twig configuration (translation files in e.g. i18n/pl/LC_MESSAGES/notification.no)
        putenv('LC_ALL='.$userObj->getCulture().'.UTF-8');
        $lcNumeric = setlocale(LC_NUMERIC, '0');
        setlocale(LC_ALL, $userObj->getCulture().'.UTF-8');   
        setlocale(LC_NUMERIC, $lcNumeric);
        bindtextdomain('notification', sfConfig::get('sf_root_dir').'/apps/mnumicore/i18n');
        bind_textdomain_codeset('notification', 'UTF-8');  
        textdomain('notification');
        
        // creates loader
        $loaderObj = new $loaderClass();
        $this->_twigObj = new Twig_Environment($loaderObj, 
                array('autoescape' => false,
                    'cache' => false));     
        $this->_twigObj->addExtension(new Twig_Extensions_Extension_I18n());
    }
    
    /**
     * Returns dynamic twig template class.
     * 
     * @param string $templateName
     * @param string $typeName
     * @return dynamic twig template class
     */
    public function getTwigTemplateObj($templateName, $typeName)
    {
        return $this->_twigObj->loadTemplate(
                TwigNotificationDatabaseLoader::createTemplateName(
                $templateName, $typeName));
    }

    /**
     * Returns Twig_Environment.
     *
     * @return Twig_Environment
     */
    public function getTwigObj()
    {
        return $this->_twigObj;
    }
}