<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mySimpleAutoload
 *
 * @author jupeter
 */
class mySimpleAutoload extends sfSimpleAutoload 
{
  /**
   * Retrieves the singleton instance of this class.
   *
   * @param  string $cacheFile  The file path to save the cache
   *
   * @return sfSimpleAutoload   A sfSimpleAutoload implementation instance.
   */
  static public function getInstance($cacheFile = null)
  {
    if (!isset(self::$instance))
    {
      self::$instance = new mySimpleAutoload($cacheFile);
    }

    return self::$instance;
  }
    
  /**
   * Loads configuration from the supplied files.
   *
   * @param array $files An array of autoload.yml files
   * 
   * @see sfAutoloadConfigHandler
   */
  public function loadConfiguration(array $files)
  {
    $config = new myAutoloadConfigHandler();
    foreach ($config->evaluate($files) as $class => $file)
    {
      $this->setClassPath($class, $file);
    }
  }
    
  /**
   * Adds a file to the autoloading system.
   *
   * @param string  $file     A file path
   * @param Boolean $register Whether to register those files as single entities (used when reloading)
   */
  public function addFile($file, $register = true)
  {
    if (!is_file($file))
    {
      return;
    }

    if (in_array($file, $this->files))
    {
      if ($this->cacheLoaded)
      {
        return;
      }
    }
    else
    {
      if ($register)
      {
        $this->files[] = $file;
      }
    }

    if ($register)
    {
      $this->cacheChanged = true;
    }

    $fileContent = file_get_contents($file);
    
    if(preg_match('~^<\?php \@"SourceGuardian"~mi', $fileContent))
    {
        $filename = basename($file);
        $class = substr($filename, 0, strpos($filename, '.'));
        $this->classes[strtolower($class)] = $file;
        
        return true;
    }

    preg_match_all('~^\s*(?:abstract\s+|final\s+)?(?:class|interface)\s+(\w+)~mi', $fileContent, $classes);
    foreach ($classes[1] as $class)
    {
      $this->classes[strtolower($class)] = $file;
    }
  }


}

?>
