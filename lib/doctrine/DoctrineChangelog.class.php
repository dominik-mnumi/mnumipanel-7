<?php
/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Changelog class for handling versionable tables
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class DoctrineChangelog
{
    // options for changelog
    private $options;
    
    // changes for given object and its relations
    private $changes = array();
    
    /**
     * Constructor
     * 
     * @param $options - Changelog options
     */
    function __construct($options = array())
    {
        if(!isset($options['columns']))
        {
            throw new Exception('Columns for main object are required.');
        }
        if(isset($options['relation']) && (!isset($options['relation']['columns']) || !isset($options['relation']['model'])))
        {
          throw new Exception('Model and columns for relation must be specified.');
        }
        
        $this->options = $options;
    }
    
    /**
     * Returns versions differences that are ready to presentation
     * 
     * @param $object - Doctrine_record Object for differences
     * @return array
     */
    public function getDifferences($object)
    {
        // differences for main object
        $this->getObjectDifferences($object, $this->options['columns']);
        
        // differences for relations objects
        foreach($this->options['relations'] as $name => $relation)
        { 
            $relatedObjects = Doctrine::getTable($name)
                ->createQuery('ro')
                ->leftJoin('ro.'.get_class($object).' o')
                ->andWhere('o.id = ?', $object->getId())
                ->andWhere('ro.deleted_at is null OR ro.deleted_at is not null')
                ->execute();
            
            $this->getRelatedObjectDifferences($relatedObjects, $relation['columns']);
        }

        return $this->sortResults($this->changes);
    }
    
    /**
     * Returns differences for given object
     *
     * @param $object - Doctrine_record Object for differences
     * @param $columns - array - list of column to get changes
     * @param $relatedObject boolean - determines if object is main or related
     * 
     * @return array
     */
    private function getObjectDifferences($object, $columns, $relatedObject = false)
    {
        // get all version for object (deleted are NOT inculded)
        $versions = range(1, $object->getVersion());

        $currentObject = $previousObject = null;
        
        foreach($versions as $version)
        {
            $changelogSet = null;
            
            // revert object to previous version
            $currentObject = $object->revert($version);
            $currentObject->refreshRelated();
            
            // check if object was added
            if($version['version'] == 1)
            {
                $changelogSet = new DoctrineChangelogSetAdd($currentObject->getEditor(), strtotime($currentObject->getCreatedAt()));
                foreach($columns as $option)
                {
                    if(is_array($option))
                    {
                        $get = 'get'.key($option);
                        $option = current($option);
                    }
                    else
                    {
                        $get = 'get'.$option;
                    }
                    $changelogSet->addValue(StringTool::splitByCapitals($option), $currentObject->$get());
                }
                $this->changes[] = $changelogSet;
            }

            if($currentObject && $previousObject)
            {
                $changelogSet = new DoctrineChangelogSetEdit($currentObject->getEditor(), strtotime($currentObject->getUpdatedAt()));
                
                // special flag for deleted objects (if only UpdatedAt and DeletedAt was changed then we dont count  it as change)
                $changesWasSet = false;
                
                foreach($columns as $option)
                {
                    if(is_array($option))
                    {
                        $get = 'get'.key($option);
                        $option = current($option);
                    }
                    else
                    {
                        $get = 'get'.$option;
                    }
                    
                    if((string)$currentObject->$get() != (string)$previousObject->$get())
                    {
                        $changesWasSet = true;
                        $changelogSet->addValue(StringTool::splitByCapitals($option), $previousObject->$get(), $currentObject->$get());
                    }
                }
                
                if($changesWasSet)
                {
                    $this->changes[] = $changelogSet;
                }
            }


            $previousObject = $currentObject->copy(false);
        }
        
        // check if object was deleted
        if($relatedObject && $object->getDeletedAt())
        {
            $changelogSet = new DoctrineChangelogSetDelete($object->getEditor(), strtotime($object->getDeletedAt()));
            foreach($columns as $option)
            {
                $get = 'get'.$option;
                if(is_array($option))
                {
                    $get = 'get'.key($option);
                    $option = current($option);
                }
                $changelogSet->addValue(StringTool::splitByCapitals($option), $object->$get());
            }
            $this->changes[] = $changelogSet;
        }
        
        return $versions;
    }
    
    /**
     * Get differences for related objects
     *
     * @param $objects - Doctrine_collection Objects for differences
     * @param $columns - array - list of column to get changes
     *
     */
    private function getRelatedObjectDifferences($objects, $columns)
    {
        foreach($objects as $object)
        {
            $this->getObjectDifferences($object, $columns, true);
        }
    }
    
    /**
     * Sorting result table by edit time
     * 
     * @return array
     */
    private function sortResults($versionChanges)
    {
        usort($versionChanges, array('DoctrineChangelog' , 'compareEditDate'));
        return $versionChanges;
    }
    
    /**
     * Method to sort elements by Edit date
     *
     * @return array
     */
    private static function compareEditDate($a, $b)
    {
        return strnatcmp($b->getEditedAt(), $a->getEditedAt());
    }
  
}