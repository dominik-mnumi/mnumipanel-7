<?php

/**
 * OrderPackage filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderPackageFormFilter extends BaseOrderPackageFormFilter
{
  public function configure()
  {
      $packageStatusPaymentNameArray = array('', 'paid', 'non paid');
      $packageStatusNameArray = $this->getPackageStatusArr();

      $this->setWidgets(array(
          'carrier_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Carrier'), 'add_empty' => true)),
          'payment_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Payment'), 'add_empty' => true)),
          'payment_status'            => new sfWidgetFormSelect(array('choices' => $packageStatusPaymentNameArray, 'label' => 'Payment status')),
          'order_package_status_name' => new sfWidgetFormSelect(array('choices' => $packageStatusNameArray, 'label' => 'Package status')),
      ));

      $this->setValidators(array(
          'carrier_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Carrier'), 'column' => 'id')),
          'payment_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Payment'), 'column' => 'id')),
          'payment_status'            => new sfValidatorChoice(array('choices' => array_keys($packageStatusPaymentNameArray))),
          'order_package_status_name' => new sfValidatorChoice(array('choices' => array_keys($packageStatusNameArray))),
      ));

      $this->widgetSchema->setNameFormat('order_package_filters[%s]');
  }
  
  /**
   * Add custom query to payment_status column.
   *
   * @param Doctrine_Query $query
   * @param string $field
   * @param int $value
   */
  protected function addPaymentStatusColumnQuery(Doctrine_Query $query, $field, $value)
  {
      if($value == 1)
      {
          $query->andWhere('r.payment_status_name = ?', 'paid');
      }
      elseif($value == 2)
      {
          $query->andWhere('r.payment_status_name <> ?', 'paid');
      }
  }
  
  /**
   * Add custom query to order_package_status_name column.
   *
   * @param Doctrine_Query $query
   * @param string $field
   * @param int $value
   */
  protected function addOrderPackageStatusNameColumnQuery(Doctrine_Query $query, $field, $value)
  {
      if($value != '0')
      {
          $query->andWhere('r.order_package_status_name = ?', $value);
      }
  }
  
  /**
   * Returns package status in prepared array (for widget choice).
   * 
   * @return array 
   */
  protected function getPackageStatusArr()
  {
      $orderPackageColl = OrderPackageStatusTable::getInstance()->findAll();
      
      $orderPackageArr = array('');
      foreach($orderPackageColl as $rec)
      {
          $orderPackageArr[$rec->getName()] = $rec->getTitle();
      }    
          
      return $orderPackageArr;
  }
}