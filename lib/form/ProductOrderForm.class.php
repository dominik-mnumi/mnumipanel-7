<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductOrderForm
 *
 * @package    mnumishop
 * @subpackage form
 * @author     Adam Marchewicz
 */
class ProductOrderForm extends BaseForm
{
    static $type = array(
        1 => 'string',
        4 => 'select',
        10 => 'size',
    );
    
    protected $size_metric;
    
    // custom validators for fields
    protected $customValidators = array(
        'count' => 
            array('type' => 'integer',
                  'options' => array('required' => true), 
                  'messages' => array('required' => 'Field "count" is required', 'invalid' => 'Field "count" is not an integer.')),
        'quantity' => 
            array('type' => 'integer',
                  'options' => array('required' => true), 
                  'messages' => array('required' => 'Field "quantity" is required', 'invalid' => 'Field "quantity" is not an integer.'))
        );

    public function configure()
    {
        parent::configure();

        $fields = $this->getOption('fields');
        $otherDefaults = $this->getOption('otherDefaults');
        $otherWidgetArray = array();
        
        foreach($fields as $row)
        {
            $fieldName = $row['field_name'];
            $fieldParam = $row['field_param'];
            $fieldLabel = $row['field_title'];
            $fieldDefaultValue = $row['field_def_val'];

            if($fieldName == 'OTHER')
            {
                $choiceArray = $this->getChoices($fieldParam);
                
                // if OTHER which have more than one option
                if(count($fieldParam) > 1)
                {
                    $otherWidgetArray[$row['field_id']] = new sfWidgetFormSelect(
                            array('choices' => $choiceArray,
                                'label' => $fieldLabel));
                    // needed because we need to set defaults later
                    $otherDefaults[$row['field_id']] = $fieldDefaultValue;
                }
                // for OTHER with one option
                else if(count($fieldParam) == 1)
                {
                    $value = array_keys($choiceArray);
                    $value = $value[0];

                    $otherWidgetArray[$row['field_id']] = new sfWidgetFormInputCheckbox(
                            array('label' => $fieldLabel,
                                'default' => false),
                            array('class' => 'mini-switch',
                                'value' => $value,
                                'val' => $value));
                    /* We do not set defaults for checkbox. If it is not checked
                     * it just doesn't exist. Setting default value, provide to
                     * always-checked form item.
                     */
                }
            }
            /* General product fields. (sides, quantity, count, etc...) */
            else if($fieldName != 'WIZARD')
            {
                $type_id = (integer) $row['field_type'];
                $type = 'renderFormElement'.ucfirst(self::$type[$type_id]);
                $name = strtolower($fieldName);
                $this->$type($name, $row);
                if(!$this->hasDefault($name))
                {
                    $this->setDefault($name, $fieldDefaultValue);
                }
                $this->widgetSchema->setLabel($name, $fieldLabel);
            }
        }

        if(!empty($otherWidgetArray))
        {
            $this->setWidget('other', new sfWidgetFormSchema($otherWidgetArray));
            if(!$this->hasDefault('other'))
            {
                $this->setDefault('other', $otherDefaults);
            }
            $this->setValidator('other', new sfValidatorPass());
        }

        $this->getWidgetSchema()->setNameFormat('order[%s]');
    }

    public function renderFormElementString($name, $row)
    {
        $this->setWidget($name, new sfWidgetFormInputText());
        
        // custom validator (for different than string)
        if(array_key_exists($name, $this->customValidators))
        {
            $validatorClassName = 'sfValidator'.ucfirst($this->customValidators["$name"]['type']);
            $this->setValidator($name, new $validatorClassName($this->customValidators["$name"]['options'], $this->customValidators["$name"]['messages']));
        }
        else
        {
            $this->setValidator($name, new sfValidatorString());
        }
    }

    public function renderFormElementSelect($name, $row)
    {
        $this->setWidget($name, new sfWidgetFormSelect(
                array('choices' => $this->getChoices($row['field_param']))));
        $this->setValidator($name, new sfValidatorChoice(array('choices' => array_keys($row['field_param']))));
    }

    public function renderFormElementSize($name, $row)
    {
        $special_choiceid = false;
        foreach($row['field_param'] as $row_item)
        {
            if($row_item['type'] == 'special')
            {
                $special_choiceid = $row_item['id'];
            }
        }
        $choices = $this->getChoices($row['field_param']);

        if($special_choiceid)
        {
            // get special choice
            $special_choice_item = array($choices[$special_choiceid]);

            // remove from list of strings
            unset($choices[$special_choiceid]);

            // put as last of choices
            list($special_value) = $special_choice_item;
            $choices[$special_choiceid] = $special_value;
        }

        $this->setWidget($name, new sfWidgetFormSelect(array('choices' => $choices), array('class' => ($special_choiceid) ? 'with_special_choice' : '')));
        $this->setValidator($name, new sfValidatorChoice(array('choices' => array_keys($choices))));

        if($special_choiceid)
        {
            // gets customizable object
            $customizableObj = $this->getOption('customizableObj');

            // width
            $this->setWidget($name.'_width',
                    new sfWidgetFormInputText(array('label' => 'Width'), 
                            array('class' => 'size size_width', 
                                'value' => ($customizableObj) ? $customizableObj->getWidth() : '')));
            $this->setValidator($name.'_width',
                    new sfValidatorString(array('required' => false)));

            // height
            $this->setWidget($name.'_height',
                    new sfWidgetFormInputText(array('label' => 'Height'), 
                            array('class' => 'size size_height',
                                'value' => ($customizableObj) ? $customizableObj->getHeight() : '')));
            $this->setValidator($name.'_height',
                    new sfValidatorString(array('required' => false)));
        }
    }
  
    private function getChoices($field_param)
    {
        $choices = array();
        $emptyField = false;

        foreach($field_param as $value)
        {
            if($value['name'] == FieldItemTable::$emptyField)
            {
                $emptyField = $value['id'];
                continue;
            }


            $choices[$value['id']] = $value['name'];
        }

        if($emptyField)
        {
            return array($emptyField => FieldItemTable::$emptyField) + $choices;
        }

        return $choices;
    }
}
