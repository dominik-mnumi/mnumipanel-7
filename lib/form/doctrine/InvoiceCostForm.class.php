<?php

/**
 * InvoiceCost form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceCostForm extends BaseInvoiceCostForm
{
    public function configure()
    {
        $this->setCssClasses('full-width');
        
        // widgets
        $this->setWidget('user_id', new sfWidgetFormInputHidden());
        $this->setWidget('client_id', new sfWidgetFormInputHidden());
        $this->setWidget('client',
                new sfWidgetFormInputText(array('label' => 'Customer'),
                        array('class' => 'full-width')));
        $this->setWidget('invoice_date',
                new sfWidgetFormInput(array(),
                        array('class' => 'datepicker')));
        $this->setWidget('payment_date',
                new sfWidgetFormInput(array(),
                        array('class' => 'datepicker')));

        // validators
        $this->setValidator('client', new sfValidatorString(array('required' => false)));
        $this->getValidator('invoice_number')->setOption('required', true);
        $this->getValidator('invoice_date')->setOption('required', true);
        $this->getValidator('payment_date')->setOption('required', true);
        $this->getValidator('amount')->setOption('required', true);

        // sets default error messages
        $this->setMessages();
        
        // sets custom error
        $this->getValidator('client_id')->setMessage('required', 'Field "%label%" is invalid');

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->useFields(array('invoice_number', 'invoice_date', 'payment_date',
            'notice', 'user_id', 'client', 'client_id', 'amount'));
    }

}
