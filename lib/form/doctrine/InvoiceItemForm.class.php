<?php

/**
 * InvoiceItem form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceItemForm extends BaseInvoiceItemForm
{
    public function configure()
    {
        // widgets
        $this->setWidget('valid', new sfWidgetFormInputHidden(
                array('default' => 0), 
                array('class' => 'invoice_item_valid')));
        $this->setWidget('order_id', new sfWidgetFormInputHidden(
                array('default' => null)));
        
        $this->getWidget('description')->setAttribute('class', 'full-width item_description');
        $this->getWidget('unit_of_measure')->setAttribute('class', 'full-width item_unit_of_measure');
        $this->getWidget('quantity')->setAttribute('class', 'full-width item_quantity');
        $this->getWidget('price_net')->setAttribute('class', 'full-width item_price_net');
        $this->getWidget('price_discount')->setAttribute('class', 'full-width item_price_discount');
        $this->getWidget('tax_value')->setAttribute('class', 'full-width item_tax');
        
        // validators
        $this->setValidator('valid', new sfValidatorPass());
        $this->setValidator('quantity', new sfValidatorNumberExtended());
        $this->setValidator('price_net', new sfValidatorNumberExtended());
        $this->setValidator('price_discount', new sfValidatorNumberExtended(array('required' => false)));
       
        // messages
        $this->setMessages();

        $invoiceItem = $this->getObject();

        //disable fields for invoice with correction
        if(!$this->isNew())
        {
            if($invoiceItem->getInvoice()->hasChildren())
            {
                foreach ($this->getWidgetSchema()->getFields() as $field)
                {
                  $field->setAttribute('disabled', 'disabled');
                }
              
            }
        }
        
        $this->setDefault('tax_value', sfConfig::get('app_invoice_default_tax',23));
        
        $this->useFields(array('description', 'unit_of_measure', 'quantity', 
            'price_net', 'price_discount', 'tax_value',
            'valid',
            'order_id'));
    }

}
