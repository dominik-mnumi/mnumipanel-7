<?php

/**
 * 
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: BasesfGuardRequestForgotPasswordForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class myGuardRequestForgotPasswordForm extends BaseFormDoctrine
{
    public function configure()
    {
        $this->setWidget('email_address', new sfWidgetFormInput(
                array('label' => ' Enter your e-mail address or username')));
        $this->setValidator('email_address', new sfValidatorString());

        $this->widgetSchema->setNameFormat('forgot_password[%s]');
        $this->validatorSchema->setPostValidator(new sfGuardRequestForgotValidatorUser());

        $this->useFields(array('email_address'));
    }

    public function getModelName()
    {
        return 'sfGuardForgotPassword';
    }

    public function save($con = null)
    {
        $userObj = Doctrine::getTable('sfGuardUser')->createQuery('u')
                ->where('u.email_address = ? OR u.username = ? AND u.is_active = ?', array($this['email_address']->getValue(), $this['email_address']->getValue(), 1))
                ->execute()
                ->getFirst();

        $time = time();

        $reqPassObj = new sfGuardForgotPassword();
        $reqPassObj->setUserId($userObj->getId());

        // deletes previous ask
        $userObj->deleteOldUserForgotPassword();
        
        //sets the same date in create process
        $reqPassObj->setCreatedAt(date("Y-m-d G:i:s", $time));
        $reqPassObj->setUpdatedAt(date("Y-m-d G:i:s", $time));
        $reqPassObj->setExpiresAt(date("Y-m-d G:i:s", $time + sfConfig::get('app_forgot_pass_expiration_time')));
        $reqPassObj->setUniqueKey(sfGuardForgotPasswordTable::getInstance()->generateForgotToken($userObj->getPassword(), $time));
        $reqPassObj->save();

        return $reqPassObj;
    }   
}