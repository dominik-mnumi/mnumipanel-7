<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * RestUserContactMobilePhoneForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestUserContactMobilePhoneForm extends BaseUserContactForm
{
    public function configure()
    {
        parent::configure();
        
        $this->disableCSRFProtection();

        $this->setWidgets(array(
            'value' => new sfWidgetFormInputHidden()
        ));

        $this->setValidators(array(
            'value' => new sfValidatorString(array('required' => true, 'max_length' => 100))
        ));
    }
}