<?php 
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * time from given date until now
 * @return string
 */

function allTimeBack($date, $default = "")
{
    if($date == "")
    {
        return $default;
    }

    $oldD = substr($date,8,2);
    $oldM = substr($date,5,2);
    $oldY = substr($date,0,4);

    if(substr($date,0,4) == '0000')
    {
        return '0 '.sfContext::getInstance()->getI18N()->__('days');
    }
    $date1 = mktime (substr($date,11,2),substr($date,14,2),substr($date,17,2),substr($date,5,2),substr($date,8,2),substr($date,0,4));
    $date2 = mktime (date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
    $totalDays = ($date2 - $date1 ) / 86400;
    if($totalDays < 1)
    {
        $direction = "&lt;";
        $totalHours = ($date2 - $date1 ) / 3600;
        if($totalHours < 1) 
        {
            $totalMinutes = ($date2 - $date1 ) / 60;
            $show = floor($totalMinutes).' '.sfContext::getInstance()->getI18N()->__('min').'.';
        } 
        else 
        {
            $totalMinutes = (($date2 - $date1 )-(floor($totalHours)*3600)) / 60;
            $show = floor($totalHours).' '.sfContext::getInstance()->getI18N()->__('hr').'. '.floor($totalMinutes).' '.sfContext::getInstance()->getI18N()->__('min').'.';
        }
    } 
    else 
    {
        $show = floor($totalDays).' '.sfContext::getInstance()->getI18N()->__('days');
    }
    
    return $show;
}