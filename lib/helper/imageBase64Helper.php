<?php 
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Print base64 string from filePath data
 *
 * @param string $filePath filepath to image 
 * @return string Image data in base64 or empty string if error
 */
function getFilePathAsBase64( $filePath )
{
    try{
        $data = file_get_contents($filePath);
        $filetype = mime_content_type($filePath);
    
        return 'data:' . $filetype . ';base64,' . base64_encode($data);    
    } 
    catch(Exception $e)
    {
        
    }

    //send empty image data - result no image
    return '';
}

/**
 * Print base64 string from fileURL data
 *
 * @param string $fileUrl file URL to image
 * @return string Image data in base64 or empty string if error
 */
function getUrlAsBase64( $fileUrl )
{
    try{
        $data = file_get_contents($fileUrl);
        $header = get_headers($fileUrl, 1);
        if (array_key_exists('Content-Type', $header)) {
            $filetype = $header['Content-Type'];
        } else {
            $filetype = 'image/png';
        }

        return 'data:' . $filetype . ';base64,' . base64_encode($data);
    } 
    catch(Exception $e)
    {
        
    }

    //send empty image data - result no image
    return '';
}