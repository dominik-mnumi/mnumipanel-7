<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
* Stripes text to a given length
*
* returns string
*/
function stripText($str, $length = 100)
{
    if(strlen($str) > $length)
    {
        $text = substr(($str = wordwrap($str,$length,'$$')),0,strpos($str,'$$')); 
        if(strlen($str) > $length)
        {
            $text .= "...";
        }
    }
    else
    {
        $text = $str;
    }
    
    return $text;
}
