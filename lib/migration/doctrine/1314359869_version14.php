<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version14 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropTable('delivery_payment');
        $this->dropTable('delivery');
        $this->createTable('carrier_payment', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'carrier_id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'length' => '4',
             ),
             'payment_id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'length' => '4',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_carrier_has_payment_payment1' => 
              array(
              'fields' => 
              array(
               0 => 'payment_id',
              ),
              ),
              'fk_carrier_has_payment_carrier1' => 
              array(
              'fields' => 
              array(
               0 => 'carrier_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
              1 => 'carrier_id',
              2 => 'payment_id',
             ),
             'charset' => 'utf8',
             ));
             
        $this->createForeignKey('carrier_payment', 'carrier_payment_carrier_id_carrier_id', array(
             'name' => 'carrier_payment_carrier_id_carrier_id',
             'local' => 'carrier_id',
             'foreign' => 'id',
             'foreignTable' => 'carrier',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('carrier_payment', 'carrier_payment_payment_id_payment_id', array(
             'name' => 'carrier_payment_payment_id_payment_id',
             'local' => 'payment_id',
             'foreign' => 'id',
             'foreignTable' => 'payment',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->addIndex('carrier_payment', 'carrier_payment_carrier_id', array(
             'fields' => 
             array(
              0 => 'carrier_id',
             ),
             ));
        $this->addIndex('carrier_payment', 'carrier_payment_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('carrier_payment', 'carrier_payment_carrier_id_carrier_id');
        $this->dropForeignKey('carrier_payment', 'carrier_payment_payment_id_payment_id');
        $this->removeIndex('carrier_payment', 'carrier_payment_carrier_id', array(
             'fields' => 
             array(
              0 => 'carrier_id',
             ),
             ));
        $this->removeIndex('carrier_payment', 'carrier_payment_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
             
        $this->createTable('delivery', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '45',
             ),
             'description' => 
             array(
              'type' => 'clob',
              'length' => '65535',
             ),
             'price' => 
             array(
              'type' => 'float',
              'length' => '',
             ),
             'active' => 
             array(
              'type' => 'boolean',
              'length' => '25',
             ),
             'shipping_address' => 
             array(
              'type' => 'boolean',
              'length' => '25',
             ),
             'free_shipping' => 
             array(
              'type' => 'float',
              'length' => '',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => '',
             'charset' => 'utf8',
             ));
        $this->createTable('delivery_payment', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'delivery_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             'payment_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_delivery_payment_delivery1' => 
              array(
              'fields' => 
              array(
               0 => 'delivery_id',
              ),
              ),
              'fk_delivery_payment_payment1' => 
              array(
              'fields' => 
              array(
               0 => 'payment_id',
              ),
              ),
              'u_delivery_payment' => 
              array(
              'fields' => 
              array(
               0 => 'delivery_id',
               1 => 'payment_id',
              ),
              'type' => 'unique',
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => '',
             'charset' => 'utf8',
             ));
        $this->dropTable('carrier_payment');
    }
}