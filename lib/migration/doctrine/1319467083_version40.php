<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version40 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('client', 'client_pricelist_id_pricelist_id');
        $this->createForeignKey('client', 'client_pricelist_id_pricelist_id_1', array(
             'name' => 'client_pricelist_id_pricelist_id_1',
             'local' => 'pricelist_id',
             'foreign' => 'id',
             'foreignTable' => 'pricelist',
             'onUpdate' => 'no action',
             'onDelete' => 'set null',
             ));
        $this->addIndex('client', 'client_pricelist_id', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             ));
    }

    public function down()
    {
        $this->createForeignKey('client', 'client_pricelist_id_pricelist_id', array(
             'name' => 'client_pricelist_id_pricelist_id',
             'local' => 'pricelist_id',
             'foreign' => 'id',
             'foreignTable' => 'pricelist',
             'onUpdate' => 'set null',
             'onDelete' => 'set null',
             ));
        $this->dropForeignKey('client', 'client_pricelist_id_pricelist_id_1');
        $this->removeIndex('client', 'client_pricelist_id', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             ));
    }
}