<?php
/**
 * Adds fieldset 'WIZARD'.
 */
class Version134 extends Doctrine_Migration_Base
{
    public function up()
    {        
        // if not exist then create
        if(!FieldsetTable::getInstance()->findOneByName('WIZARD'))
        {          
            $obj = new Fieldset();
            $obj->setName('WIZARD');
            $obj->setLabel('Wizard');
            $obj->setHidden(1);
            $obj->setChangeable(0);
            $obj->save();
            
            $treeObj = Doctrine_Core::getTable('Fieldset')->getTree();
            $treeObj->createRoot($obj);
        }
    }

    public function down()
    {
        if(FieldsetTable::getInstance()->findOneByName('WIZARD'))
        { 
            FieldsetTable::getInstance()->findOneByName('WIZARD')->delete();
        }
    }
}