<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version160 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('file', 'file_order_id_orders_id');
        $this->createForeignKey('file', 'file_order_id_orders_id_1', array(
             'name' => 'file_order_id_orders_id_1',
             'local' => 'order_id',
             'foreign' => 'id',
             'foreignTable' => 'orders',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->addIndex('file', 'file_order_id', array(
             'fields' => 
             array(
              0 => 'order_id',
             ),
             ));
    }

    public function down()
    {
        $this->createForeignKey('file', 'file_order_id_orders_id', array(
             'name' => 'file_order_id_orders_id',
             'local' => 'order_id',
             'foreign' => 'id',
             'foreignTable' => 'orders',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->dropForeignKey('file', 'file_order_id_orders_id_1');
        $this->removeIndex('file', 'file_order_id', array(
             'fields' => 
             array(
              0 => 'order_id',
             ),
             ));
    }
}