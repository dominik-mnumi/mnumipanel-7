<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version305 extends Doctrine_Migration_Base
{
    public function up()
    {
        // foreach client name and client address trims unnecessary white marks
        foreach(InvoiceTable::getInstance()->findAll() as $rec)
        {
            $rec->setClientName(trim($rec->getClientName()));
            $rec->setClientAddress(trim($rec->getClientAddress()));
            $rec->save();
        }
    }

    public function down()
    {

    }
}