<?php

/**
 * FieldItem
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class FieldItem extends BaseFieldItem
{
    /**
     * Get formatted cost
     *
     * @return string Cost with formatter (ex. "0.00" instead "0")
     */
    public function getCost()
    {
        $price = $this->_get('cost');
        $pricesion = mnumicoreConfiguration::getSettingPricePrecision();

        $floatPrice = round($price, $pricesion);

        return sprintf("%01.".$pricesion."f", $floatPrice);
    }

    /**
     * Get list of all related pricelist ids
     * 
     * @return array
     */
    public function getUsedPricelistIds()
    {
        $result = array();
        foreach($this->getFieldItemPrices() as $row)
        {
            $result[] = $row->getPricelistId();
        }
        return $result;
    }

    /**
     * Return field item price query
     * 
     * @param Doctrine_Query $query
     * @param unknown_type $pricelistId
     * @return Doctrine_Query
     */
    public function getFieldItemPriceByPricelistIdQuery($query = null,
            $pricelistId = null)
    {
        if($query == null)
        {
            $query = Doctrine::getTable('FieldItemPrice')->createQuery('fip')
                    ->where('fip.field_item_id = ?', $this->getId());
        }

        $query = $query->where('fip.field_item_id = ?', $this->getId());

        if($pricelistId != null)
        {
            $query->addWhere('fip.pricelist_id = ?', $pricelistId);
        }

        // 86400 secs = 24 hours * 60 seconds * 60 minutes = 24 hours = 1 day
        $lifetime = 86400;
        $resultCacheHash = $this->getCachePrefix('pricelist_' . $pricelistId);

        $query = $query->useResultCache(true, $lifetime, $resultCacheHash);

        return $query;
    }

    /**
     * Get path to cache key, adding name suffix
     *
     * @param string $name
     * @return string
     */
    public function getCachePrefix($name = null)
    {
        if($name != null)
        {
            $name = '_' . $name;
        }
        return FieldItemTable::getInstance()->getCachePrefix() . $this->getId() . $name;
    }

    /**
     * Returns field item price for current price list
     *
     * @param integer $pricelistId
     * @return Doctrine_Record
     */
    public function getFieldItemPrice($pricelistId = null)
    {
        return $this->getFieldItemPriceByPricelistIdQuery(null, $pricelistId)->fetchOne();
    }

    /**
     * Get FieldItemPrice object
     * 
     * @param int $pricelistId
     * @return FieldItemPrice|boolean
     */
    public function getFieldItemPriceOrDefault($pricelistId = null)
    {
        $fieldItemPrice = $this->getFieldItemPriceByPricelistIdQuery(null,
                        $pricelistId)->fetchOne();

        if($fieldItemPrice instanceOf FieldItemPrice)
        {
            return $fieldItemPrice;
        }

        $defaultPricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
        $fieldItemPrice = $this->getFieldItemPriceByPricelistIdQuery(null,
                        $defaultPricelist)->fetchOne();

        if($fieldItemPrice instanceOf FieldItemPrice)
        {
            return $fieldItemPrice;
        }
        
        return false;
    }

    public function getMeasureType($count = 1, $quantity = 1, $factor = 1,
            $pricelistId = null)
    {
        $range = (integer)round($factor);
        if($range == 0)
        {
            $range = 1;
        }

        //gets price object
        $priceObj = $this->getFieldItemPriceOrDefault($pricelistId);

        if(!$priceObj)
        {
            throw new Exception('Price object for selected pricelist does not exist');
        }

        return $this->getMeasureUnit()->getFormalName();
    }

    /**
     * Method to return name or label (if is set)
     * 
     * @param $extendedInformation boolean
     * @return string
     */
    public function getFieldLabel($extendedInformation = false)
    {
        $label = $this->getName();

        if($this->getLabel())
        {
            $label = $this->getLabel();
        }
        elseif($extendedInformation)
        {   
            // if label is not null and diff then name
            if($this->getLabel() && $this->getName() != $this->getLabel())
            {
                $i18NObj = sfContext::getInstance()->getI18N();
                $label = $i18NObj->__($this->getName()).' - '.$i18NObj->__($this->getLabel());
            }
        }
       
        return $label;
    }

    public function canDelete()
    {
        return true;
    }

    /**
     * Returns true if current field item is in given fieldset branch.
     * 
     * @param string $fieldsetBranch
     * @return boolean
     * @throws Exception 
     */
    public function isInFieldsetBranch($fieldsetBranch)
    {
        $fieldsetObj = FieldsetTable::getInstance()->findOneByName($fieldsetBranch);
        if(!$fieldsetObj)
        {
            throw new Exception('No such fieldset.');
        }
        
        $fieldItemColl = $fieldsetObj->getFieldItemQuery()->execute();
        $fieldItemArr = array();
        foreach($fieldItemColl as $rec)
        {
            $fieldItemArr[] = $rec->getId();
        }
        
        return in_array($this->getId(), $fieldItemArr) 
                ? true
                : false;
    }

    public function preSave($event)
    {
        $cacheDriver = $this->getTable()->getAttribute(Doctrine_Core::ATTR_RESULT_CACHE);
        $cacheDriver->deleteByPrefix($this->getCachePrefix());
    }

    /**
     * Returns default field item price object.
     *
     * @return Doctrine_Record
     */
    public function getDefaultFieldItemPrice()
    {
        return FieldItemPriceTable::getInstance()->createQuery()
            ->where('field_item_id = ?', $this->getId())
            ->addWhere('pricelist_id = ?', PricelistTable::getInstance()->getDefaultPricelist()->getId())
            ->fetchOne();
    }
}
