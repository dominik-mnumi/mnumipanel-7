<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * File system extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportFileSystem extends Report
{
    public function checkStatus($value = null)
    {
        $status = 'green';
        
        if(!is_writable(sfConfig::get('sf_root_dir').'/web/uploads'))
        {
            $status = 'red';
        }

        if(!is_writable(sfConfig::get('sf_root_dir').'/web/uploads/category'))
        {
            $status = 'red';
        }

        if(!is_writable(sfConfig::get('sf_root_dir').'/web/uploads/product'))
        {
            $status = 'red';
        }

        return $status;
    }

    public function getValue()
    {
        $string = null;

        if(is_writable(sfConfig::get('sf_root_dir').'/web/uploads'))
        {
            $string .= '/web/uploads';
        }

        if(is_writable(sfConfig::get('sf_root_dir').'/web/uploads/category'))
        {
            ($string) ? $string .= ', /web/uploads/category'
                      : $string .= '/web/uploads/category';
        }

        if(is_writable(sfConfig::get('sf_root_dir').'/web/uploads/product'))
        {
            ($string) ? $string .= ', /web/uploads/product'
                      : $string .= '/web/uploads/product';
        }

        return $string;
    }
}