<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PHP extension extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportPHPExtension extends Report
{
    static $extensionArray = array('gettext', 'json', 'libxml', 'mbstring', 'mcrypt', 'SimpleXML', 'xsl');

    public function checkStatus($value = null)
    {
        $status = 'green';

        ($value) ? $extensionCurrentArray = $value
                 : $extensionCurrentArray = get_loaded_extensions();

        foreach(self::$extensionArray as $rec)
        {
            if(!in_array($rec, $extensionCurrentArray))
            {
                $status = 'red';
            }
        }

        return $status;
    }

    public function getValue()
    {
        $extensionCurrentArray = get_loaded_extensions();

        $string = implode(", ", $extensionCurrentArray);
        return $string;
    }
}