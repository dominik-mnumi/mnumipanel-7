<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Memory limit extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportPHPMemoryLimit extends Report
{
    public function checkStatus($value = null)
    {
        ($value) ? $memoryLimit = $value
                 : $memoryLimit = ini_get("memory_limit");

        if(strnatcmp('256M', $memoryLimit) <= 0)
        {
            return 'green';
        }
        else if(strnatcmp('128M', $memoryLimit) <= 0)
        {
            return 'yellow';
        }
        else
        {
            return 'red';
        }
    }

    public function getValue()
    {
        $memoryLimit = ini_get("memory_limit");

        return $memoryLimit;
    }
}