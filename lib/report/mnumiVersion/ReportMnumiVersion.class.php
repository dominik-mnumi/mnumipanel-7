<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PHP version extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
abstract class ReportMnumiVersion extends Report
{
    public static $versionSource = 'http://security.mnumi.com/latest-version.json'; 
    
    protected static $production = 'production';
    protected static $nightly = 'nightly';
    protected static $beta = 'beta';

    protected $versionSourceArr = array();
    
    // version
    protected $version;
    
    // production, nightly or beta
    protected $versionType;
  
    /**
     * Returns prepared version source array.
     * 
     * @param array $arr
     * @return array
     * @throws Exception 
     */
    public static function getPreparedVersionSourceArr($arr)
    {
        $result = file_get_contents($arr);
        if(!$result)
        {
            throw new Exception('Cannot get latest version of applications.');
        }
        
        // prepares version array (cuts off first letter - v)
        $versionSourceArr = (array)json_decode($result);
        foreach($versionSourceArr as &$rec)
        {
            $rec = substr($rec, 1);
        }
        
        return $versionSourceArr;
    }
    
    /**
     * Creates instance of extending class (this is abstract).
     * 
     * @param string $forcedVersion needed for tests
     * @throws Exception 
     */
    public function __construct($forcedVersion = null)
    { 
        $this->versionSourceArr = self::getPreparedVersionSourceArr(self::$versionSource);
        $this->version = $this->getVersion($forcedVersion);
        $this->versionType = $this->getVersionType();
        
        parent::__construct();
    }

    /**
     * Returns status for current version of application.
     * 
     * @param string $value
     * @return string 
     */
    public function checkStatus($value = null)
    {
        $value ? $version = $value
               : $version = $this->version;

        switch($this->versionType)
        {
            // compare versions
            case self::$production:
                if(version_compare($version, $this->versionSourceArr[$this->mnumiApplication]) >= 0)
                {
                    return self::$statusGreen;
                }
                else 
                {
                    return self::$statusRed; 
                }
            // for nightly checks dates    
            case self::$nightly:        
                $currentDate = date('Ymd');
                $currentTime = strtotime($currentDate);
                $versionTime = strtotime($version);
                if($versionTime >= $currentTime)
                {
                    return self::$statusGreen;
                }

                // gets yesterday date
                $yesterdayTime = strtotime('-1 day', $currentTime); 
                if($versionTime >= $yesterdayTime) 
                {
                    return self::$statusYellow; 
                }
                
                if($versionTime < $yesterdayTime) 
                {
                    return self::$statusRed; 
                }
            // for beta always yellow    
            case self::$beta:
                return self::$statusYellow;
        }

    }
    
    /**
     * Returns status info.
     * 
     * @return string 
     */
    public function getStatusInfo()
    {
        // sets default status
        $statusLabel = $this->currentStatus;
        
        // if nightly or beta
        if(in_array($this->versionType, array(self::$nightly, self::$beta)))
        {
            // for yellow status
            if($this->currentStatus == self::$statusYellow)
            {
                $statusLabel = self::$statusYellow.'_'.$this->versionType;
            }             
        }

        return (array_key_exists('status', $this->info)) ?  $this->info['status'][$statusLabel]
                                                         :  null;
    }
    
    /**
     * Gets version of Mnumi application in extending classes. 
     */
    protected function getVersion($forcedVersion = null)
    {
        // parameter sets version as given parameter
        if($forcedVersion)
        {
            return $forcedVersion;
        }

        $version = exec('dpkg -s '.$this->mnumiApplication.' | grep "Version: " | sed \'s/Version: //g\'');

        // gets only version (not package reloaded)
        $arr = explode('-', $version);
        if(count($arr) > 1)
        {
            $version = $arr[0];
        }

        if(empty($version))
        {
            throw new Exception('Cannot get version of MnumiPanel.');
        }
        
        return $version;
    }
    
    /**
     * Returns version type: production, nightly or beta.
     * 
     * @return string 
     */
    protected function getVersionType()
    {
        // gets application version
        $version = empty($this->version) ? $this->getVersion() : $this->version;

        // production
        if(preg_match('/^(\d+\.){1}(\d+\.)?(\d+\.)?(\d+)$/', $version))
        {
            return self::$production;
        }
        
        // nightly
        if(preg_match('/^\d{8}$/', $version))
        {
            return self::$nightly;
        }   
        
        // beta
        if(preg_match('/^(\d+\.){1}(\d+\.)?(\d+\.)?(\d+)\+(.+)$/', $version))
        {
            return self::$beta;
        }  
    }

    public static function visible()
    {
        return (getenv('PRODUCTION_ENVIRONMENT') !== false);
    }
}