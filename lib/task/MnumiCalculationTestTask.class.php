<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php');

/**
 * Installs sample data.
 */
class MnumiCalculationTest extends sfDoctrineBaseTask
{

    /**
     * @see sfTask
     * 
     * example cli use: ./symfony mnumi:sample-data [--application="mnumicore"] [--env="dev"] [--no-confirmation] [--language="en"] [--generate-data-only]
     * language default: en
     */
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('dir', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', '1'),
            new sfCommandOption('id', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', false),

        ));

        $this->namespace        = 'mnumi';
        $this->name             = 'calculation-test';
        $this->briefDescription = 'Calculation test';

        $this->detailedDescription = <<<EOF
The [mnumi:calculation-test|INFO] task loads sample data into the database:
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $databaseManager = new sfDatabaseManager($this->configuration);       
        $environment     = $this->configuration instanceof sfApplicationConfiguration ? $this->configuration->getEnvironment() : 'all';

        if($options['id']) {
            $orders = array(OrderTable::getInstance()->find($options['id']));
        } else {
            /** @var Order[] $orders */
            $orders = OrderTable::getInstance()->findAll();
        }


        $dir = '/tmp/calc/' . $options['dir'];

        $this->logBlock('Destination directory: ' . $dir, 'INFO');

        @mkdir('/tmp/calc');
        @mkdir($dir);

        foreach ($orders as $order) {
            $id = $order->getId();

            if ($id < 2800) continue;

            $filename = $dir . '/' . $id . '.txt';

            if(file_exists($filename)) {
                continue;
            }

            try {
                $this->logBlock(" - order: " . $order->getId(), 'INFO');
                $message = var_export($order->calculate(), 1);
            } catch (Exception $e) {
                $message = "Ignore: " . $order->getId() . ': ' . $e->getMessage();
                $this->logBlock($message, 'ERROR');
            }
            file_put_contents($filename, $message);

            unset($order);
        }
   }
}