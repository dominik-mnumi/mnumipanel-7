<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php');

/**
 * Installs data.
 */
class MnumiDataLoadTask extends sfDoctrineBaseTask
{

    /**
     * @see sfTask
     * 
     * example cli use: ./symfony mnumi:data-load [--application="mnumicore"] [--env="dev"] [--no-confirmation] [--language="en"]
     * language default: en
     */
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('language', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application language', 'en'),
            new sfCommandOption('no-confirmation', null, sfCommandOption::PARAMETER_NONE, 'Whether to force proceed'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),         
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev')
        ));

        $this->namespace        = 'mnumi';
        $this->name             = 'data-load';
        $this->briefDescription = 'Loads data from sql file';

        $this->detailedDescription = <<<EOF
The [mnumi:dump-load|INFO] task loads data from sql file:
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {        
        // gets sql file
        $filename = sfConfig::get("sf_data_dir").'/v3MigrationFiles/v3Dump_'.$options['language'].'.sql';
        if(!file_exists($filename))
        {
            throw new Exception('Data for specified language does not exist. Please run mnumi:data-dump task first.');
        }
        
        // database drop
        $this->logSection('mnumi', 'dropping database... ');
        
        // prepares array with options and unsets some unnecessary 
        $withoutSomeOptionArr = $options;
        unset($withoutSomeOptionArr['language']);

        // drops and creates database for specific environment
        if($this->runTask('doctrine:drop-db', array(), $withoutSomeOptionArr))
        {
            $this->logSection('mnumi', 'task aborted');
            return 1;
        }

        // database create
        $this->logSection('mnumi', 'creating database... ');

        unset($withoutSomeOptionArr['no-confirmation']);
        $this->runTask('doctrine:build-model', array(), $withoutSomeOptionArr);
        $this->runTask('doctrine:create-db', array(), $withoutSomeOptionArr);
        $this->runTask('doctrine:build-sql', array(), $withoutSomeOptionArr);
        $this->runTask('doctrine:insert-sql', array(), $withoutSomeOptionArr);

        // clears cache
        $this->logSection('mnumi', 'cache cleaning... ');
        $this->clearCache();

        // loads sample data
        $this->logSection('mnumi', 'loading data...'); 
        $sqlContent = file_get_contents($filename);
        
        // gets current connection
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();

        $result = $conn->execute($sqlContent);
        
        // loads sample data
        $this->logSection('mnumi', 'loading finished successfully...');

        
    }

    /**
     * Clears cache.
     */
    protected function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir').'/'.sfConfig::get('sf_app').'/'.$env.'/';
            $cache    = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
    }

}