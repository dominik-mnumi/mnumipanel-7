<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php');

/**
 * Installs sample data.
 */
class MnumiInstallSampleData extends sfDoctrineBaseTask
{

    /**
     * @see sfTask
     * 
     * example cli use: ./symfony mnumi:sample-data [--application="mnumicore"] [--env="dev"] [--no-confirmation] [--language="en"] [--generate-data-only]
     * language default: en
     */
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),
            new sfCommandOption('language', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application language', 'en'),           
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('no-confirmation', null, sfCommandOption::PARAMETER_NONE, 'Whether to force dropping of the database'),
            new sfCommandOption('generate-data-only', null, sfCommandOption::PARAMETER_NONE, 'The application sample data loader generator')
        ));

        $this->namespace        = 'mnumi';
        $this->name             = 'sample-data';
        $this->briefDescription = 'Loads Sample site fixture data';

        $this->detailedDescription = <<<EOF
The [mnumi:sample-data|INFO] task loads sample data into the database:
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $databaseManager = new sfDatabaseManager($this->configuration);       
        $environment     = $this->configuration instanceof sfApplicationConfiguration ? $this->configuration->getEnvironment() : 'all';

        // DATA LOAD SECTION
        
        // GENERATOR SECTION
        if(!empty($options['generate-data-only']))
        {
            // loads sample data
            $this->logSection('mnumi', 'generating data...');

            // removes tmp files before
            $fixturesTmpGeneratorFilename = __DIR__.'/../MnumiDataGenerator/MnumiFixturesLoader.class.php.tmp';
            if(file_exists($fixturesTmpGeneratorFilename))
            {
                unlink($fixturesTmpGeneratorFilename);
            }

            // some preparation for MnumifixturesLoader

            $fixturesSampleGeneratorFilename = __DIR__.'/../MnumiDataGenerator/MnumiFixturesLoader.class.php.sample';
            $fixturesDestinationGeneratorFilename = __DIR__.'/../MnumiDataGenerator/MnumiFixturesLoader.class.php';
            copy($fixturesSampleGeneratorFilename, $fixturesDestinationGeneratorFilename);

            // pricelist           
            $mnumiDataGeneratorObj = new MnumiDataGeneratorPricelist(new MnumiDataSqlParserPricelist());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();

            // field_item    
            $mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItem(new MnumiDataSqlParserFieldItem());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
            
            // field_item_price 
            $mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItemPrice(new MnumiDataSqlParserFieldItemPrice());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
         
            // field_item_price_type
            $mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItemPriceType(new MnumiDataSqlParserFieldItemPriceType());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();         

            // field_item_price_quantity
            $mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItemPriceQuantity(new MnumiDataSqlParserFieldItemPriceQuantity());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
            
            // field_item_size
            //$mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItemSize(new MnumiDataSqlParserFieldItemSize());
            //$mnumiDataGeneratorObj->insertIntoMarkedPlace();
       
            // field_item_material
            //$mnumiDataGeneratorObj = new MnumiDataGeneratorFieldItemMaterial(new MnumiDataSqlParserFieldItemMaterial());
            //$mnumiDataGeneratorObj->insertIntoMarkedPlace();
            
            // product
            $mnumiDataGeneratorObj = new MnumiDataGeneratorProduct(new MnumiDataSqlParserProduct());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
        
            // product_field
            $mnumiDataGeneratorObj = new MnumiDataGeneratorProductField(new MnumiDataSqlParserProductField());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
     
            // product_field_item
            $mnumiDataGeneratorObj = new MnumiDataGeneratorProductFieldItem(new MnumiDataSqlParserProductFieldItem());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
    
            // product_fixprice
            $mnumiDataGeneratorObj = new MnumiDataGeneratorProductFixprice(new MnumiDataSqlParserProductFixprice());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();
           
            // product_wizard
            $mnumiDataGeneratorObj = new MnumiDataGeneratorProductWizard(new MnumiDataSqlParserProductWizard());
            $mnumiDataGeneratorObj->insertIntoMarkedPlace();

        }
        // LOADER SECTION
        else
        { 
            // prepares array with options and unsets some unnecessary 
            $withoutSomeOptionArr = $options;
            unset($withoutSomeOptionArr['language']);
            unset($withoutSomeOptionArr['generate-data-only']);

            // drops and creates database for specific environment
            if($this->runTask('doctrine:drop-db', array(), $withoutSomeOptionArr))
            {
                $this->logSection('mnumi', 'task aborted');
                return 1;
            }

            unset($withoutSomeOptionArr['no-confirmation']);
            $this->runTask('doctrine:create-db', array(), $withoutSomeOptionArr);
            $this->runTask('doctrine:build-sql', array(), $withoutSomeOptionArr);
            $this->runTask('doctrine:insert-sql', array(), $withoutSomeOptionArr);

            // clears cache
            $this->logSection('mnumi', 'Cache cleaning... ');
            $this->clearCache();

            // loads sample data
            $this->logSection('mnumi', 'Loading data...');
            
            // sets language
            $contextObj = sfContext::createInstance($this->configuration)
                    ->getUser()
                    ->setCulture($options['language']);

            MnumiFixturesLoader::getInstance($contextObj)->loadSampleData();
        }
    }
    
    /**
     * Clears cache.
     */
    protected function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir') . '/' . sfConfig::get('sf_app') . '/'.$env.'/';
            $cache = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
        
    }

}