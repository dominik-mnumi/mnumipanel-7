<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Import layout from external site.
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class configureApiKeyTask extends sfBaseTask
{
    protected $applicationKey = array(
        'MnumiWizard' => array(
            'configKey' => 'wizard',
            'host' => '%s',
            'api_key_name' => 'secret_key'
         ),
    	'MnumiStudio' => array(
    		'configKey' => 'studio_rest',
    		'host' => '%s',
    		'url' => '%sapi_server.php',
    		'api_key_name' => 'api_key'
    	),
	);
    
    /**
     * @see sfTask
     */
    protected function configure()
    {
        $this->namespace = 'mnumi';
        $this->name = 'configure-key';
        $this->briefDescription = 'Configure API configuration with Mnumi applications';

        $this->detailedDescription = '
            The [mnumi:configure-key|INFO] configure MnumiShop with external Mnumi application. 
            
            Available applications: 
             - MnumiStudio
             - MnumiPanel
             - MnumiWizard
            
            Usage example: 
              [./symfony mnumi:configure-key application |INFO]
        ';
        
        $this->addArguments(array(
            new sfCommandArgument('appname', sfCommandArgument::REQUIRED, 'The application name'),
            new sfCommandArgument('hostname', sfCommandArgument::REQUIRED, 'The application host name'),
            new sfCommandArgument('webapi-key', sfCommandArgument::REQUIRED, 'The application webapi key'),
        ));
        
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore')
        ));

    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $appName = $arguments['appname'];
        $hostname = $arguments['hostname'];
        $webapi = $arguments['webapi-key'];
        
        $availableApplications = array_keys($this->applicationKey);
        if(!in_array($appName, $availableApplications))
        {
            $this->logBlock(sprintf('Wrong application name "%s" (allowed: %s)', $appName, implode(', ', $availableApplications)), 'ERROR');
            exit(1);
        }
        
        if(substr($hostname, 0, 7) != 'http://' && substr($hostname, 0, 8) != 'https://')
        {
            $this->logBlock('Hostname must have "http(s)://" at the begin.', 'ERROR');
            exit(1);
        }
        
        if(substr($hostname, -1) != '/')
        {
            $this->logBlock('Hostname must have slash "/" at the end.', 'ERROR');
            exit(1);
        }
        $appConfiguration = sfConfig::get('sf_app_config_dir') .'/app.yml';
        
        if(!file_exists($appConfiguration))
        {
            $this->logBlock(sprintf('Configuration file (%s) does not exist.', $appConfiguration), 'ERROR');
            exit(1);
        }
        
        $config = sfYaml::load($appConfiguration);
        
        $appConfig = $this->applicationKey[$appName];
        $appKey = $appConfig['configKey'];
        $config['all'][$appKey]['host'] = $hostname;
        if(array_key_exists('url', $appConfig))
        {
            $config['all'][$appKey]['url'] = sprintf($appConfig['url'], $hostname);
        }
        $config['all'][$appKey][$appConfig['api_key_name']] = $webapi;
        
        $result = file_put_contents($appConfiguration, sfYaml::dump($config, 4)); 
        if($result === false)
        {
            $this->logBlock(sprintf('Could not save (%s) file.', $appConfiguration), 'ERROR');
            exit(1);
        }
        $this->clearCache();
        $this->logBlock(sprintf('Saved new API configuration for "%s" application.', $appName), 'INFO');
        exit(0);
    }
    
    private function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir') . '/' . sfConfig::get('sf_app') . '/'.$env.'/';
            $cache = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
        
    }

}

