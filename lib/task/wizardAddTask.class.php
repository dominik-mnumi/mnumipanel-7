<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiCreateWebapi
 *
 * @author jupeter
 */
class wizardAddTask extends sfDoctrineBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));
    
    $this->addArguments(array(
         new sfCommandArgument('productName', sfCommandArgument::REQUIRED, 'Product name'),
         new sfCommandArgument('wizards', sfCommandArgument::REQUIRED, 'Comma separated wizards'),
    ));

    $this->namespace        = 'wizard';
    $this->name             = 'add';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [wizard:add|INFO] releate wizard to product.
Call it with:

  [php symfony wizard:add "product-slug" wizard1,wizard2,wizard3|INFO]
EOF;
  }
  
  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    
    // check if product exist
    if(! ($product = ProductTable::getInstance()->findOneBySlug($arguments['productName'])))
    {
    	$this->logBlock('Could not find "'. $arguments['productName'] .'" product.', 'ERROR');
    	exit(1);
    }
    
    $wizards = split(',', $arguments['wizards']);
    
    $i = 0;
    foreach($wizards as $wizard)
    {
    	// verify if ProductWizard already exist
    	if(ProductWizardTable::getInstance()->findOneByProductIdAndWizardName(
    			$product->getId(), $wizard))
    	{
    		continue;
    	}
    	
    	$row = new ProductWizard();
    	$row->setProduct($product);
    	$row->setWizardName($wizard);
    	$row->save();
    	$i++;
    }
    
    // if all projects already imported
    if($i == 0)
    {
    	$this->logBlock('No project related. Probably all projects already reladed to product "'. $arguments['productName'] .'".', 'INFO');
    	exit(0);
    }
    
	$this->logBlock($i . ' projects releated successfull to product "'. $arguments['productName'] .'".', 'INFO');
    exit(0);
  }
}

?>
