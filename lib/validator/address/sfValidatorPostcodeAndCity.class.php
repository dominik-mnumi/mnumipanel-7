<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorPostcodeAndCity
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorPostcodeAndCity extends sfValidatorString
{
    protected function configure($options = array(), $messages = array())
    {
        $options = array_merge($options, array('trim' => true));

        parent::configure($options, $messages);
    }
    
    /**
     * Extend doClean method.
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $value = parent::doClean($value);
        $postcodeAndArray = explode(' ', $value, 2);

        if(2 != count($postcodeAndArray) 
                || empty($postcodeAndArray[0]) 
                || empty($postcodeAndArray[1]))
        {
            throw new sfValidatorError($this, 'Field "Postcode and city" is invalid. Format: Postcode City', array('value' => $value));
        }
        
        return $value;
    }
}