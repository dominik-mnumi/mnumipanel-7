<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class sfGuardRequestForgotValidatorUser extends sfValidatorBase
{

    public function configure($options = array(), $messages = array())
    {
        $this->addOption('throw_global_error', false);

        $this->setMessage('invalid', 'The requested email or username is invalid');
    }

    protected function doClean($values)
    {
        $username = isset($values['email_address']) ? $values['email_address'] : '';

        if($username)
        {
            $userObj = $this->getTable()->createQuery('u')
                    ->where('u.email_address = ? OR u.username = ? AND u.is_active = ?', array($username, $username, 1))
                    ->execute()
                    ->getFirst();

            // user exists?
            if($userObj)
            {
                return $values;
            }
        }

        if($this->getOption('throw_global_error'))
        {
            throw new sfValidatorError($this, 'invalid');
        }

        throw new sfValidatorErrorSchema($this, array('email_address' => new sfValidatorError($this, 'invalid')));
    }

    protected function getTable()
    {
        return Doctrine::getTable('sfGuardUser');
    }
}
