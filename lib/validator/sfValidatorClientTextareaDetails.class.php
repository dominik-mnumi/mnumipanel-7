<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorClientTextareaDetails
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorClientTextareaDetails extends sfValidatorString
{
    /**
     * Extends doClean method. 
     *
     * @param string $value
     */
    protected function doClean($value)
    {        
        $detailsArray = preg_split( '/\n/', $value);
        
        // first checks if details have 3 lines (client name, address and postode with city)
        if(count($detailsArray) != 3 && count($detailsArray) != 4)
        {
            throw new sfValidatorError($this, 'Field "Client details" has invalid data');
        }
        
        // first checks first line
        $valueTmp = trim($detailsArray[0]);
        if(empty($valueTmp))
        {
            throw new sfValidatorError($this, 'Field "Client details" has invalid data');
        }

        // checks address
        $valueTmp = trim($detailsArray[1]);
        if(empty($valueTmp))
        {                                            // checks address
        $valueTmp = trim($detailsArray[1]);
        if(empty($valueTmp))
        {
            throw new sfValidatorError($this, 'Field "Client details" has invalid data');
        }

            throw new sfValidatorError($this, 'Field "Client details" has invalid data');
        }

        // checks postcode and city
        $postcodeAndCity = new sfValidatorPostcodeAndCity();
        $postcodeAndCity->setMessage('invalid', 'Field "Client details" has invalid data');
        $postcodeAndCity->setMessage('required', 'Field "Client details" - postcode and city are required (xx-xxx City)');
        $postcodeAndCity->clean($detailsArray[2]);

        // checks country
        $valueTmp = isset($detailsArray[3]) ? trim($detailsArray[3]) : null;
        if(!empty($valueTmp) && $valueTmp != 'Polska')
        {
            if(!in_array($valueTmp, array_values(Doctrine_Validator_Country::getCountries())))
            {
                throw new sfValidatorError($this, 'Incorrect country in "Client details". Please put country name in english');
            }
        }

        return parent::doClean($value);
    }
}