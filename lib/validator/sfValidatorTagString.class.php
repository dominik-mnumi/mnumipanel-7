<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorTagString
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorTagString extends sfValidatorString
{
    /**
     * Extend doClean method. Replacing last "/" by " "
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $string = strtolower($value);

        //prepares string
        $replaceArray = array(' ', '--', '&quot;', '!', '@', '#', '$', '%', '^', '&', '*',
            '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?',
            '[', ']', '\\', ';', "'", ',', '.', '/', '*', '+', '~', '`',
            '=');
        $string = str_replace($replaceArray, ' ', $string);
        
        return parent::doClean($string);
    }
}