<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
  get('/client')->

  with('request')->begin()->
    isParameter('module', 'client')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
  end()
;

$browser->
    get('client')->
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Clients/')->
    end();

#
# checking visibility of user edit page links
#

mnumiTestFunctional::loadData(); //reload data because it was deleted
$browser = mnumiTestFunctional::getLoggedInstance('office');

$client = ClientTable::getInstance()->findOneByFullname('client1');

$browser
  ->info('  Checking client edit page')
  ->get('/client/edit/'.$client->getId())
  ->info('    User from fixtures does not have edit_client_general_information permission')
  ->with('response')->
    begin()
      ->isStatusCode(200)
      ->info('      does not see edit general information button')
      ->checkElement('#edit_client_general_information_container button:contains("Change")', false)
      ->info('      sees edit credits buttton')
      ->checkElement('#edit_client_credits_container button:contains("Change")')
      ->info('      does not see edit deliveries buttton')
      ->checkElement('#edit_client_deliveries_container button:contains("Change")', false)
      ->info('      sees edit client attachments buttton')
      ->checkElement('#edit_client_attachments_container button:contains("Change")')
    ->end();

// 20
$browser = mnumiTestFunctional::getLoggedInstance();
$browser
  ->info('  Checking client edit page as Admin')
  ->get('/client/edit/'.$client->getId())
  ->info('    Admin sees all')
  ->with('response')->
    begin()
      ->isStatusCode(200)
      ->checkElement('#edit_client_general_information_container button:contains("Change")')
      ->checkElement('#edit_client_credits_container button:contains("Change")')
      ->checkElement('#edit_client_deliveries_container button:contains("Change")')
      ->checkElement('#edit_client_attachments_container button:contains("Change")')
    ->end();

$attachments = $client->ClientAttachments;
$attachment = $attachments[0];

$addresses = $client->ClientAddresses;
$address = $addresses[0];

$browser = mnumiTestFunctional::getLoggedInstance('UserClientActionsFunctional');
$browser
  ->info('  Checking client with no *client_edit* permissions')
  ->get('/client/edit/'.$client->getId())
  ->with('response')
    ->begin()
      ->isStatusCode(200)
    ->end()
    
  ->get('/client/'.$client->getId().'/attachment')
  ->with('response')
     ->begin()
       ->isStatusCode(403)
     ->end()

  #---------- attachments --------------------   
  ->get('/client/attachment/edit/' . $attachment->getId())
    ->with('response')
      ->begin()
        ->isStatusCode(403)
      ->end()     
  ->post('/client/attachment/delete/' . $attachment->getId())
      ->with('response')
        ->begin()
          ->isStatusCode(403)
        ->end()
  ->post('/client/attachment/deleteMany', array('selected' => array($attachment->getId())))
        ->with('response')
          ->begin()
            ->isStatusCode(403)
          ->end()
          
  #---------- delivery --------------------                    
   ->get('/client/'.$client->getId().'/delivery')
     ->with('response')
        ->begin()
          ->isStatusCode(403)
        ->end()       
  ->get('/delivery/edit/' . $address->getId())
     ->with('response')
       ->begin()
         ->isStatusCode(403)
       ->end()     
   ->post('/delivery/delete/' . $address->getId())
       ->with('response')
         ->begin()
           ->isStatusCode(403)
         ->end()
   ->post('/delivery/deleteMany', array('selected' => array($address->getId())))
       ->with('response')
         ->begin()
           ->isStatusCode(403)
         ->end()

  
  #---------- user --------------------    
   ->get('/client/'.$client->getId().'/user')
     ->with('response')
       ->begin()
         ->isStatusCode(403)
       ->end();
