<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
  get('/mnumiQL/index')->

  with('request')->begin()->
    isParameter('module', 'mnumiQL')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;
