<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();


$browser->
  get('/settings/notification')->

  with('request')->begin()->
    isParameter('module', 'notification')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Notification/')->
  end()
;

$browser->get('/settings/notification/add')->
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Create notification/')->
    end()->
    //
    info('#try save empty form')->
    //
    click(' Save')->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/required/')->
    end()->
    //
    info('#go to new notification form again')->
    //
    click(' Save', array('notification' => array('title' => 'test_title',
        'content' => 'test_content')))->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Saved successfully/')->
    end()
;

$browser->get('/settings/notification');

$c = new sfDomCssSelector($browser->getResponseDom());
$selectorArray = $c->matchAll('.table-actions a.delete');

$nodeArray = $selectorArray->getNodes();

$browser->get('/settings/notification')->
    info('#try to delete notification')->
    //
    click($nodeArray[0])->
    //
    with('response')->
    begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    //
    followRedirect()->
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Deleted successfully/')->
    end()->
    //
    info('#client deleted')
;
