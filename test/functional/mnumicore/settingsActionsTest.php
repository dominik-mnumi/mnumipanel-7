<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/settings')->
        
  with('response')->begin()->
    isStatusCode(401)-> // Status 401: authorization request
    checkElement('body', '/Please log in/')->
  end()->

  with('request')->begin()->
    isParameter('module', 'settings')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()->
        
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
        
  get('/settings')->
        
  with('request')->begin()->
    isParameter('module', 'settings')->
    isParameter('action', 'index')->
  end()->
  
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Settings groups/')->
  end()->
  
  click('Size')->
  with('response')->begin()->
    checkElement('body', '/Size/')->
    checkElement('body', '!/Cost/')->
  end()->
        
  info('Try delete non-exist fieldset')->
  get('/settings/deleteField/8888')->
        
  with('response')->begin()->
    isStatusCode(404)->
  end()
;

$browser->get('/settings');
$c = new sfDomCssSelector($browser->getResponseDom());
  
$browser->
  info('Try delete exist fieldset')->
  click($c->matchSingle('a.delete')->getNode(), array(), array('method' => 'post'))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  followRedirect()->
    with('response')->begin()->
    checkElement('article ul[class="message success grid_12"]', true)->
    isStatusCode()->
  end()->
  
  post('/settings/create')->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Choose parent category/')->
  end()->
  
  post('/settings/create', array('fieldset' => array(
  'name' => '')))->
  with('response')->begin()->
    checkElement('body', '/Field "Name" is required/')->
  end()->

  post('/settings/create', array('fieldset' => array(
  'name' => 'test name',
  'label' => 'test label',
  'root_id' => ''
  )),
  array('_with_csrf' => true))->
  with('response')->begin()->
    checkElement('body', '!/Name is required/')->
    checkElement('body', '!/Create settingss group/')->
  end()    
;

$printer_id = PrinterTable::getInstance()->findOneByName('Default printer')->getId();

$browser->
  get('/settings')->
  
  with('request')->begin()->
    isParameter('module', 'settings')->
    isParameter('action', 'index')->
  end()->
  
  click('Size')->
  with('response')->begin()->
    checkElement('body', '/Group: Size/')->
  end()->
  
  click('Add setting')->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Default printer/')->
  end()->
  
  click(' Save', array('field_item' => array(
  'name' => 'test paper',
  'newFieldPrintsize' => array(
      'printer_id' => $printer_id,
      'width' => '12',
      'height' => '12'
  ))), array('_with_csrf' => true)
  )->
    
  followRedirect()->
    with('response')->begin()->
    checkElement('body', '/Settings groups/')->
    isStatusCode()->
  end()->
  //one more time with cost as 50,5
  
  click('Others')->
  with('response')->begin()->
    checkElement('body', '/Group: Others/')->
  end()->
  
  click('Add setting')->
  with('response')->begin()->
    isStatusCode()->
  end()->
  
  click(' Save', array('field_item' => array(
  'name' => 'test paper 3',
  'cost' => '50,5'
  )), array('_with_csrf' => true))->
  with('response')->begin()->
    checkElement('body', '!/is not a number/')->
    isStatusCode()->
  end();
  
// delete field item


$browser->
  info('Try delete non-exist field item')->
  get('/settings/deleteFieldItem/8888')->
        
  with('response')->begin()->
    isStatusCode(404)->
  end();

$browser->
  get('/settings')->
  
  with('request')->begin()->
    isParameter('module', 'settings')->
    isParameter('action', 'index')->
  end()-> 
  
  click('Size')->
  with('response')->begin()-> 
    checkElement('body', '/Group: Size/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());

$browser->
  info('Try delete exist field item')->
        
  click($c->matchSingle('.table-actions a.delete')->getNode(), array(), array('method' => 'post'))->        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  followRedirect()->
    with('response')->begin()->
    checkElement('article ul[class="message success grid_12"]', true)->
    isStatusCode()->
  end()->
  
  click('Size')->
  click('Add setting')->
  with('response')->begin()->
    checkElement('body', '/Default printer/')->
    checkElement('body', '!/Maximal price/')->
  end()->
  
  click('Material type')->
  click('Add setting')->
  with('response')->begin()->
    checkElement('body', '!/Maximal price/')->
    checkElement('body', '!/Default printer/')->
  end()->

  click('Others')->
  click('Add setting')->
  with('response')->begin()->
    checkElement('body', '/Maximal price/')->
    checkElement('body', '!/Default printer/')->
  end();

// test measure units for different forms

$browser->get('/settings')->
click('Size')->
  with('response')->begin()->
    checkElement('body', '/Size/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
  click($c->matchSingle('a.without-tip')->getNode(), array(), array('method' => 'get'))->
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '!/Measure unit/')->
  end();

$browser->get('/settings')->
click('Material type')->
  with('response')->begin()->
    checkElement('body', '/Material type/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
  click($c->matchSingle('a.without-tip')->getNode(), array(), array('method' => 'get'))->
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Cost/')->
    checkElement('body', '/Label/')->
    checkElement('body', '!/Per copy/')->
    checkElement('body', '/Per square metre/')->
    checkElement('body', '!/Linear metre/')->
  end();


$browser->get('/settings')->
click('Print type')->
  with('response')->begin()->
    checkElement('body', '/Print type/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
  click($c->matchSingle('a.without-tip')->getNode(), array(), array('method' => 'get'))->
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Per page/')->
    checkElement('body', '/Label/')->
    checkElement('body', '!/Per copy/')->
    checkElement('body', '!/Per square metre/')->
    checkElement('body', '!/Linear metre/')->
  end();

$browser->get('/settings')->
click('Others')->
  with('response')->begin()->
    checkElement('body', '/Others/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
  click($c->matchSingle('a.without-tip')->getNode(), array(), array('method' => 'get'))->
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Name/')->
  end();

$browser->get('/settings')->
  with('response')->begin()->
    checkElement('body', '/Settings groups/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
  click($c->matchSingle('a.edit')->getNode(), array(), array('method' => 'get'))->
      
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Edit field/')->
  end()->
  
  click(' Save', array('fieldset' => array(
  'label' => 'Size'))
  )->
    
  followRedirect()->
    with('response')->begin()->
    checkElement('body', '/Settings groups/')->
    isStatusCode()->
  end();
