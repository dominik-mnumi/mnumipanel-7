<?php

/* 
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  info('1. Login page')->
  get('/login')->

  with('request')->begin()->
    isParameter('module', 'sfGuardAuth')->
    isParameter('action', 'signin')->
  end()->
 
  with('response')->begin()->
    isStatusCode(401)-> // Status 401: authorization request
    checkElement('body', '/Please log in/')->
  end()->
        
  info('2. Unauthorized access')->
  
  click('Login', array('signin' => array(
    'username' => 'wronguser',
    'password' => 'wrongpassword',
  )))->
  
  with('form')->
    begin()->
    hasErrors(1)->
    isError('username', 'invalid')->
  end()->

  info('3.0.1 Invalid username lost pass')->

  click('Send', array('forgot_password' => array(
    'email_address' => 'wronguser'
  )))->

  with('form')->
    begin()->
    hasErrors(1)->
    isError('email_address', 'invalid')->
  end()->

  info('3. Login successfull')->
        
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->

  info('3.1 Logged successful')->
    
  with('response')->begin()->
    isRedirected()->
    isStatusCode(302)->
  end()->

  info('3.2 Redirecting to clear')->
  followRedirect()->

  with('response')->begin()->
    isRedirected()->
    isStatusCode(302)->
    info('3.2 Redirected successfull')->
  end()->

  info('3.3 Redirecting to main page')->
  followRedirect()->
    
  with('request')->begin()->
    isParameter('module', 'dashboard')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    info('3.4 Open Dashboard successfull')->
  end()
;

$browser->
  info('4. Logout action')->
  get('/logout')->
 
  with('request')->begin()->
    isParameter('module', 'sfGuardAuth')->
    isParameter('action', 'signout')->
  end()->

  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  followRedirect()->
    
  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '/Please log in/')->
  end()
;
        