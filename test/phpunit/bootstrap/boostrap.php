<?php

require_once __DIR__ . '/../../../config/ProjectConfiguration.class.php';
mnumiTestFunctional::getConfiguration();

require_once __DIR__ . '/../BasePhpunitTestCase.php';

// remove all cache
sfToolkit::clearDirectory(sfConfig::get('sf_app_cache_dir'));

class mnumiTestFunctional
{
    static $configuration = false;
    
    public static function getConfiguration()
    {
        if(self::$configuration == false)
        {
            self::$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'test', true);
            sfContext::createInstance(self::$configuration);
        }
        
        return self::$configuration;
    }
}
