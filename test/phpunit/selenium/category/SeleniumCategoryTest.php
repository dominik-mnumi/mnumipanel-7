<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class SeleniumCategoryTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test add category
     */
    public function testAdd()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/product");
        $this->click("id=dialog-add-category-click");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("xpath=(//div[@id='dialog-add-category-form']/form/fieldset/table/tbody/tr/td[2])[3]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->type("xpath=(//input[@id='catName'])[2]", "new test category");
        $this->click("css=button[type=\"button\"]");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("new test category");
    }

    /**
     *  Test delete category
     */
    public function testDelete()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/product");
        $this->verifyTextPresent("Catalogs");
        $this->click("//div[@id='sf_content']/article/section/div/div/ul/li[6]/span/a[3]/img");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Delete item" == $this->getText("css=div.modal-window.block-border > div.block-content > h1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("css=button[type=\"button\"]");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Catalogs"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

}

?>