<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class OrderPermissionStatusTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        
    }

    /** @TODO make for behat
     *  Tests permission order status.
     
    public function testStatus()
    {
        $this->fixture()->clean()->loadOwn('status');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Create new order");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Create new order" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("To realization" == $this->getText("xpath=(//button[@id='step_1'])[2]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("xpath=(//button[@id='step_1'])[2]");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("To bindery" == $this->getText("xpath=(//button[@id='step_1'])[2]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("xpath=(//button[@id='step_1'])[2]");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Ready" == $this->getText("xpath=(//button[@id='step_1'])[2]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("xpath=(//button[@id='step_1'])[2]");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to bindery" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to realization" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to new order" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("To realization" == $this->getText("xpath=(//button[@id='step_1'])[2]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isTextPresent("To realization"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "realization");
        $this->type("id=signin_password", "realization");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if(!$this->isTextPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "bindery");
        $this->type("id=signin_password", "bindery");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if(!$this->isTextPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "ready");
        $this->type("id=signin_password", "ready");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if(!$this->isTextPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "status");
        $this->type("id=signin_password", "status");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to bindery" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to realization" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to new order" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "new");
        $this->type("id=signin_password", "new");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Back to new order" == $this->getText("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if(!$this->isTextPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "deleted");
        $this->type("id=signin_password", "deleted");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("7.00" == $this->getValue("name=price[PRINT][price]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("id=step_1");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if(!$this->isTextPresent("id=step_1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }
    }*/

}

?>