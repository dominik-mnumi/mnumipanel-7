<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class OrderPermissionTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        
    }

    /**
     *  Tests permission order list.
     */
    public function testViewList()
    {
        $this->fixture()->clean()->loadOwn('viewList');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Flyers 20x30mm - important");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Flyers 20x30mm - important");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("you do not have proper credentials");
    }

    /**
     *  Tests permission order create.
     */
    public function testCreate()
    {
        $this->fixture()->clean()->loadOwn('create');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Clients");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->click("id=dialog-new-order-click");
        $this->click("xpath=(//form[@id='dialog-new-order-form']/div/div/div/ul/li/b)[3]");
        $this->click("xpath=(//a[contains(text(),'Flyers 1page')])[2]");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Flyers 1page");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Clients");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->click("id=dialog-new-order-click");
        $this->click("xpath=(//form[@id='dialog-new-order-form']/div/div/div/ul/li/b)[3]");
        $this->click("xpath=(//a[contains(text(),'Flyers 1page')])[2]");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Order created");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Clients");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Add order"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->open("/mnumicore_test_selenium.php/client/newOrder?clientId=.".ClientTable::getInstance()->findAll()->getFirst()->getId().".&productId=1&userId=".sfGuardUserTable::getInstance()->findAll()->getFirst()->getId());
        $this->verifyTextPresent("you do not have proper credentials");
    }

    /**
     *  Tests permission order create.
     */ 
    public function testEdit()
    {
        $this->fixture()->clean()->loadOwn('edit');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Flyers 20x30mm");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Flyers 20x30mm");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("you do not have proper credentials");
    }

    /**
     *  Tests permission order file.
     */
    public function testFile()
    {
        $this->fixture()->clean()->loadOwn('file');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertTrue($this->isElementPresent("id=pickfiles"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertTrue($this->isElementPresent("id=pickfiles"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isElementPresent("id=pickfiles"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
    
    /**
     *  Tests permission order block price.
     */
    public function testBlockPrice()
    {
        $this->fixture()->clean()->loadOwn('blockPrice');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Price block");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Price block");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Price block"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

    /**
     *  Tests permission order send calculation.
   
    public function testSendCalculation()
    {
        $this->fixture()->clean()->loadOwn('sendCalculation');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Send");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Send");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Send"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

    /**
     *  Tests permission order view changelog.
     */
    public function testViewChangelog()
    {
        $this->fixture()->clean()->loadOwn('viewChangelog');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Changelog");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Changelog");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Changelog"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

    /**
     *  Tests permission order trader.
     */
    public function testTrader()
    {
        $this->fixture()->clean()->loadOwn('trader');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Designer/Seller");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Designer/Seller");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Designer/Seller"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

    /**
     *  Tests permission order reminder.
     */
    public function testReminder()
    {
        $this->fixture()->clean()->loadOwn('reminder');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Add reminder");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Add reminder");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Add reminder"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

    /**
     *  Tests permission order duplicate.
     */
    public function testDuplicate()
    {
        $this->fixture()->clean()->loadOwn('duplicate');

        $this->open("/mnumicore_test_selenium.php/");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->click("link=Company");
        $this->click("link=Orders");
        $this->waitForPageToLoad("30000");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Duplicate order");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin2");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Duplicate order");
        $this->click("link=Logout");
        $this->waitForPageToLoad("30000");
        $this->type("id=signin_username", "admin3");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("Duplicate order"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
}

?>