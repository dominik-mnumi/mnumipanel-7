<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class PackageTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test list action
     */
    public function testList()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/package");
        $this->verifyTextPresent("Search");
        $this->verifyTextPresent("Packages");
        $this->verifyTextPresent("Some street 1/10");
    }

    /**
     * Server shows ERROR.
     * 
     *  Test show + update action
    
    public function testShowAndUpdate()
    {
        $orderPackageObj = OrderPackageTable::getInstance()->findAll()->getFirst();

        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/client/".$orderPackageObj->getClient()->getId()."/packages/".$orderPackageObj->getId());
        $this->verifyTextPresent("Delivery address Some street 1/10, London, 66-100");
        $this->click("id=dialog-edit-package-click");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("css=div.block-footer.align-right > button[type=\"button\"]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->select("xpath=(//select[@id='EditPackageForm_carrier_id'])[2]",
                "label=Polish post - Priority");
        $this->type("xpath=(//textarea[@id='EditPackageForm_description'])[2]",
                "test info");
        $this->click("css=div.block-footer.align-right > button[type=\"button\"]");
        $this->open("/mnumicore_test_selenium.php/client/".$orderPackageObj->getClient()->getId()."/packages/".$orderPackageObj->getId());
        $this->verifyTextPresent("Delivery address Polish post - Priority, first street, New York, 46-222");
    } */

    /**
     *  Test shelf list action
     */
    public function testShelfList()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/shelf");
        $this->verifyTextPresent("Shelf number: 1");
        $this->verifyTextPresent("Flyers 20x30mm - important");
    }

    /**
     *  Test to book action
     
    public function testToBook()
    {
        $orderPackageObj = OrderPackageTable::getInstance()->findAll()->getFirst();

        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/client/".$orderPackageObj->getClient()->getId()."/packages/".$orderPackageObj->getId());
        $this->click("link=Package to invoicing");
        $this->waitForPageToLoad("30000");
        $this->assertFalse($this->isTextPresent("Package to invoicing"));
    }*/

    /**
     *  Test index action
     */
    public function testIndex()
    {
        $orderPackageObj = OrderPackageTable::getInstance()->findAll()->getFirst();

        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/client/".$orderPackageObj->getClient()->getId()."/packages");
        $this->verifyTextPresent("Delivery address: Some street 1/10, London, 66-100");
        $this->verifyTextPresent("Flyers 20x30mm - important");
        $this->verifyTextPresent("Photo Album Large - 30x26 cm");
    }

}

?>