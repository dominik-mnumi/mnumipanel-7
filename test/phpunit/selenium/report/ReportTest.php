<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class ReportTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test carrier action
     */
    public function testCarrier()
    {
        $orderPackageObj = OrderPackageTable::getInstance()->findAll()->getFirst();

        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/settings/report/carrier");
        try
        {
            $this->assertTrue($this->isTextPresent("Delivery"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
    
    /**
     *  Test ViewCarrier action
     */
    public function testViewCarrier()
    {
        $orderPackageObj = OrderPackageTable::getInstance()->findAll()->getFirst();
      
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/settings/report/carrier/view?id=1");
        try
        {
            $this->assertTrue($this->isTextPresent("Imie i nazwisko"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
}

?>