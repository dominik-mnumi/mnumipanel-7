<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class CalculationFactorReadTest extends BasePhpunitTestCase
{
    /**
     * Test test countFactor() static method.
     */
    public function testCountFactor()
    {
        //simple 1-to-1 check
        $array = calculationFactor::count(210, 297, 210, 297, 1);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(210, 297, 210, 297, 2);
        $this->assertEquals(2, $array['factor']);
        
        $array = calculationFactor::count(210, 297, 210, 297, 3);
        $this->assertEquals(3, $array['factor']);

        //for printer size 320 x 450
        $array = calculationFactor::count(320, 450, 297, 210, 1);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(320, 450, 297, 210, 2);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(320, 450, 297, 210, 3);
        $this->assertEquals(2, $array['factor']);

        $array = calculationFactor::count(320, 450, 297, 210, 4);
        $this->assertEquals(2, $array['factor']);

        //for printer size 320 x 450 and small business cards 90 x 50
        $array = calculationFactor::count(320, 450, 90, 50, 1);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(320, 450, 90, 50, 10);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(320, 450, 90, 50, 30);
        $this->assertEquals(1, $array['factor']);

        $array = calculationFactor::count(320, 450, 90, 50, 31);
        $this->assertEquals(2, $array['factor']);

        //for large size
        $array = calculationFactor::count(320, 450, 841, 1189, 1);
        $this->assertEquals(8, $array['factor']);

        $array = calculationFactor::count(320, 450, 841, 1189, 2);
        $this->assertEquals(16, $array['factor']);
    }    
}