<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');

class CastReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        
    }
    
    /**
     * Tests multiToOneDimension() method.
     */
    public function testMultiToOneDimension()
    {
        // 1. Simple string
        $arr = 'test';
        $resultArr = Cast::multiToOneDimension($arr);
        
        $this->assertEquals(array('test'), $resultArr);
        
        // 2. One dimension
        $arr = array('test', 'test2', 'test3');
        $resultArr = Cast::multiToOneDimension($arr);
        
        $this->assertEquals(array('test', 'test2', 'test3'), $resultArr);
        
        // 3. Two dimensions
        $arr = array('test1' => array('test11', 'test12'), 
                     'test2' => array('test21', 'test22'), 
                     'test3' => array('test31', 'test32'));
        $resultArr = Cast::multiToOneDimension($arr);
        
        $this->assertEquals(array('test11', 'test12', 
                                  'test21', 'test22',
                                  'test31', 'test32'), 
                            $resultArr);
        
        // 4. Three dimensions
        $arr = array('test1' => array('test11' => array('test111', 'test112'), 
                                      'test12' => array('test121', 'test122')), 
                     'test2' => array('test21' => array('test211', 'test212'), 
                                      'test22' => array('test221', 'test222')), 
                     'test3' => array('test31' => array('test311', 'test312'), 
                                      'test32' => array('test321', 'test322')));
        $resultArr = Cast::multiToOneDimension($arr);
        
        $this->assertEquals(array('test111', 'test112', 
                                  'test121', 'test122',
                                  'test211', 'test212',
                                  'test221', 'test222',
                                  'test311', 'test312',
                                  'test321', 'test322'), 
                            $resultArr);
    }
    
}