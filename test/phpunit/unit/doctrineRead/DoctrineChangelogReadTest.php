<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class ChangelogReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    public function testChangelog()
    {
        // for invoice
        $invoice = InvoiceTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        
        $diffs = $invoice->getDifferences();
        $changeTypes = array('added', 'edited', 'deleted');
        
        $this->assertTrue(is_array($diffs));
        foreach($diffs as $diff)
        {
            $this->assertEquals($diff->getEditor()->getUsername(), 'admin');
            $this->assertTrue(is_int($diff->getEditedAt()));
            $this->assertTrue(in_array($diff->getChangeType(), $changeTypes));
            
            $all = $diff->fetchAll();
            $this->assertTrue(is_array($all));
            
            foreach($all as $change)
            {
                $this->assertTrue(array_key_exists('name', $change));
            }
        }
        
        
        //for order
        $order = OrderTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        
        $diffs = $order->getDifferences();
        
        $this->assertTrue(is_array($diffs));

	    $diff = $diffs[0];

        $this->assertEquals($diff->getEditor()->getUsername(), 'admin');
        $this->assertTrue(is_int($diff->getEditedAt()));
        $this->assertTrue(in_array($diff->getChangeType(), $changeTypes));
          
        $all = $diff->fetchAll();
        $this->assertTrue(is_array($all));
          
        foreach($all as $change)
        {
            $this->assertTrue(array_key_exists('name', $change));
        }
    }
}
