<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');

class RestServerAdapterReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * initalize protected varibles
     */
    protected $object;
    protected $webapi_key;
    protected $product_name;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->object = new RestServerAdapter();
        $this->webapi_key = 'LDAvTuD7i53Ef5yXEOabAvt9';
        $this->product_name = 'ulotki-dwustronne';
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Test testDoCategory() method.
     */
    public function testDoCategory()
    {
        $rests = array();
        $rests[1] = $this->object->doCategory($this->webapi_key);

        // fetch by id
        $category = CategoryTable::getInstance()->findOneBySlug('boolkets');
        $rests[2] = $this->object->doCategory($this->webapi_key, $category->getNode()->getParent()->id);

        // fetch by name
        $rests[3] = $this->object->doCategory($this->webapi_key, 'boolkets');

        foreach($rests as $rest)
        {
            $this->assertArrayHasKey('info', $rest);

            // get category information
            $info = $rest['info'];
            $this->assertNotNull('id', $rest['info']);
            $this->assertNotNull('name', $rest['info']);
            $this->assertNotNull('slug', $rest['info']);

            $this->assertArrayHasKey('results', $rest);

            $this->assertGreaterThan(1, count($rest['results']));

            foreach($rest['results'] as $obj)
            {
                $this->assertArrayHasKey('id', $obj);
                $this->assertArrayHasKey('parent_id', $obj);
                $this->assertArrayHasKey('name', $obj);
                $this->assertArrayHasKey('link', $obj);

                $this->assertNotNull($obj['name']);
            }
        }
    }
    
    public function testDoCarriers()
    {
    	$rest = $this->object->doCarriers($this->webapi_key);
    	
    	$this->isTrue(count($rest) > 0);
    	
    	$keys = array(
    		'id',
    		'name',
    	    'label',
    		'delivery_time',
    		'active',
    		'apply_on_all_pricelist',
    		'tax',
    		'restrict_for_cities',
    		'require_shipment_data',
    		'cost',
    		'price',
    		'free_shipping_amount',
    		'carrier_report_type_name'
    	);
    	
    	foreach($rest['results'] as $carrier)
    	{
    		$this->assertEquals($keys, array_keys($carrier));
    	}
    }
    
    public function testDoGetPayments()
    {
    	$rest = $this->object->doPayments($this->webapi_key);
    	 
    	$this->isTrue(count($rest) > 0);
    	 
    	$keys = array(
    			'id',
    			'name',
    			'label',
    			'description',
    			'active',
    			'for_office',
    			'apply_on_all_pricelist',
    			'changeable',
    			'apply_on_all_carriers',
    			'required_credit_limit',
    			'cost',
    			'free_payment_amount',
    			'income_pattern',
    	    	'outcome_pattern',
    			'deposit_enabled'
    	);
    	 
    	foreach($rest['results'] as $carrier)
    	{
    		$this->assertEquals($keys, array_keys($carrier));
    	}
    }
    
    public function testDoGetOrderFormField()
    {
    	$rest = $this->object->doGetOrderFormField($this->webapi_key, 'product-1');
    	
    	$this->assertArrayHasKey('fields', $rest);
    	$this->assertArrayHasKey('product', $rest);
    	$this->assertArrayHasKey('wizards', $rest);
    	
    	$this->isTrue(count($rest['fields']) > 3);
    	
    	foreach($rest['fields'] as $field)
    	{
    		$this->assertArrayHasKey('field_id', $field);
    		$this->assertArrayHasKey('field_title', $field);
    		$this->assertArrayHasKey('field_type', $field);
    		$this->assertArrayHasKey('field_name', $field);
    	}
    }

    /**
     * Test doProductList method 
     * who get list of products by category name
     */
    public function testDoProductList()
    {
        $rest = $this->object->doProductList($this->webapi_key, 'bussiness-cards');
        $this->assertArrayHasKey('results', $rest);
        $this->assertGreaterThan(1, count($rest['results']));
        foreach($rest['results'] as $obj)
        {
            $this->assertArrayHasKey('slug', $obj);
            $this->assertArrayHasKey('name', $obj);

            $this->assertNotNull($obj['slug']);
            $this->assertNotNull($obj['name']);
        }
    }
    /* @TODO Test when testDoCheckNewOrder will use only new core
    public function testDoCheckNewOrder()
    {
        $productSlug = 'product-1';

        $fieldArray['COUNT'] =
            array
                (
                'name' => 'COUNT',
                'value' => 1
        );

        $fieldArray['QUANTITY'] =
            array
                (
                'name' => 'QUANTITY',
                'value' => 10
        );

        $fieldArray['SIZE'] =
            array
                (
                'name' => 'SIZE',
                'value' => FieldItemTable::getInstance()->findOneByName('A4')->getId()
        );

        $fieldArray['MATERIAL'] =
            array
                (
                'name' => 'MATERIAL',
                'value' => FieldItemTable::getInstance()->findOneByName('Paper X')->getId()
        );

        $fieldArray['PRINT'] =
            array
                (
                'name' => 'PRINT',
                'value' => FieldItemTable::getInstance()->findOneByName('Print X')->getId()
        );

        $fieldArray['SIDES'] =
            array
                (
                'name' => 'SIDES',
                'value' => FieldItemTable::getInstance()->findOneByName('Single')->getId()
        );

        $rest = $this->object->doCheckNewOrder($this->webapi_key, null, $productSlug, $fieldArray);

        $this->assertArrayHasKey('result', $rest);
        $this->assertGreaterThan(1, count($rest['result']));
        $this->assertArrayHasKey('summaryPriceNet', $rest['result']);
    }
    */

    /* @TODO Test when doGetPaymentFiltered will use only new core
    public function testDoGetPaymentFiltered()
    {
        $rest = $this->object->doGetPaymentFiltered($this->webapi_key, 'non-exist-session', 1);

        $this->assertEquals('test 1', $rest['results'][0]['name']);
    }
    */
    
    public function testDoGetCarrier()
    {
        $rest = $this->object->doGetCarrier($this->webapi_key, CarrierTable::getInstance()->findOneByName('Some carrier')->getId());
        $this->assertEquals('Some carrier', $rest['results']['name']);
    }
    

}
