<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class CashDeskReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->cashDesk = CashDeskTable::getInstance()->findOneByDescription('Payment for last order');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Checks if after block delete balance is good.
     */
    public function testUpdateBalanceAfterTakeDownBlock()
    {
        $balanceBeforeTakeDownBlock = CashDeskTable::getInstance()->findOneByDescription('something')->getBalance();
        $this->assertEquals(300 , $balanceBeforeTakeDownBlock);
        
        CashDeskTable::getInstance()->findOneByDescription('Block')->delete();  
        $balanceAfterTakeDownBlock = CashDeskTable::getInstance()->findOneByDescription('something')->getBalance();   
        $this->assertEquals(350 , $balanceAfterTakeDownBlock);
    }
    
    /**
     * Check formated number for cash desk
     */
    public function testGetFormattedNumber()
    {
        $this->assertEquals($this->cashDesk->getFormattedNumber(), 'IN 1'.'/'.date('Y', strtotime($this->cashDesk->getCreatedAt())));
    }
}