<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class FieldsetTableReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests Fieldset tabs.
     */
    public function testGeneral()
    {
         //check default tabs
        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('GENERAL');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('COUNT');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('SIZE');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('MATERIAL');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('PRINT');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('SIDES');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('FIXEDPRICE');
        $this->assertTrue(is_object($fieldsetObj));

        $fieldsetObj = FieldsetTable::getInstance()->findOneByName('OTHER');
        $this->assertTrue(is_object($fieldsetObj));
    }

    /**
     * Tests mergeOrderDefaultFieldsetArrays() method. FieldsetTable.class.php.
     */
    public function testMergeOrderDefaultFieldsetArrays()
    {
        $slug = 'product-1';
        $productObj = Doctrine::getTable('Product')->findOneBySlug($slug);

        //gets default fieldsets array
        $defaultFields = $productObj->getProductFieldsetsForPriceDefaultArray();

        //1. user custom order - some values
        $fields['COUNT'] = array('name' => 'COUNT', 'value' => 10);
        $fields['QUANTITY'] = array('name' => 'QUANTITY', 'value' => 1);
        $fields['MATERIAL'] = array('name' => 'MATERIAL', 'value' => FieldItemTable::getInstance()->findOneByName('Paper X')->getId());
        $fields['SIZE'] = array('name' => 'SIZE', 'value' => FieldItemTable::getInstance()->findOneByName('A4')->getId());
        $fields['PRINT'] = array('name' => 'PRINT', 'value' => FieldItemTable::getInstance()->findOneByName('Print X')->getId());
        $fields['SIDES'] = array('name' => 'SIDES', 'value' => FieldItemTable::getInstance()->findOneByName('Single')->getId());

        //new fields, check if properly overwrite
        $newFields = FieldsetTable::mergeOrderDefaultFieldsetArrays($defaultFields, $fields);
        $this->assertGreaterThanOrEqual('3', count($newFields));

        //2. user custom order - no values
        $fields = array();

        //new fields, check if properly overwrite
        $newFields = FieldsetTable::mergeOrderDefaultFieldsetArrays($defaultFields, $fields);
        $this->assertGreaterThanOrEqual('0', count($newFields));
    }

}