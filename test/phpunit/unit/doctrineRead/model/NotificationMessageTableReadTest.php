<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class NotificationMessageTableReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->loadCommon('messages'));
    }

    /**
     * Tests filtered payment.
     */
    public function testGetNotificationsToSend()
    {
        // get notifications to send
        $resultArray = NotificationMessageTable::getInstance()->getNotificationsToSend(10);
        
        $this->assertEquals('mnumi@test.com', $resultArray[0]['sender']);
        
        $this->assertEquals('123123123', $resultArray[1]['sender']);
    }

}