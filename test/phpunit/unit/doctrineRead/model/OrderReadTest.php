<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class OrderReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Order instance
     * @var Order
     */
    private $order;

    protected function _start()
    {
    	$this->package = OrderPackageTable::getInstance()->createQuery('m')->limit(1)->fetchOne();

        if(!$this->package instanceof OrderPackage)
        {
            throw new Exception('Sample OrderPackage does not exist. Check fixtures. ');
        }
        $this->order = OrderTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        $this->orderToDuplicate = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Order to duplicate'")->limit(1)->fetchOne();
        //import file here to get real file on server
        $this->orderToDuplicate->importFile(sfConfig::get('sf_test_dir').'/phpunit/data/sample_image.jpg');
        $this->emptyOrder = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Empty order'")->limit(1)->fetchOne();
        $this->orderDraft = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Order draft'")->limit(1)->fetchOne();

        if(!$this->order instanceof Order)
        {
            throw new Exception('Sample Order does not exist. Check fixtures. ');
        }

        $this->file = FileTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        if(!$this->file instanceof File)
        {
            throw new Exception('Sample File does not exist. Check fixtures. ');
        }
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    public function testGetFileById()
    {
        $file = $this->order->findOneFileById($this->file->getId());
        $this->assertInstanceOf('File', $file);
    }

    public function testCanEditByUser()
    {
        // check permission for administrator
        $user = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('admin');
        if(!$user instanceof sfGuardUser)
        {
            throw new Exception('User "UserAdmin" does not exists.');
        }
        $this->assertTrue($this->order->canEditByUser($user));

        // check permission for user who is editor
        $user = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('office1');
        if(!$user instanceof sfGuardUser)
        {
            throw new Exception('User with Restricted client permission does not exists.');
        }
        $this->assertTrue($this->order->canEditByUser($user));


        // check permission for user who is editor
        $user = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('olduser');
        if(!$user instanceof sfGuardUser)
        {
            throw new Exception('User with office no client permission does not exists.');
        }
        $this->assertFalse($this->order->canEditByUser($user));
    }

    /**
     * Tests getNextWorkflows method.
     */
    public function testGetNextWorkflows()
    {
        $workFlows = $this->order->getNextWorkflows();
        $this->assertEquals(2, count($workFlows));
    }

    /**
     * Tests getPhotoWithPath method.
     */
    public function testGetPhotoWithPath()
    {
        $this->assertTrue((boolean)preg_match('|(.*)/file/get/main/1|',
                        $this->order->getPhotoWithPath()));
    }

    /**
     * Tests getFilePath method.
     */
    public function testGetFilePath()
    {
        $this->assertTrue((boolean)preg_match('|(.*)/data/files/'.date('Y/m/d',
                                strtotime($this->order->getCreatedAt())).'/'.$this->order->getId().'/|',
                        $this->order->getFilePath()));
    }

    /**
     * Tests isAffectedByCoupon method
     */
    public function testIsAffectedByCoupon()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        $coupon2 = CouponTable::getInstance()->find('BonusCoupon2');
        $coupon3 = CouponTable::getInstance()->find('CouponInactive');
        
        $this->assertTrue($coupon instanceof Coupon);
        $this->assertTrue($coupon2 instanceof Coupon);
        $this->assertTrue($coupon3 instanceof Coupon);
        
        // afected by table coupon_product
        $this->assertTrue($this->order->isAffectedByCoupon($coupon));
      
        // affected by apply for all products
        $this->assertTrue($this->order->isAffectedByCoupon($coupon2));
      
        // not affected (products)
        $this->assertFalse($this->order->isAffectedByCoupon($coupon3));
    }

    /**
     * Test getTotalAmountGross method
     */
    public function testGetTotalAmountGross()
    {
        $this->assertEquals($this->order->getTotalAmountGross(), 123);
    }
    
    /**
     * Test getBaseAmount method
     */
    public function testGetBaseAmount()
    {
        $this->assertEquals($this->order->getBaseAmount(), 90);
    }
    
    /**
     * Test testGetTotalAmount method
     */
    public function testGetTotalAmount()
    {
        $this->assertEquals($this->order->testGetTotalAmount(), 110.7);
    }
    
    /**
     * Test getDiscountAmount method.
     */
    public function testGetDiscountAmount()
    {
        $this->assertEquals($this->order->getDiscountAmount(), 10);
    }

}
