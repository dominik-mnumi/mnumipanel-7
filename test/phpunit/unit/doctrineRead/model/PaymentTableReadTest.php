<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class PaymentTableReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadOwn('default'));
    }

    /**
     * Tests filtered payment.
     */
    public function testFilteredPayment()
    {
        //gets all payment
        $resultArray = PaymentTable::getInstance()->getFilteredPayment();
        $this->assertEquals('test 1', $resultArray[0]['name']);

        //gets payment with pricelist OR apply_on_all_pricelist AND (forOffice = 0 OR forOffice = 1) AND (creditLimit = 0 OR creditLimit = 1)
        $resultArray = PaymentTable::getInstance()->getFilteredPayment(PricelistTable::getInstance()->getDefaultPricelist()->getId(), 1, 1);
        $this->assertEquals(5, count($resultArray));
        $this->assertEquals('test 1', $resultArray[0]['name']);

        //gets payment forOffice = 0 OR forOffice = 1
        $resultArray = PaymentTable::getInstance()->getFilteredPayment(null, 1);
        $this->assertEquals(12, count($resultArray));

        //gets payment from pricelist and apply_on_all_pricelist 
        $resultArray = PaymentTable::getInstance()->getFilteredPayment(PricelistTable::getInstance()->getDefaultPricelist()->getId());
        $this->assertEquals(5, count($resultArray));
        $this->assertEquals('test 1', $resultArray[0]['name']);
    }
    
    /**
     * Tests payment - delivery relation.
     */
    public function testDoCheckPayment()
    {
        $payment = Doctrine::getTable('Payment')->findOneByName('test 1');
        if(!$payment instanceof Payment)
        {
            throw new Exception('Payment "test 1" does not exists');
        }
        
        $carrier = Doctrine::getTable('Carrier')->findOneByName('Some carrier');
        if(!$carrier instanceof Carrier)
        {
            throw new Exception('Carrier "Some carrier" does not exists');
        }
        $this->assertEquals(false, $payment->isRelatedWithCarrier($carrier->getId()));
        
        $payment = Doctrine::getTable('Payment')->findOneByName('cash payment');
        if(!$payment instanceof Payment)
        {
            throw new Exception('Payment "cash" does not exists');
        }
        $this->assertEquals(false, $payment->isRelatedWithCarrier(0));
    }

}
