<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class ProductFieldReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests testGetProductFieldItemsAsArray() method. ProductField.class.php.
     */
    public function testGetProductFieldItemsAsArray()
    { 
        $productObj = ProductTable::getInstance()->findOneByName('product 1');
        $fieldsetQuantityObj = FieldsetTable::getInstance()->findOneByName('QUANTITY');

        $productFieldObj = ProductFieldTable::getInstance()->findOneByProductIdAndFieldsetId($productObj->getId(), $fieldsetQuantityObj->getId());

        $fixPrices = $productFieldObj->getProductFieldItemsAsArray();
        
        $this->assertEquals(5, count($fixPrices));

    }

}
