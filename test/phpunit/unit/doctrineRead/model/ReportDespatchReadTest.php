<?php
require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class ReportDespatchReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadOwn('report'));
    }

    public function testGetOrderedDespatchesByReportName()
    {
        $reportDespatch = ReportDespatchTable::getInstance()->getOrderedDespatchesByReportName('pl-poczta-polska')->getFirst();
        
        $this->assertEquals($reportDespatch->getNumber() , 1);
    }
}