<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class sfGuardUserReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * @var sfGuardUser
     */
    private $object;
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->object = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('admin');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests checkPassword() method.
     */
    public function testCheckPassword()
    {
        // new password format
        $this->assertTrue($this->object->checkPassword('admin'));
        $this->assertFalse($this->object->checkPassword('wrong-password'));

        // old-style password format (OLD_CORE)
        $user = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('olduser');
        $this->assertTrue($user->checkPassword('qwerty'));
        $this->assertFalse($user->checkPassword('wrong-password'));
    }

    /**
     * Tests getPoints() method.
     */
    public function testGetPoints()
    {
        $points = $this->object->getPoints();
        $types = LoyaltyPointTable::$types;

        $this->assertInstanceOf('Doctrine_Collection', $points);
        $this->assertContains('new', $types);
        $this->assertContains('accepted', $types);
        $this->assertContains('used', $types);
        $this->assertContains('lost', $types);

        foreach($points as $point)
        {
            $this->assertInstanceOf('LoyaltyPoint', $point);

            $this->assertContains($point->getStatus(), $types);
            $this->assertGreaterThan(0, $point->getPoints());
        }
    }

    /**
     * Test getAvailablePointsCount.
     */
    public function testGetAvailablePointsCount()
    {
        $this->assertEquals(450, $this->object->getAvailablePointsCount());
    }

    /**
     * Test getPointsCount.
     */
    public function testGetPointsCount()
    {
        $this->assertEquals(1200, $this->object->getPointsCount(null));
        $this->assertEquals(300, $this->object->getPointsCount('new'));
        $this->assertEquals(550, $this->object->getPointsCount('accepted'));
        $this->assertEquals(100, $this->object->getPointsCount('used'));
        $this->assertEquals(250, $this->object->getPointsCount('lost'));
    }

    /**
     * Test getUserContactsByType.
     */
    public function testGetUserContactsByType()
    {
        $contacts = $this->object->getUserContactsByType('SMS');
        $this->assertEquals(1, count($contacts));

        $contacts = $this->object->getUserContactsByType('E-mail');
        $this->assertEquals(1, count($contacts));
    }

    /**
     * Test getUserContactsByType.
     */
    public function testGetDefaultContactByType()
    {
        $contact = $this->object->getDefaultContactByType('SMS');
        $this->assertEquals('510222132', $contact->getValue());
    }

    /**
     * Test getUserContactsByType.
     */
    public function testHasNotificationByType()
    {
        $this->assertEquals(true, $this->object->hasNotificationByType('SMS'));
        $this->assertEquals(true, $this->object->hasNotificationByType('E-mail'));
    }

}