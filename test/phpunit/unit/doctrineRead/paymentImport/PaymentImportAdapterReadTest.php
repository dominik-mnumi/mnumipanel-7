<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class PaymentImportAdapterReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->filenameWIN1250 = __DIR__.'/files/transactionsWIN1250.csv';
        $this->filenameUTF8 = __DIR__.'/files/transactionsUTF8.csv';
    }
    
    /**
     * Tests detection of encoding.
     */
    public function testDetectEncoding()
    {
        // checks encoding
        // WINDOWS-1250  
        $inputEncoding = PaymentImportAdapter::detectEncoding($this->filenameWIN1250);
        $this->assertEquals('WINDOWS-1250' , $inputEncoding);

        // UTF-8
        $inputEncoding = PaymentImportAdapter::detectEncoding($this->filenameUTF8);       
        $this->assertEquals('UTF-8' , $inputEncoding);
    }    
}