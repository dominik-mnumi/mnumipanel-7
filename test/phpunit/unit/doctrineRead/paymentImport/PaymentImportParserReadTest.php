<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class PaymentImportParserReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->fixture()->clean()->loadSnapshot('common');
        
        $filename = __DIR__.'/files/transactions.csv';
        $paymentImportAdapterCsvObj = new PaymentImportAdapterCsv($filename);

        // view objects
        $this->coll = $paymentImportAdapterCsvObj->getRowColl();
    }
    
    /**
     * Checks created collection of rows.
     */
    public function testGetRowColl()
    {  
        $this->assertEquals(true, is_array($this->coll));        
        $this->assertGreaterThanOrEqual(3, count($this->coll));
    }
    
    /**
     * Tests invoice parsing.
     */
    public function testParseInvoices()
    {
        $this->assertEquals('INVOICE/11/2012/4' , $this->coll[0]->getMatchedInvoices()->getFirst());
    }

    /**
     * Tests package parsing.

    public function testParsePackages()
    {
        $this->assertEquals('INVOICE/'.date('m/Y').'/4' , $this->coll[1]->getMatchedInvoices()->getFirst());
    }

    /**
     * Tests order parsing.

    public function testParseOrders()
    {
        $this->assertEquals('INVOICE/'.date('m/Y').'/4' , $this->coll[2]->getMatchedInvoices()->getFirst());
    } */
}