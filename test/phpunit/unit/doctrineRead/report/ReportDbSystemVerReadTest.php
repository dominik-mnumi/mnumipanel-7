<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class unit_ReportDbSystemVerReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        // ignore problems in SQLLite
        try
        {
            $this->report = new ReportDbSystemVer();     
        }
        catch (Doctrine_Connection_Sqlite_Exception $e)
        {
            $this->report = false;
        }
    }

    /**
     * Test getValue();
     */
    public function testGetValue()
    {
        if(!$this->report)
        {
            return;
        }
        $this->assertNotEmpty($this->report->getValue());
    }

    /**
     * Test checkStatus();
     */
    public function testCheckStatus()
    {
        if(!$this->report)
        {
            return;
        }
        $this->assertTrue(in_array($this->report->checkStatus(), Report::$statusArray));

        $this->assertEquals('red', $this->report->checkStatus('1.0'));
        $this->assertEquals('yellow', $this->report->checkStatus('5.1'));
        $this->assertEquals('green', $this->report->checkStatus('5.3'));
    }

    
    
}