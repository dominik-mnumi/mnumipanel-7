<?php

class SahiSnapshotFactory
{
	public static function prepareCommonFixtures($fixtures)
	{
		return $fixtures->clean()
			->loadCommon('dictionaries')
			->loadCommon('notification')
			->loadCommon('settings_and_product')
			->loadCommon('coupon')
			->loadCommon('carrier')
			->loadCommon('payment')
			->loadCommon('sfGuard')
			->loadCommon('shop')
			->loadCommon('invoiceStatus')
			->loadCommon('order_package')
			->loadCommon('order')
			->loadCommon('invoice')
			->loadCommon('cashDesk');
	}
}