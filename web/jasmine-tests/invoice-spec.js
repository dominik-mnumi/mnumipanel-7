describe("InvoiceItem", function() {
  
  beforeEach(function() {
    this.invoice_item = new window.InvoiceItem;
  });
  
  it("should have default amount 100", function() {
    expect(this.invoice_item.get('amount')).toBe(100.00)
  });
  
  it("should have default quantity of 1", function() {
    expect(this.invoice_item.get('quantity')).toBe(1)
  });
  
  it("should calculate price correctly without tax", function() {
    this.invoice_item.set({quantity: 15, amount: 129.99, tax: 0})
    expect(this.invoice_item.getTotalPrice()).toBe(1949.85)
  });
  
  it("should calculate price correctly with tax", function() {
     this.invoice_item.set({quantity: 15, amount: 129.99, tax: 0.23})
     expect(this.invoice_item.getTotalPrice()).toBe(2398.32)
   });
});

describe("InvoiceItemView", function() {
  
  beforeEach(function() {
    this.view = new window.InvoiceItemView({model: new InvoiceItem});
  });
  
  
  it("should create a list element", function() {
    expect(this.view.el.nodeName).toEqual("TR");
  });
  
  it("should have 'invoice_items' class", function(){
    expect($(this.view.el).hasClass('invoice_items')).toBeTruthy();
  });
  
  it("should render with remove button", function(){
    expect($(this.view.render().el).find('button').hasClass('remove-line-item')).toBeTruthy();
  });
  
  it("should remove element when clicked on remove button", function(){    
    $('.line-items').append(this.view.render().el);
    expect($('.line-items').children().length).toEqual(1);
    $(this.view.el).find('button').trigger('click');
    expect($('.line-items').children().length).toEqual(0);
  });
    
  
});

describe("InvoiceFormView", function() {
  
  beforeEach(function() {
    this.view = new window.InvoiceFormView({model: new Invoice})
  });
  
  it("should have a new row button", function(){
    expect($(this.view.render().el).find('button').hasClass('new-line-item')).toBeTruthy();
  });
  
  it("should append new row on button click", function(){    
    this.view.render()
    $(this.view.el).find('button.new-line-item').trigger('click');
    expect($('.line-items').children().length).toEqual(1);
    $(this.view.el).find('button.new-line-item').trigger('click');
    expect($('.line-items').children().length).toEqual(2);  
    expect(this.view.model.getInvoiceItems().length).toEqual(2);  
    
  });
  
});