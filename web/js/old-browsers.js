/**
 * Older browsers detection.
 */
jQuery(document).ready(function($) 
{
    // Change these values to fit your needs
    if (($.browser.msie) ||                                                             // if IE
        ($.browser.mozilla && parseFloat($.browser.version) < 15) ||                    // if lower then FF15
        ($.browser.opera && parseFloat($.browser.version) < 9) ||			// Opera 8 and lower
        ($.browser.webkit && parseInt($.browser.version) < 400)				// Older Chrome and Safari
        ) 
    {
        // If coming back from the old browsers page
        if(window.location.search.indexOf('forceAccess=yes') > -1)
        {
            // Mark for future tests
            setCookie('forceAccess', 'yes');
        } 

        // If no cookie has been set
        if(getCookie('forceAccess') != 'yes'
               && document.location.pathname.substring(-11) != '/oldBrowser')
        {    
            document.location.href = '/oldBrowser';
        }
    }
	
    /**
     * Get cookie params
     * @return object an object with every params in the cookie
     */
    function getCookieParams()
    { 
        var parts = document.cookie.split(/; */g);
        var params = {};
		
        for(var i = 0; i < parts.length; ++i)
        {
            var part = parts[i];
            if(part)
            {
                var equal = part.indexOf('=');
                if(equal > -1)
                {
                    var param = part.substr(0, equal);
                    var value = unescape(part.substring(equal+1));
                    params[param] = value;
                }
            }
        }
	
        return params;
    }
	
    /**
     * Get a cookie value
     * @param string name the cookie name
     * @return string the value, or null if not defined
     */
    function getCookie(name)
    {
        var params = getCookieParams();
        return params[name] || null;
    }
	
    /**
     * Write a cookie value.
     * 
     * @param string name the cookie name
     * @param string value the value
     * @param int days number of days for cookie life
     * @param string domain
     * @param string secure
     * @return void
     */
    function setCookie (name, value, days, path, domain, secure)
    {
        var cookieString = name + "=" + escape(value);

        if(days)
        {
            var expires = new Date();
            expires.setTime(expires.getTime() + (days*24*60*60*1000));
            cookieString += "; expires=" + expires.toGMTString();
        }

        if(path)
        {
            cookieString += "; path=" + escape(path);
        }

        if(domain)
        {
            cookieString += "; domain=" + escape(domain);
        }

        if( secure)
        {
            cookieString += "; secure";
        }

        document.cookie = cookieString;
    }

    $("#skip a").click(function(e)
    {
        e.preventDefault();
        
        setCookie('forceAccess', 'yes');

        window.location.href = $(this).attr('href');
    });

});