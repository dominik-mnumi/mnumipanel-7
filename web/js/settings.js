function addPriceRangeView(type, label, pricelistId)
{   
    priceRangeViewColl[pricelistId][type] = new PriceRangeView({ 
        pricelistId: pricelistId, 
        name: type, 
        label: label, 
        collection: new PriceRangeCollection(priceRangeViewValueColl[pricelistId][type])});             
}

jQuery(document).ready(function($)
{  
    window.priceRangeViewColl = new Array();
    window.priceRangeViewValueColl = new Array();
    
    Backbone.sync = function(method, model, options)
    { 
    // do nothing 
    };

    window.PriceRangeRow = Backbone.Model.extend({
        idAttribute: "quantity"
    });

    window.PriceRangeCollection = Backbone.Collection.extend({
        model: PriceRangeRow,

        // sorting by ID
        comparator: function(row) 
        {
            return parseFloat(row.id);
        }
    });

    window.PriceRangeRowView = Backbone.View.extend({
        template: _.template($('#price-item').html()),
        tagName: 'tr',

        events: 
        {
            "click .delete" : "removeRow",
            "change .field-price": "updateRow",
            "change .field-price-duplex": "updateRow",
            "change .field-quantity": "updateRow"
        },

        initialize: function() 
        {
            this.model.bind('destroy', this.unrender, this);
        },

        updateRow: function()
        {
            this.model.save({
                quantity: this.$('.field-quantity').val(),
                price: this.$('.field-price').val(),
                price_duplex: this.$('.field-price-duplex') 
                    ? this.$('.field-price-duplex').val() 
                    : null,
                cost: this.$('.field-cost').val()
            });
        },

        render: function() 
        {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },

        removeRow: function()
        {
            this.model.destroy();
            return this;
        },

        unrender: function() 
        {
            this.$el.remove();
        }
    });

    window.PriceRangeView = Backbone.View.extend({
        el: '#tab-pricelist-16 .price-table',
        item_no: 0,
        ids: [],

        events: {
            "change .field-price": "addAll",
            "click a.deletePriceTable": "deleteAll"
        },

        initialize: function() 
        {
            this.render(); 

            this.main = this.$('tbody');
            this.footer = this.$('tfoot');

            _.bindAll(this, 'addOne', 'addAll');

            // add empty row
            var lastItem = 0;            
           
            if(!this.collection.last())
            {
                // if price square metre, linear metre
                if(this.options.name == 'price_square_metre' 
                    || this.options.name == 'price_linear_metre')
                {
                    lastItem = 0;
                }
                else
                {
                    lastItem = 1;
                }
            }
            else
            {
                lastItem = parseInt(this.collection.last().get('quantity')) + 1;
            }

            this.collection.add({
                quantity: lastItem, 
                price: '',
                price_duplex: '',
                cost: '',
                type: ''
            });
            this.addAll();

            // hide used price types
            $('#tab-pricelist-' + this.options.pricelistId + ' select.priceType option[value=' + this.options.name + ']').addClass('hidden').hide();
        },

        render: function() 
        {
            var content = $('#tab-pricelist-' + this.options.pricelistId + ' .price-tables');

            var template = _.template($('#price-content').html());

            content.prepend(template({
                options: this.options
            }));

            this.$el = $('#tab-pricelist-' + this.options.pricelistId + ' .price-' + this.options.name);
        },

        addOne: function(item) 
        {
            id = parseFloat(item.attributes.quantity);

            if($.inArray(id, this.ids) > -1)
            {
                return;
            }
            this.ids.push(id);

            var view = new PriceRangeRowView({
                model: item,
                item_no: this.item_no,
                type: 'exist',
                pricelistId: this.options.pricelistId,
                name: this.options.name,
                totalRowsNo: this.collection.length
            });

            this.main.append(view.render().el);
            this.item_no++;
        },

        addAll: function() 
        {
            this.collection.sort(); // sort by quantity

            // add empty line (if not set)
            var lastrow = (this.collection.last()) 
                ? this.collection.last() 
                : null; 

            // if we add price at last row, insert blank row at end
            if(lastrow != null && lastrow.get('price') != '')
            {
                var itemId = parseInt(lastrow.get('quantity')) + 1;

                this.collection.add({
                    quantity: itemId, 
                    price: '',
                    price_duplex: '',
                    cost: '',
                    type: ''
                });
            }

            // render all collection
            this.item_no = 0;
            this.main.html('');
            this.ids = [];
            this.collection.each(this.addOne);
        },

        deleteAll: function() 
        {
            this.$el.remove();

            // un-hide used price types
            $('#tab-pricelist-' + this.options.pricelistId + ' select.priceType option[value=' + this.options.name + ']').removeClass('hidden').show();
        }
    });

});
